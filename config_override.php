<?php
/***CONFIGURATOR***/
$sugar_config['disable_persistent_connections'] = false;
$sugar_config['email_xss'] = 'YToxMzp7czo2OiJhcHBsZXQiO3M6NjoiYXBwbGV0IjtzOjQ6ImJhc2UiO3M6NDoiYmFzZSI7czo1OiJlbWJlZCI7czo1OiJlbWJlZCI7czo0OiJmb3JtIjtzOjQ6ImZvcm0iO3M6NToiZnJhbWUiO3M6NToiZnJhbWUiO3M6ODoiZnJhbWVzZXQiO3M6ODoiZnJhbWVzZXQiO3M6NjoiaWZyYW1lIjtzOjY6ImlmcmFtZSI7czo2OiJpbXBvcnQiO3M6ODoiXD9pbXBvcnQiO3M6NToibGF5ZXIiO3M6NToibGF5ZXIiO3M6NDoibGluayI7czo0OiJsaW5rIjtzOjY6Im9iamVjdCI7czo2OiJvYmplY3QiO3M6MzoieG1wIjtzOjM6InhtcCI7czo2OiJzY3JpcHQiO3M6Njoic2NyaXB0Ijt9';
$sugar_config['disabled_languages'] = '';
$sugar_config['default_theme'] = 'SuiteR';
$sugar_config['disabled_themes'] = 'SuiteP,Suite7';
$sugar_config['slow_query_time_msec'] = '150';
$sugar_config['default_module_favicon'] = true;
$sugar_config['dashlet_auto_refresh_min'] = '30';
$sugar_config['stack_trace_errors'] = false;
$sugar_config['developerMode'] = false;
$sugar_config['list_max_entries_per_page'] = '25';
$sugar_config['list_max_entries_per_subpanel'] = '15';
$sugar_config['dbconfigoption']['collation'] = 'utf8_general_ci';
$sugar_config['default_date_format'] = 'd/m/Y';
$sugar_config['default_language'] = 'es_ES';
$sugar_config['hide_history_contacts_emails']['Cases'] = false;
$sugar_config['hide_history_contacts_emails']['Accounts'] = false;
$sugar_config['hide_history_contacts_emails']['Opportunities'] = true;
$sugar_config['export_delimiter'] = '|';
$sugar_config['admin_export_only'] = true;
$sugar_config['default_number_grouping_seperator'] = '';
$sugar_config['disable_convert_lead'] = true;
$sugar_config['hide_subpanels'] = false;
$sugar_config['passwordsetting']['systexpirationtype'] = '1';
$sugar_config['passwordsetting']['generatepasswordtmpl'] = '8411da79-ef3a-76e6-5667-59ac9ebb372c';
$sugar_config['passwordsetting']['lostpasswordtmpl'] = '15da2a21-3e82-9a0b-6268-59b739e15d29';
$sugar_config['SAML_loginurl'] = '';
$sugar_config['SAML_logouturl'] = '';
$sugar_config['SAML_X509Cert'] = '';
$sugar_config['authenticationClass'] = '';
/***CONFIGURATOR***/