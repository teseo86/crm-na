<?php
 // created: 2017-05-02 00:40:46
$layout_defs["Leads"]["subpanel_setup"]['fp_events_leads_1'] = array (
  'order' => 100,
  'module' => 'FP_events',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_FP_EVENTS_LEADS_1_FROM_FP_EVENTS_TITLE',
  'get_subpanel_data' => 'fp_events_leads_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopCreateButton',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopEventSelectButton',
     'mode' => 'MultiSelect',
    
     
    ),
  ),
);
