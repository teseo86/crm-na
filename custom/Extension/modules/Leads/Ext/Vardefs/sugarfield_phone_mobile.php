<?php
 // created: 2017-03-06 21:21:32
$dictionary['Lead']['fields']['phone_mobile']['audited']=true;
$dictionary['Lead']['fields']['phone_mobile']['inline_edit']=true;
$dictionary['Lead']['fields']['phone_mobile']['comments']='Mobile phone number of the contact';
$dictionary['Lead']['fields']['phone_mobile']['merge_filter']='disabled';

 ?>