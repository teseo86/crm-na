<?php
 // created: 2017-03-06 21:20:12
$dictionary['Lead']['fields']['first_name']['audited']=true;
$dictionary['Lead']['fields']['first_name']['inline_edit']=true;
$dictionary['Lead']['fields']['first_name']['comments']='First name of the contact';
$dictionary['Lead']['fields']['first_name']['merge_filter']='disabled';

 ?>