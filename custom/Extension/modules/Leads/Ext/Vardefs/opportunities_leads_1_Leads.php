<?php
// created: 2017-07-19 07:10:12
$dictionary["Lead"]["fields"]["opportunities_leads_1"] = array (
  'name' => 'opportunities_leads_1',
  'type' => 'link',
  'relationship' => 'opportunities_leads_1',
  'source' => 'non-db',
  'module' => 'Opportunities',
  'bean_name' => 'Opportunity',
  'vname' => 'LBL_OPPORTUNITIES_LEADS_1_FROM_OPPORTUNITIES_TITLE',
);
