<?php
 // created: 2017-03-06 21:18:44
$dictionary['Lead']['fields']['birthdate']['audited']=true;
$dictionary['Lead']['fields']['birthdate']['inline_edit']=true;
$dictionary['Lead']['fields']['birthdate']['comments']='The birthdate of the contact';
$dictionary['Lead']['fields']['birthdate']['merge_filter']='disabled';
$dictionary['Lead']['fields']['birthdate']['enable_range_search']=false;

 ?>