<?php
 // created: 2017-03-06 21:21:43
$dictionary['Lead']['fields']['phone_home']['audited']=true;
$dictionary['Lead']['fields']['phone_home']['inline_edit']=true;
$dictionary['Lead']['fields']['phone_home']['comments']='Home phone number of the contact';
$dictionary['Lead']['fields']['phone_home']['merge_filter']='disabled';

 ?>