<?php
 // created: 2017-07-19 07:28:55
$dictionary['Opportunity']['fields']['name']['required']=false;
$dictionary['Opportunity']['fields']['name']['inline_edit']=true;
$dictionary['Opportunity']['fields']['name']['comments']='Name of the opportunity';
$dictionary['Opportunity']['fields']['name']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['name']['len']='255';
$dictionary['Opportunity']['fields']['name']['full_text_search']=array (
);

 ?>