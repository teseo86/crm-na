<?php
// created: 2017-07-19 07:10:12
$dictionary["Opportunity"]["fields"]["opportunities_leads_1"] = array (
  'name' => 'opportunities_leads_1',
  'type' => 'link',
  'relationship' => 'opportunities_leads_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_OPPORTUNITIES_LEADS_1_FROM_LEADS_TITLE',
);
