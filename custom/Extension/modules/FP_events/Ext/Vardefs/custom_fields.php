<?php

$dictionary["FP_events"]["fields"]["register_user_email"] = array (
	'required' => false,
	'name' => 'register_user_email',
	'vname' => 'LBL_REGISTER_USER_EMAIL_MESSAGE',
	'type' => 'text',
	'massupdate' => 0,
	'default' => '',
	'comments' => '',
	'help' => '',
	'audited' => 1,
	'reportable' => 0,
	'studio' => 'visible',
	'dependency' => false,
);  

$dictionary["FP_events"]["fields"]["check_to_send_email"] = array (
    'required' => false,
    'name' => 'check_to_send_email',
    'vname' => 'LBL_CHECK_TO_SEND_EMAIL',
    'type' => 'bool',
	'massupdate' => 0,
	'comments' => '',
	'help' => '',
	'importable' => 'true',
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => '0',
	'audited' => false,
	'reportable' => true,
	'calculated' => false,
	'len' => '255',
	'size' => '20',
);
?> 

?> 
