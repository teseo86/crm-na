<?php
 // created: 2017-02-22 17:59:59
$dictionary['FP_events']['fields']['duration_hours']['required']=false;
$dictionary['FP_events']['fields']['duration_hours']['inline_edit']=true;
$dictionary['FP_events']['fields']['duration_hours']['comments']='Duration (hours)';
$dictionary['FP_events']['fields']['duration_hours']['merge_filter']='disabled';
$dictionary['FP_events']['fields']['duration_hours']['enable_range_search']=false;
$dictionary['FP_events']['fields']['duration_hours']['min']=false;
$dictionary['FP_events']['fields']['duration_hours']['max']=false;
$dictionary['FP_events']['fields']['duration_hours']['disable_num_format']='';

 ?>