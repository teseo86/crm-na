<?php
// created: 2017-02-20 21:57:03
$dictionary["FP_events"]["fields"]["campaigns_fp_events_1"] = array (
  'name' => 'campaigns_fp_events_1',
  'type' => 'link',
  'relationship' => 'campaigns_fp_events_1',
  'source' => 'non-db',
  'module' => 'Campaigns',
  'bean_name' => 'Campaign',
  'vname' => 'LBL_CAMPAIGNS_FP_EVENTS_1_FROM_CAMPAIGNS_TITLE',
  'id_name' => 'campaigns_fp_events_1campaigns_ida',
);
$dictionary["FP_events"]["fields"]["campaigns_fp_events_1_name"] = array (
  'name' => 'campaigns_fp_events_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CAMPAIGNS_FP_EVENTS_1_FROM_CAMPAIGNS_TITLE',
  'save' => true,
  'id_name' => 'campaigns_fp_events_1campaigns_ida',
  'link' => 'campaigns_fp_events_1',
  'table' => 'campaigns',
  'module' => 'Campaigns',
  'rname' => 'name',
);
$dictionary["FP_events"]["fields"]["campaigns_fp_events_1campaigns_ida"] = array (
  'name' => 'campaigns_fp_events_1campaigns_ida',
  'type' => 'link',
  'relationship' => 'campaigns_fp_events_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CAMPAIGNS_FP_EVENTS_1_FROM_FP_EVENTS_TITLE',
);
