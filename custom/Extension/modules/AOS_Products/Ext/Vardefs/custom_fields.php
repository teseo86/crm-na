<?php


$dictionary['AOS_Products']['fields']['contacts_aos_products_1_fields'] = array (
			'name' => 'contacts_aos_products_1_fields',
			'rname' => 'id',
			'relationship_fields'=>array(
                            'id' => 'contacts_aos_products_1_id', 
                            'precio_curso' => 'contacts_aos_products_1_precio_curso', 
                            'vendedor' => 'contacts_aos_products_1_vendedor',
                            'id_inscripcion' => 'contacts_aos_products_1_id_inscripcion',
                            'como_se_entero' => 'contacts_aos_products_1_como_se_entero',
                            'rol' => 'contacts_aos_products_1_rol',
                            'fecha_inicio_clases' => 'contacts_aos_products_1_fecha_inicio_clases',
                            'fecha_fin_clases' => 'contacts_aos_products_1_fecha_fin_clases',
                        ),
			'vname' => 'LBL_CONTACTS_AOS_PRODUCTS_1_FIELDS',
			'type' => 'relate',
			'link' => 'contacts_aos_products_1',
			'link_type' => 'relationship_info',
			'join_link_name' => 'contacts_aos_products_1_c',
                        'join_name' => 'contacts_aos_products_1_c',
			'source' => 'non-db',
			'importable' => false,
                        'duplicate_merge'=> 'disabled',
			'studio' => array('listview' => false),
			'join_primary' => false, //this is key!!! See SugarBean.php and search for join_primary for more info

    
);

$dictionary['AOS_Products']['fields']['contacts_aos_products_1_id'] = array (
			'name' => 'contacts_aos_products_1_id',
			'type' => 'varchar',
			'source' => 'non-db',
			'vname' => 'LBL_CONTACT_AOS_PRODUCTS_1_ID',
			'studio' => array('listview' => false),
);

$dictionary['AOS_Products']['fields']['contacts_aos_products_1_vendedor'] = array (
			'name' => 'contacts_aos_products_1_vendedor',
			'type' => 'varchar',
                        'source' => 'non-db',
			'vname' => 'LBL_VENDEDOR',
                        'reportable' => true,
);

$dictionary['AOS_Products']['fields']['contacts_aos_products_1_precio_curso'] = array (
			'name' => 'contacts_aos_products_1_precio_curso',
			'type' => 'varchar',
                        'source' => 'non-db',
			'vname' => 'LBL_PRECIO_CURSO',
                        'reportable' => true,
);


$dictionary['AOS_Products']['fields']['contacts_aos_products_1_id_inscripcion'] = array (
			'name' => 'contacts_aos_products_1_id_inscripcion',
			'type' => 'varchar',
                        'source' => 'non-db',
			'vname' => 'LBL_ID_INSCRIPCION',
                        'reportable' => true,
);

$dictionary['AOS_Products']['fields']['contacts_aos_products_1_rol'] = array (
			'name' => 'contacts_aos_products_1_rol',
			'type' => 'varchar',
                        'source' => 'non-db',
			'vname' => 'LBL_ROL_CONTACTO',
                        'reportable' => true,
);

$dictionary['AOS_Products']['fields']['contacts_aos_products_1_como_se_entero'] = array (
			'name' => 'contacts_aos_products_1_como_se_entero',
			'type' => 'varchar',
                        'source' => 'non-db',
			'vname' => 'LBL_COMO_SE_ENTERO_CURSO',
                        'reportable' => true,
);

$dictionary['AOS_Products']['fields']['contacts_aos_products_1_fecha_inicio_clases'] = array (
			'name' => 'contacts_aos_products_1_fecha_inicio_clases',
			'type' => 'date',
                        'source' => 'non-db',
			'vname' => 'LBL_FECHA_INICIO_CURSO',
                        'reportable' => true,
);

$dictionary['AOS_Products']['fields']['contacts_aos_products_1_fecha_fin_clases'] = array (
			'name' => 'contacts_aos_products_1_fecha_fin_clases',
			'type' => 'date',
                        'source' => 'non-db',
			'vname' => 'LBL_FECHA_FIN_CURSO',
                        'reportable' => true,
);

