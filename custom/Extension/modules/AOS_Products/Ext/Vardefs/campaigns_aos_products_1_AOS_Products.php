<?php
// created: 2017-02-20 21:57:03
$dictionary["AOS_Products"]["fields"]["campaigns_aos_products_1"] = array (
  'name' => 'campaigns_aos_products_1',
  'type' => 'link',
  'relationship' => 'campaigns_aos_products_1',
  'source' => 'non-db',
  'module' => 'Campaigns',
  'bean_name' => 'Campaign',
  'vname' => 'LBL_CAMPAIGNS_AOS_PRODUCTS_1_FROM_CAMPAIGNS_TITLE',
  'id_name' => 'campaigns_aos_products_1campaigns_ida',
);
$dictionary["AOS_Products"]["fields"]["campaigns_aos_products_1_name"] = array (
  'name' => 'campaigns_aos_products_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CAMPAIGNS_AOS_PRODUCTS_1_FROM_CAMPAIGNS_TITLE',
  'save' => true,
  'id_name' => 'campaigns_aos_products_1campaigns_ida',
  'link' => 'campaigns_aos_products_1',
  'table' => 'campaigns',
  'module' => 'Campaigns',
  'rname' => 'name',
);
$dictionary["AOS_Products"]["fields"]["campaigns_aos_products_1campaigns_ida"] = array (
  'name' => 'campaigns_aos_products_1campaigns_ida',
  'type' => 'link',
  'relationship' => 'campaigns_aos_products_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CAMPAIGNS_AOS_PRODUCTS_1_FROM_AOS_PRODUCTS_TITLE',
);
