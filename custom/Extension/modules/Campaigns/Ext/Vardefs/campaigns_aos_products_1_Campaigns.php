<?php
// created: 2017-02-20 21:57:03
$dictionary["Campaign"]["fields"]["campaigns_aos_products_1"] = array (
  'name' => 'campaigns_aos_products_1',
  'type' => 'link',
  'relationship' => 'campaigns_aos_products_1',
  'source' => 'non-db',
  'module' => 'AOS_Products',
  'bean_name' => 'AOS_Products',
  'side' => 'right',
  'vname' => 'LBL_CAMPAIGNS_AOS_PRODUCTS_1_FROM_AOS_PRODUCTS_TITLE',
);
