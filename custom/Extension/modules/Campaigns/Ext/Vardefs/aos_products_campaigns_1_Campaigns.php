<?php
// created: 2017-02-20 21:26:39
$dictionary["Campaign"]["fields"]["aos_products_campaigns_1"] = array (
  'name' => 'aos_products_campaigns_1',
  'type' => 'link',
  'relationship' => 'aos_products_campaigns_1',
  'source' => 'non-db',
  'module' => 'AOS_Products',
  'bean_name' => 'AOS_Products',
  'vname' => 'LBL_AOS_PRODUCTS_CAMPAIGNS_1_FROM_AOS_PRODUCTS_TITLE',
  'id_name' => 'aos_products_campaigns_1aos_products_ida',
);
$dictionary["Campaign"]["fields"]["aos_products_campaigns_1_name"] = array (
  'name' => 'aos_products_campaigns_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_AOS_PRODUCTS_CAMPAIGNS_1_FROM_AOS_PRODUCTS_TITLE',
  'save' => true,
  'id_name' => 'aos_products_campaigns_1aos_products_ida',
  'link' => 'aos_products_campaigns_1',
  'table' => 'aos_products',
  'module' => 'AOS_Products',
  'rname' => 'name',
);
$dictionary["Campaign"]["fields"]["aos_products_campaigns_1aos_products_ida"] = array (
  'name' => 'aos_products_campaigns_1aos_products_ida',
  'type' => 'link',
  'relationship' => 'aos_products_campaigns_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_AOS_PRODUCTS_CAMPAIGNS_1_FROM_CAMPAIGNS_TITLE',
);
