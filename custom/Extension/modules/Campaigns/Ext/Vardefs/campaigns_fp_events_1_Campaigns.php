<?php
// created: 2017-02-20 21:57:03
$dictionary["Campaign"]["fields"]["campaigns_fp_events_1"] = array (
  'name' => 'campaigns_fp_events_1',
  'type' => 'link',
  'relationship' => 'campaigns_fp_events_1',
  'source' => 'non-db',
  'module' => 'FP_events',
  'bean_name' => 'FP_events',
  'side' => 'right',
  'vname' => 'LBL_CAMPAIGNS_FP_EVENTS_1_FROM_FP_EVENTS_TITLE',
);
