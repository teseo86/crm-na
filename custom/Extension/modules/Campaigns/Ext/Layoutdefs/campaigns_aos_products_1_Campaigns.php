<?php
 // created: 2017-02-20 21:57:03
$layout_defs["Campaigns"]["subpanel_setup"]['campaigns_aos_products_1'] = array (
  'order' => 100,
  'module' => 'AOS_Products',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CAMPAIGNS_AOS_PRODUCTS_1_FROM_AOS_PRODUCTS_TITLE',
  'get_subpanel_data' => 'campaigns_aos_products_1',
  'top_buttons' => 
  array (
    0 => 
    array (
     // 'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      //'widget_class' => 'SubPanelTopSelectButton',
      //'mode' => 'MultiSelect',
    ),
  ),
);
