<?php
 // created: 2017-02-20 21:57:03
$layout_defs["Campaigns"]["subpanel_setup"]['campaigns_fp_events_1'] = array (
  'order' => 100,
  'module' => 'FP_events',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CAMPAIGNS_FP_EVENTS_1_FROM_FP_EVENTS_TITLE',
  'get_subpanel_data' => 'campaigns_fp_events_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      //'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
     // 'widget_class' => 'SubPanelTopSelectButton',
     // 'mode' => 'MultiSelect',
    ),
  ),
);
