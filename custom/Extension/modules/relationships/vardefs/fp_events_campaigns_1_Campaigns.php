<?php
// created: 2017-02-20 21:27:33
$dictionary["Campaign"]["fields"]["fp_events_campaigns_1"] = array (
  'name' => 'fp_events_campaigns_1',
  'type' => 'link',
  'relationship' => 'fp_events_campaigns_1',
  'source' => 'non-db',
  'module' => 'FP_events',
  'bean_name' => 'FP_events',
  'vname' => 'LBL_FP_EVENTS_CAMPAIGNS_1_FROM_FP_EVENTS_TITLE',
  'id_name' => 'fp_events_campaigns_1fp_events_ida',
);
$dictionary["Campaign"]["fields"]["fp_events_campaigns_1_name"] = array (
  'name' => 'fp_events_campaigns_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_FP_EVENTS_CAMPAIGNS_1_FROM_FP_EVENTS_TITLE',
  'save' => true,
  'id_name' => 'fp_events_campaigns_1fp_events_ida',
  'link' => 'fp_events_campaigns_1',
  'table' => 'fp_events',
  'module' => 'FP_events',
  'rname' => 'name',
);
$dictionary["Campaign"]["fields"]["fp_events_campaigns_1fp_events_ida"] = array (
  'name' => 'fp_events_campaigns_1fp_events_ida',
  'type' => 'link',
  'relationship' => 'fp_events_campaigns_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_FP_EVENTS_CAMPAIGNS_1_FROM_CAMPAIGNS_TITLE',
);
