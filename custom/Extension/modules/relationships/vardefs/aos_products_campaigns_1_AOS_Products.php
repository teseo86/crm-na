<?php
// created: 2017-02-20 21:26:39
$dictionary["AOS_Products"]["fields"]["aos_products_campaigns_1"] = array (
  'name' => 'aos_products_campaigns_1',
  'type' => 'link',
  'relationship' => 'aos_products_campaigns_1',
  'source' => 'non-db',
  'module' => 'Campaigns',
  'bean_name' => 'Campaign',
  'side' => 'right',
  'vname' => 'LBL_AOS_PRODUCTS_CAMPAIGNS_1_FROM_CAMPAIGNS_TITLE',
);
