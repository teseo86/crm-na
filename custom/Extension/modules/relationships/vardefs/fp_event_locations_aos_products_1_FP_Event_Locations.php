<?php
// created: 2017-08-31 18:30:20
$dictionary["FP_Event_Locations"]["fields"]["fp_event_locations_aos_products_1"] = array (
  'name' => 'fp_event_locations_aos_products_1',
  'type' => 'link',
  'relationship' => 'fp_event_locations_aos_products_1',
  'source' => 'non-db',
  'module' => 'AOS_Products',
  'bean_name' => 'AOS_Products',
  'side' => 'right',
  'vname' => 'LBL_FP_EVENT_LOCATIONS_AOS_PRODUCTS_1_FROM_AOS_PRODUCTS_TITLE',
);
