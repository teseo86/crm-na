<?php
// created: 2017-08-31 18:30:20
$dictionary["AOS_Products"]["fields"]["fp_event_locations_aos_products_1"] = array (
  'name' => 'fp_event_locations_aos_products_1',
  'type' => 'link',
  'relationship' => 'fp_event_locations_aos_products_1',
  'source' => 'non-db',
  'module' => 'FP_Event_Locations',
  'bean_name' => 'FP_Event_Locations',
  'vname' => 'LBL_FP_EVENT_LOCATIONS_AOS_PRODUCTS_1_FROM_FP_EVENT_LOCATIONS_TITLE',
  'id_name' => 'fp_event_locations_aos_products_1fp_event_locations_ida',
);
$dictionary["AOS_Products"]["fields"]["fp_event_locations_aos_products_1_name"] = array (
  'name' => 'fp_event_locations_aos_products_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_FP_EVENT_LOCATIONS_AOS_PRODUCTS_1_FROM_FP_EVENT_LOCATIONS_TITLE',
  'save' => true,
  'id_name' => 'fp_event_locations_aos_products_1fp_event_locations_ida',
  'link' => 'fp_event_locations_aos_products_1',
  'table' => 'fp_event_locations',
  'module' => 'FP_Event_Locations',
  'rname' => 'name',
);
$dictionary["AOS_Products"]["fields"]["fp_event_locations_aos_products_1fp_event_locations_ida"] = array (
  'name' => 'fp_event_locations_aos_products_1fp_event_locations_ida',
  'type' => 'link',
  'relationship' => 'fp_event_locations_aos_products_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_FP_EVENT_LOCATIONS_AOS_PRODUCTS_1_FROM_AOS_PRODUCTS_TITLE',
);
