<?php
// created: 2017-02-20 21:27:33
$dictionary["FP_events"]["fields"]["fp_events_campaigns_1"] = array (
  'name' => 'fp_events_campaigns_1',
  'type' => 'link',
  'relationship' => 'fp_events_campaigns_1',
  'source' => 'non-db',
  'module' => 'Campaigns',
  'bean_name' => 'Campaign',
  'side' => 'right',
  'vname' => 'LBL_FP_EVENTS_CAMPAIGNS_1_FROM_CAMPAIGNS_TITLE',
);
