<?php
// created: 2017-06-27 12:49:26
$dictionary["AOS_Products"]["fields"]["contacts_aos_products_1"] = array (
  'name' => 'contacts_aos_products_1',
  'type' => 'link',
  'relationship' => 'contacts_aos_products_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_CONTACTS_AOS_PRODUCTS_1_FROM_CONTACTS_TITLE',
);
