<?php
// created: 2017-02-21 17:40:05
$dictionary["AOS_Products"]["fields"]["aos_products_opportunities_1"] = array (
  'name' => 'aos_products_opportunities_1',
  'type' => 'link',
  'relationship' => 'aos_products_opportunities_1',
  'source' => 'non-db',
  'module' => 'Opportunities',
  'bean_name' => 'Opportunity',
  'side' => 'right',
  'vname' => 'LBL_AOS_PRODUCTS_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE',
);
