<?php
// created: 2017-06-27 12:49:26
$dictionary["Contact"]["fields"]["contacts_aos_products_1"] = array (
  'name' => 'contacts_aos_products_1',
  'type' => 'link',
  'relationship' => 'contacts_aos_products_1',
  'source' => 'non-db',
  'module' => 'AOS_Products',
  'bean_name' => 'AOS_Products',
  'vname' => 'LBL_CONTACTS_AOS_PRODUCTS_1_FROM_AOS_PRODUCTS_TITLE',
);
