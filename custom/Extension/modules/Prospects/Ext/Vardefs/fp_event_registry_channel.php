<?php
 // created: 2017-02-16 20:11:38
$dictionary['Prospect']['fields']['e_registry_fields']=
                array(
                    'name' => 'e_registry_fields',
                    'rname' => 'id',
                    'relationship_fields' =>
                        array(
                            'id' => 'registry_channel_id',
                            'registry_channel' => 'registry_channel_name',
                             'how_to_know' => 'how_to_know',
                              'how_to_know_count' => 'how_to_know_count',
                              'id_reservation' => 'id_reservation',
                              'datetime_reservation' => 'datetime_reservation',
                              'description' => 'description',
                        ),
                    'vname' => 'LBL_REGISTRY_CHANNEL',
                   'type' => 'relate',
                'link' => 'fp_events_prospects_1',
                'link_type' => 'relationship_info',
                'join_link_name' => 'fp_events_prospects_1',
                'source' => 'non-db',
                    'importable' => 'false',
                    'duplicate_merge' => 'disabled',
                    'studio' => false,
                );
       $dictionary['Prospect']['fields']['registry_channel_name']=
                array(
                    'massupdate' => false,
                    'name' => 'registry_channel_name',
                    'type' => 'enum',
                    'studio' => 'false',
                    'source' => 'non-db',
                    'vname' => 'LBL_LIST_REGISTRY_CHANNEL',
                    'options' => 'fp_event_registry_channel_dom',
                    'importable' => 'false',
                );
           $dictionary['Prospect']['fields']['registry_channel_id']=
                array(
                    'name' => 'registry_channel_id',
                    'type' => 'varchar',
                    'source' => 'non-db',
                    'vname' => 'LBL_LIST_REGISTRY_CHANNEL',
                    'studio' =>
                        array(
                            'listview' => false,
                        ),
                );
                
                 $dictionary['Prospect']['fields']['how_to_know']=
                array(
                    'massupdate' => false,
                    'name' => 'how_to_know',
                    'type' => 'varchar',
                    'studio' => 'false',
                    'source' => 'non-db',
                    'vname' => 'LBL_HOW_TO_KNOW',
                   'importable' => 'false',
                );
                $dictionary['Prospect']['fields']['how_to_know_count']=
                array(
                    'massupdate' => false,
                    'name' => 'how_to_know_count',
                    'type' => 'int',
                    'studio' => 'false',
                    'source' => 'non-db',
                    'vname' => 'LBL_HOW_TO_KNOW',
                   'importable' => 'false',
                );
                $dictionary['Prospect']['fields']['id_reservation']=
                array(
                    'massupdate' => false,
                    'name' => 'id_reservation',
                    'type' => 'varchar',
                    'studio' => 'false',
                    'source' => 'non-db',
                    'vname' => 'LBL_ID_RESERVATION',
                   'importable' => 'false',
                );
                $dictionary['Prospect']['fields']['datetime_reservation']=
                array(
                    'massupdate' => false,
                    'name' => 'datetime_reservation',
                    'type' => 'datetime',
                    'studio' => 'false',
                    'source' => 'non-db',
                    'vname' => 'LBL_DATETIME_RESERVATION',
                   'importable' => 'false',
                );
                $dictionary['Prospect']['fields']['description']=
                array(
                    'massupdate' => false,
                    'name' => 'description',
                    'type' => 'text',
                    'studio' => 'false',
                    'source' => 'non-db',
                    'vname' => 'LBL_DESCRIPTION',
                   'importable' => 'false',
                );


 ?>
