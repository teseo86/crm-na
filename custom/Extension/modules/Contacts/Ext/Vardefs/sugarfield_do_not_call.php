<?php
 // created: 2017-03-23 20:58:53
$dictionary['Contact']['fields']['do_not_call']['inline_edit']=true;
$dictionary['Contact']['fields']['do_not_call']['comments']='An indicator of whether contact can be called';
$dictionary['Contact']['fields']['do_not_call']['merge_filter']='disabled';

 ?>