<?php
// created: 2017-08-24 16:13:23
$dictionary["smart_Consulta"]["fields"]["smart_consulta_leads"] = array (
  'name' => 'smart_consulta_leads',
  'type' => 'link',
  'relationship' => 'smart_consulta_leads',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_SMART_CONSULTA_LEADS_FROM_LEADS_TITLE',
  'id_name' => 'smart_consulta_leadsleads_ida',
);
$dictionary["smart_Consulta"]["fields"]["smart_consulta_leads_name"] = array (
  'name' => 'smart_consulta_leads_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SMART_CONSULTA_LEADS_FROM_LEADS_TITLE',
  'save' => true,
  'id_name' => 'smart_consulta_leadsleads_ida',
  'link' => 'smart_consulta_leads',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["smart_Consulta"]["fields"]["smart_consulta_leadsleads_ida"] = array (
  'name' => 'smart_consulta_leadsleads_ida',
  'type' => 'link',
  'relationship' => 'smart_consulta_leads',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_SMART_CONSULTA_LEADS_FROM_SMART_CONSULTA_TITLE',
);
