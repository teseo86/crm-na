<?php
$app_list_strings['moduleList']['FP_events']='Actividades';
$app_list_strings['moduleListSingular']['FP_events']='Actividades';
$GLOBALS['app_list_strings']['departamentos_peru_list']=array (
  'lima' => 'Lima',
  'arequipa' => 'Arequipa',
  'chiclayo' => 'Chiclayo',
);
$app_list_strings['moduleList']['FP_Event_Locations']='Sedes';
$app_list_strings['moduleListSingular']['FP_Event_Locations']='Sedes';
$GLOBALS['app_list_strings']['gender_list']=array (
  'male' => 'Masculino',
  'female' => 'Femenino',
);
$GLOBALS['app_list_strings']['courses_list']=array (
  'yoga' => 'Yoga',
  'meditacion' => 'Meditación',
  'oratoria' => 'Oratoria',
  'inteligencia_emocional' => 'Inteligencia Emocional',
  'lectura_veloz' => 'Lectura Veloz',
  'tecnicas_estudio' => 'Técnicas de Estudio',
  'chikung' => 'Chi Kung',
  'tecnicas_controlar_estres' => 'Técnicas para controlar el estrés',
  'ortografia_redaccion' => 'Ortografía y Redacción',
  'relajacion' => 'Relajación',
  'yoga_en_forma' => 'Yoga en forma',
  'administracion_tiempo' => 'Administración Eficaz del Tiempo',
  'liderazgo' => 'Liderazgo',
);
$GLOBALS['app_list_strings']['sede_list']=array (
  'sede_central' => 'San Isidro',
  'miraflores' => 'Miraflores',
  'pueblo_libre' => 'Pueblo Libre',
  'san_miguel' => 'San Miguel',
  'brena' => 'Breña',
);
$GLOBALS['app_list_strings']['horario_comunicacion_list']=array (
  'morning' => 'Mañana',
  'tarde' => 'Tarde',
  'noche' => 'Noche',
);
$GLOBALS['app_list_strings']['phone_communication_list']=array (
  'call' => 'Llamada',
  'sms' => 'SMS',
  'whatsapp' => 'Whatsapp',
  'facebook' => 'Facebook',
);
$app_list_strings['moduleList']['AOS_Products']='Cursos';
$app_list_strings['moduleListSingular']['AOS_Products']='Cursos';
$GLOBALS['app_list_strings']['activity_type_list']=array (
  'charla' => 'Charla',
  'conferencia' => 'Conferencia',
  'exposicion_didactica' => 'Exposición Didáctica',
  'dialogo_filosofico' => 'Diálogo Filosófico',
  'taller' => 'Taller',
  'charla_presentacion' => 'Charla de Presentación',
  'velada_filosofica' => 'Velada Filosófica',
  'seminario_filosofia' => 'Seminario de Filosofía',
  'teatro_infantil' => 'Teatro Infantil',
  'cuenta_cuentos' => 'Cuenta cuentos filosóficos',
  'voluntariado' => '	 Voluntariado',
  'charla_cafe' => 'Charla Café',
  'exposicion_montaje' => 'Exposición Montaje',
  'recital' => 'Recital',
  'teatro' => 'Teatro',
  'ciclo_conferencias' => 'Ciclo de Conferencias',
  'charla_medica' => 'Charla Médica',
  'exposicion' => 'Exposición',
  'seminario_taller' => 'Seminario Taller',
  'seminario' => 'Seminario',
  'video_forum' => 'Video Fórum',
  'documental' => 'Documental',
  'presentacion_musical' => 'Presentación Musical',
  'presentacion_teatral' => 'Presentación Teatral',
  'charla_audiovisual' => 'Charla Audiovisual',
  'concierto_gala' => 'Concierto de Gala',
  'clase_presentacion' => 'Clase de Presentación',
  'charla_taller' => 'Charla Taller',
  'charla_asociados' => 'Charla para Asociados',
  'conversatorio' => 'Conversatorio',
  'charla_informativa' => 'Charla Informativa',
  'clase_demostrativa' => 'Clase Demostrativa',
  'libro_forum' => 'Libro Fórum',
  'cine_forum' => 'Cine Fórum',
  'audicion' => 'Audición',
  'cafe_filosofico' => 'Café Filosófico',
  'otro' => 'Otro',
);
$GLOBALS['app_list_strings']['activity_status_list']=array (
  'active' => 'Activo',
  'inactive' => 'Inactivo',
);

$GLOBALS['app_list_strings']['activity_status_type_dom']=array (
  '' => '--Ninguno--',
  'active' => 'Activo',
  'inactive' => 'Inactivo',
);
$GLOBALS['app_list_strings']['fp_event_invite_status_dom']=array (
  'Invited' => 'Invitados',
  'Not Invited' => 'No Invitados',
  'Attended' => 'Asistentes',
  'Not Attended' => 'No Asistentes',
  'Confirm' => 'Confirmado',
  'Assist' => 'Asistente',
  'AssistWithoutReservation' => 'Asistente sin reserva',
  'Register' => 'Registrado',
);
$app_list_strings['moduleList']['Leads']='Interesados';
$app_list_strings['moduleList']['AOR_Reports']='Reportes';
$app_list_strings['moduleListSingular']['Leads']='Interesados';
$app_list_strings['moduleListSingular']['AOR_Reports']='Reportes';
$app_list_strings['record_type_display']['Leads']='Interesados';
$app_list_strings['parent_type_display']['Leads']='Interesados';
$app_list_strings['record_type_display_notes']['Leads']='Interesados';
$GLOBALS['app_list_strings']['phone_type_sms_list']=array (
  'sms' => 'SMS',
  'whatsapp' => 'Whatsapp',
);
$app_list_strings['moduleList']['HER01_SMS']='SMS';
$app_list_strings['moduleListSingular']['HER01_SMS']='SMS';
$app_strings['LBL_TABGROUP_ACTIVITIES'] = 'Acciones';

$GLOBALS['app_list_strings']['release_status_dom']=array (
  'Active' => 'Activo',
  'Inactive' => 'Inactivo',
);
$app_list_strings['moduleList']['AOS_Product_Categories']='Curso Base';
$app_list_strings['moduleList']['hero2_course_sell']='Ventas cursos';
$app_list_strings['moduleListSingular']['AOS_Product_Categories']='Curso Base';
$app_list_strings['moduleListSingular']['hero2_course_sell']='Ventas cursos';
$GLOBALS['app_list_strings']['turno_list']=array (
  'preoffice' => 'Pre-Office',
  'manana' => 'Mañana',
  'tarde' => 'Tarde',
  'noche' => 'Noche',
  'sabado' => 'Sábado',
  'domingo' => 'Domingo',
);

$GLOBALS['app_list_strings']['fp_event_registry_channel_dom']=array (
  'Email' => 'Email',
  'Call' => 'Llamada',
  'App' => 'App',
  'Web' => 'Web',
  'Mensaje_texto' => 'Mensaje Texto',
);
$GLOBALS['app_list_strings']['source_message_list']=array (
  '' => '',
  'SMS' => 'SMS',
  'Whatsapp' => 'Whatsapp',
);
$GLOBALS['app_list_strings']['course_type_list']=array (
  'curso' => 'Curso',
  'taller' => 'Taller',
  'curso_empresa' => 'Curso Empresa',
);
$app_list_strings['moduleList']['KReports']='Reportes Especializados';
$app_list_strings['moduleListSingular']['KReports']='Reporte Especializado';
$GLOBALS['app_list_strings']['categoria_cliente_list']=array (
  'sin_categoria' => '',
  'hierro' => 'Hierro',
  'bronce' => 'Bronce',
  'plata' => 'Plata',
  'oro' => 'Oro',
);
$GLOBALS['app_list_strings']['rol_list']=array (
  'probacionista' => 'Probacionista',
  'miembro' => 'Miembro',
  'ffvv' => 'FFVV',
  'voluntario' => 'Voluntario',
  'actividades' => 'Actividades',
  'cursos' => 'Cursos',
  'bd_antigua' => 'BD Antigua',
  'ex_probacionista' => 'Ex Probacionista',
  'ex_miembro' => 'Ex Miembro',
);
$GLOBALS['app_list_strings']['categoria_filosofia_list']=array (
  'sin_categoria' => '',
  'invitar_actividades' => 'Invitar Actividades',
  'invitar_charla_refuerzo' => 'Invitar Charla Refuerzo',
  'invitar_clase_presentacion' => 'Invitar Clase Presentación',
  'invitar_curso_filosofia' => 'Invitar curso Filosofía',
  'personalizado' => 'Invitación personalizada',
);

$GLOBALS['app_list_strings']['categoria_interesado_list']=array (
  '' => '',
  'bd_antigua' => 'BD Antigua',
  'voluntariado' => 'Voluntariado',
  'consultas' => 'Consultas',
  'actividades' => 'Actividades',
);
$GLOBALS['app_list_strings']['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Llamada en Frío',
  'Existing Customer' => 'Cliente Existente',
  'Self Generated' => 'Auto Generado',
  'Public Relations' => 'Relaciones Públicas',
  'Direct Mail' => 'Correo Directo',
  'Conference' => 'Conferencia',
  'Trade Show' => 'Exposición',
  'Web Site' => 'Sitio Web',
  'Word of mouth' => 'Recomendación',
  'Email' => 'Email',
  'Campaign' => 'Campaña',
  'Other' => 'Otro',
  'Fil' => 'FIL',
  'contacto_personal_externo' => 'Contacto Personal Externo',
  'bd_antigua' => 'BD Antigua',
);
$GLOBALS['app_list_strings']['lead_status_dom']=array (
  '' => '',
  'New' => 'Nuevo',
  'Assigned' => 'Asignado',
  'Converted' => 'Convertido',
  'Dead' => 'Fallecido',
  'bd_na' => 'Base de datos NA',
  'baja_definitiva' => 'Baja definitiva',
  'baja_solicitada' => 'Baja Solicitada',
);