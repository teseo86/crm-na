<?php
$GLOBALS['app_list_strings']['gender_list']=array (
  'male' => 'Male',
  'female' => 'Female',
);
$GLOBALS['app_list_strings']['courses_list']=array (
  'yoga' => 'Yoga',
  'meditacion' => 'Meditación',
  'oratoria' => 'Oratoria',
  'inteligencia_emocional' => 'Inteligencia Emocional',
  'lectura_veloz' => 'Lectura Veloz',
  'tecnicas_estudio' => 'Técnicas de Estudio',
  'chikung' => 'Chi Kung',
  'tecnicas_controlar_estres' => 'Técnicas para controlar el estrés',
  'ortografia_redaccion' => 'Ortografía y Redacción',
  'relajacion' => 'Relajación',
  'yoga_en_forma' => 'Yoga en forma',
  'administracion_tiempo' => 'Administración Eficaz del Tiempo',
  'liderazgo' => 'Liderazgo',
);
$GLOBALS['app_list_strings']['sede_list']=array (
  'sede_central' => 'San Isidro',
  'miraflores' => 'Miraflores',
  'pueblo_libre' => 'Pueblo Libre',
  'san_miguel' => 'San Miguel',
  'brena' => 'Breña',
);
$GLOBALS['app_list_strings']['horario_comunicacion_list']=array (
  'morning' => 'Mañana',
  'tarde' => 'Tarde',
  'noche' => 'Noche',
);
$GLOBALS['app_list_strings']['phone_communication_list']=array (
  'call' => 'Llamada',
  'sms' => 'SMS',
  'whatsapp' => 'Whatsapp',
  'facebook' => 'Facebook',
);
$GLOBALS['app_list_strings']['activity_type_list']=array (
  'charla' => 'Charla',
  'conferencia' => 'Conferencia',
  'exposicion_didactica' => 'Exposición Didáctica',
  'dialogo_filosofico' => 'Diálogo Filosófico',
  'taller' => 'Taller',
  'charla_presentacion' => 'Charla de Presentación',
  'velada_filosofica' => 'Velada Filosófica',
  'seminario_filosofia' => 'Seminario de Filosofía',
  'teatro_infantil' => 'Teatro Infantil',
  'cuenta_cuentos' => 'Cuenta cuentos filosóficos',
  'voluntariado' => '	 Voluntariado',
  'charla_cafe' => 'Charla Café',
  'exposicion_montaje' => 'Exposición Montaje',
  'recital' => 'Recital',
  'teatro' => 'Teatro',
  'ciclo_conferencias' => 'Ciclo de Conferencias',
  'charla_medica' => 'Charla Médica',
  'exposicion' => 'Exposición',
  'seminario_taller' => 'Seminario Taller',
  'seminario' => 'Seminario',
  'video_forum' => 'Video Fórum',
  'documental' => 'Documental',
  'presentacion_musical' => 'Presentación Musical',
  'presentacion_teatral' => 'Presentación Teatral',
  'charla_audiovisual' => 'Charla Audiovisual',
  'concierto_gala' => 'Concierto de Gala',
  'clase_presentacion' => 'Clase de Presentación',
  'charla_taller' => 'Charla Taller',
  'charla_asociados' => 'Charla para Asociados',
  'conversatorio' => 'Conversatorio',
  'charla_informativa' => 'Charla Informativa',
  'clase_demostrativa' => 'Clase Demostrativa',
  'libro_forum' => 'Libro Fórum',
  'cine_forum' => 'Cine Fórum',
  'audicion' => 'Audición',
  'cafe_filosofico' => 'Café Filosófico',
  'otro' => 'Otro',
);
$GLOBALS['app_list_strings']['activity_status_list']=array (
  'active' => 'Activo',
  'inactive' => 'Inactivo',
);

$GLOBALS['app_list_strings']['activity_status_type_dom']=array (
  '' => '--None--',
  'active' => 'Active',
  'inactive' => 'Inactive',
);
$GLOBALS['app_list_strings']['fp_event_invite_status_dom']=array (
  'Invited' => 'Invited',
  'Not Invited' => 'Not Invited',
  'Attended' => 'Attended',
  'Not Attended' => 'Not Attended',
  'Confirm' => 'Confirmado',
  'Assist' => 'Asistente',
  'AssistWithoutReservation' => 'Asistente sin reserva',
  'Register' => 'Registrado',
);
$GLOBALS['app_list_strings']['phone_type_sms_list']=array (
  'sms' => 'SMS',
  'whatsapp' => 'Whatsapp',
);
$GLOBALS['app_list_strings']['release_status_dom']=array (
  'Active' => 'Active',
  'Inactive' => 'Inactive',
);
$GLOBALS['app_list_strings']['turno_list']=array (
  'preoffice' => 'Pre-Office',
  'manana' => 'Mañana',
  'tarde' => 'Tarde',
  'noche' => 'Noche',
  'sabado' => 'Sábado',
  'domingo' => 'Domingo',
);

$GLOBALS['app_list_strings']['fp_event_registry_channel_dom']=array (
  'Email' => 'Email',
  'Call' => 'Call',
  'App' => 'App',
  'Web' => 'Web',
  'Mensaje_texto' => 'Mensaje Texto',
);
$GLOBALS['app_list_strings']['record_type_display_notes']=array (
 'Accounts' => 'Account',
	'Contacts' => 'Contact',
	'Opportunities' => 'Opportunity',
	'Tasks' => 'Task',
	'Emails' => 'Email',

	'Bugs' => 'Bug',
	'Project' => 'Project',
	'ProjectTask' => 'Project Task',
	'Prospects' => 'Target',
	'Cases' => 'Case',
	'Leads' => 'Lead',

	'Meetings' => 'Meeting',
	'Calls' => 'Call',
	  'FP_events' => 'Events',
   'AOS_Products' => 'Courses',
'HER01_SMS' => 'SMS',

);

$GLOBALS['app_list_strings']['source_message_list']=array (
  '' => '',
  'SMS' => 'SMS',
  'Whatsapp' => 'Whatsapp',
);
$GLOBALS['app_list_strings']['course_type_list']=array (
  'curso' => 'Curso',
  'taller' => 'Taller',
  'curso_empresa' => 'Curso Empresa',
);
$GLOBALS['app_list_strings']['categoria_cliente_list']=array (
  'sin_categoria' => '',
  'hierro' => 'Hierro',
  'bronce' => 'Bronce',
  'plata' => 'Plata',
  'oro' => 'Oro',
);
$GLOBALS['app_list_strings']['rol_list']=array (
  'probacionista' => 'Probacionista',
  'miembro' => 'Miembro',
  'ffvv' => 'FFVV',
  'voluntario' => 'Voluntario',
  'actividades' => 'Actividades',
  'cursos' => 'Cursos',
  'bd_antigua' => 'BD Antigua',
  'ex_probacionista' => 'Ex Probacionista',
  'ex_miembro' => 'Ex Miembro',
);
$GLOBALS['app_list_strings']['categoria_filosofia_list']=array (
  'sin_categoria' => '',
  'invitar_actividades' => 'Invitar Actividades',
  'invitar_charla_refuerzo' => 'Invitar Charla Refuerzo',
  'invitar_clase_presentacion' => 'Invitar Clase Presentación',
  'invitar_curso_filosofia' => 'Invitar curso Filosofía',
  'personalizado' => 'Invitación personalizada',
);

$GLOBALS['app_list_strings']['categoria_interesado_list']=array (
  'bd_antigua' => 'BD Antigua',
  'actividades' => 'Actividades',
  'consultas' => 'Consultas',
  '' => '',
  'voluntariado' => 'Voluntariado',
);
$GLOBALS['app_list_strings']['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Cold Call',
  'Existing Customer' => 'Existing Customer',
  'Self Generated' => 'Self Generated',
  'Public Relations' => 'Public Relations',
  'Direct Mail' => 'Direct Mail',
  'Conference' => 'Conference',
  'Trade Show' => 'Trade Show',
  'Web Site' => 'Web Site',
  'Word of mouth' => 'Word of mouth',
  'Email' => 'Email',
  'Campaign' => 'Campaign',
  'Other' => 'Other',
  'Fil' => 'FIL',
  'contacto_personal_externo' => 'Contacto Personal Externo',
  'bd_antigua' => 'BD Antigua',
);
$GLOBALS['app_list_strings']['lead_status_dom']=array (
  '' => '',
  'New' => 'New',
  'Assigned' => 'Assigned',
  'Converted' => 'Converted',
  'Dead' => 'Dead',
  'bd_na' => 'Base de datos NA',
  'baja_definitiva' => 'Baja definitiva',
  'baja_solicitada' => 'Baja Solicitada',
);