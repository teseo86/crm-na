<?php
// created: 2017-05-06 06:04:56
$GLOBALS['tabStructure'] = array (
  'LBL_TABGROUP_SALES' => 
  array (
    'label' => 'LBL_TABGROUP_SALES',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'Accounts',
      2 => 'Contacts',
      3 => 'Opportunities',
      4 => 'Leads',
    ),
  ),
  'LBL_TABGROUP_MARKETING' => 
  array (
    'label' => 'LBL_TABGROUP_MARKETING',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'Accounts',
      2 => 'Contacts',
      3 => 'Leads',
      4 => 'Campaigns',
      5 => 'Prospects',
      6 => 'ProspectLists',
    ),
  ),
  'LBL_TABGROUP_SUPPORT' => 
  array (
    'label' => 'LBL_TABGROUP_SUPPORT',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'Accounts',
      2 => 'Contacts',
      3 => 'Cases',
      4 => 'Bugs',
    ),
  ),
  'LBL_TABGROUP_ACTIVITIES' => 
  array (
    'label' => 'LBL_TABGROUP_ACTIVITIES',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'Calendar',
      2 => 'Calls',
      3 => 'Meetings',
      4 => 'HER01_SMS',
      5 => 'Emails',
      6 => 'Tasks',
      7 => 'Notes',
    ),
  ),
  'LBL_TABGROUP_COLLABORATION' => 
  array (
    'label' => 'LBL_TABGROUP_COLLABORATION',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'Emails',
      2 => 'Documents',
      3 => 'Project',
    ),
  ),
);