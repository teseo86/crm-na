<?php
// created: 2017-08-24 16:13:23
$dictionary["Lead"]["fields"]["smart_consulta_leads"] = array (
  'name' => 'smart_consulta_leads',
  'type' => 'link',
  'relationship' => 'smart_consulta_leads',
  'source' => 'non-db',
  'module' => 'smart_Consulta',
  'bean_name' => false,
  'side' => 'right',
  'vname' => 'LBL_SMART_CONSULTA_LEADS_FROM_SMART_CONSULTA_TITLE',
);
