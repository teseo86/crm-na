<?php
// created: 2017-08-24 16:13:23
$dictionary["Contact"]["fields"]["smart_consulta_contacts"] = array (
  'name' => 'smart_consulta_contacts',
  'type' => 'link',
  'relationship' => 'smart_consulta_contacts',
  'source' => 'non-db',
  'module' => 'smart_Consulta',
  'bean_name' => false,
  'side' => 'right',
  'vname' => 'LBL_SMART_CONSULTA_CONTACTS_FROM_SMART_CONSULTA_TITLE',
);
