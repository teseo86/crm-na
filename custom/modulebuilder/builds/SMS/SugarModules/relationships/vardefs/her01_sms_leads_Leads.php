<?php
// created: 2017-05-06 15:48:47
$dictionary["Lead"]["fields"]["her01_sms_leads"] = array (
  'name' => 'her01_sms_leads',
  'type' => 'link',
  'relationship' => 'her01_sms_leads',
  'source' => 'non-db',
  'module' => 'HER01_SMS',
  'bean_name' => 'HER01_SMS',
  'vname' => 'LBL_HER01_SMS_LEADS_FROM_HER01_SMS_TITLE',
);
