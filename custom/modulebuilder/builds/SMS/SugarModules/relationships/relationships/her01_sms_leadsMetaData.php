<?php
// created: 2017-05-06 15:48:47
$dictionary["her01_sms_leads"] = array (
  'true_relationship_type' => 'many-to-many',
  'relationships' => 
  array (
    'her01_sms_leads' => 
    array (
      'lhs_module' => 'HER01_SMS',
      'lhs_table' => 'her01_sms',
      'lhs_key' => 'id',
      'rhs_module' => 'Leads',
      'rhs_table' => 'leads',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'her01_sms_leads_c',
      'join_key_lhs' => 'her01_sms_leadsher01_sms_ida',
      'join_key_rhs' => 'her01_sms_leadsleads_idb',
    ),
  ),
  'table' => 'her01_sms_leads_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'her01_sms_leadsher01_sms_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'her01_sms_leadsleads_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'her01_sms_leadsspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'her01_sms_leads_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'her01_sms_leadsher01_sms_ida',
        1 => 'her01_sms_leadsleads_idb',
      ),
    ),
  ),
);