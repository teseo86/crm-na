<?php
 // created: 2017-05-06 15:48:47
$layout_defs["HER01_SMS"]["subpanel_setup"]['her01_sms_leads'] = array (
  'order' => 100,
  'module' => 'Leads',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_HER01_SMS_LEADS_FROM_LEADS_TITLE',
  'get_subpanel_data' => 'her01_sms_leads',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
