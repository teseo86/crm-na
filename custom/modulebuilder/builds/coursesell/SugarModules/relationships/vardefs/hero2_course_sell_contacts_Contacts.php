<?php
// created: 2017-05-08 17:18:41
$dictionary["Contact"]["fields"]["hero2_course_sell_contacts"] = array (
  'name' => 'hero2_course_sell_contacts',
  'type' => 'link',
  'relationship' => 'hero2_course_sell_contacts',
  'source' => 'non-db',
  'module' => 'hero2_course_sell',
  'bean_name' => false,
  'side' => 'right',
  'vname' => 'LBL_HERO2_COURSE_SELL_CONTACTS_FROM_HERO2_COURSE_SELL_TITLE',
);
