<?php
// created: 2017-05-08 17:18:41
$dictionary["hero2_course_sell"]["fields"]["hero2_course_sell_contacts"] = array (
  'name' => 'hero2_course_sell_contacts',
  'type' => 'link',
  'relationship' => 'hero2_course_sell_contacts',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_HERO2_COURSE_SELL_CONTACTS_FROM_CONTACTS_TITLE',
  'id_name' => 'hero2_course_sell_contactscontacts_ida',
);
$dictionary["hero2_course_sell"]["fields"]["hero2_course_sell_contacts_name"] = array (
  'name' => 'hero2_course_sell_contacts_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_HERO2_COURSE_SELL_CONTACTS_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'hero2_course_sell_contactscontacts_ida',
  'link' => 'hero2_course_sell_contacts',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["hero2_course_sell"]["fields"]["hero2_course_sell_contactscontacts_ida"] = array (
  'name' => 'hero2_course_sell_contactscontacts_ida',
  'type' => 'link',
  'relationship' => 'hero2_course_sell_contacts',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_HERO2_COURSE_SELL_CONTACTS_FROM_HERO2_COURSE_SELL_TITLE',
);
