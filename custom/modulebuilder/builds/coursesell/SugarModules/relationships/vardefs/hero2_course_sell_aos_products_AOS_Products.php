<?php
// created: 2017-05-08 17:18:41
$dictionary["AOS_Products"]["fields"]["hero2_course_sell_aos_products"] = array (
  'name' => 'hero2_course_sell_aos_products',
  'type' => 'link',
  'relationship' => 'hero2_course_sell_aos_products',
  'source' => 'non-db',
  'module' => 'hero2_course_sell',
  'bean_name' => false,
  'side' => 'right',
  'vname' => 'LBL_HERO2_COURSE_SELL_AOS_PRODUCTS_FROM_HERO2_COURSE_SELL_TITLE',
);
