<?php
// created: 2017-05-08 17:18:41
$dictionary["hero2_course_sell"]["fields"]["hero2_course_sell_aos_products"] = array (
  'name' => 'hero2_course_sell_aos_products',
  'type' => 'link',
  'relationship' => 'hero2_course_sell_aos_products',
  'source' => 'non-db',
  'module' => 'AOS_Products',
  'bean_name' => 'AOS_Products',
  'vname' => 'LBL_HERO2_COURSE_SELL_AOS_PRODUCTS_FROM_AOS_PRODUCTS_TITLE',
  'id_name' => 'hero2_course_sell_aos_productsaos_products_ida',
);
$dictionary["hero2_course_sell"]["fields"]["hero2_course_sell_aos_products_name"] = array (
  'name' => 'hero2_course_sell_aos_products_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_HERO2_COURSE_SELL_AOS_PRODUCTS_FROM_AOS_PRODUCTS_TITLE',
  'save' => true,
  'id_name' => 'hero2_course_sell_aos_productsaos_products_ida',
  'link' => 'hero2_course_sell_aos_products',
  'table' => 'aos_products',
  'module' => 'AOS_Products',
  'rname' => 'name',
);
$dictionary["hero2_course_sell"]["fields"]["hero2_course_sell_aos_productsaos_products_ida"] = array (
  'name' => 'hero2_course_sell_aos_productsaos_products_ida',
  'type' => 'link',
  'relationship' => 'hero2_course_sell_aos_products',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_HERO2_COURSE_SELL_AOS_PRODUCTS_FROM_HERO2_COURSE_SELL_TITLE',
);
