<?php
 // created: 2017-05-08 17:18:41
$layout_defs["AOS_Products"]["subpanel_setup"]['hero2_course_sell_aos_products'] = array (
  'order' => 100,
  'module' => 'hero2_course_sell',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_HERO2_COURSE_SELL_AOS_PRODUCTS_FROM_HERO2_COURSE_SELL_TITLE',
  'get_subpanel_data' => 'hero2_course_sell_aos_products',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
