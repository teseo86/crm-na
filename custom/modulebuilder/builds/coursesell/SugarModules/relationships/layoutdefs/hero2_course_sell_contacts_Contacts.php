<?php
 // created: 2017-05-08 17:18:41
$layout_defs["Contacts"]["subpanel_setup"]['hero2_course_sell_contacts'] = array (
  'order' => 100,
  'module' => 'hero2_course_sell',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_HERO2_COURSE_SELL_CONTACTS_FROM_HERO2_COURSE_SELL_TITLE',
  'get_subpanel_data' => 'hero2_course_sell_contacts',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
