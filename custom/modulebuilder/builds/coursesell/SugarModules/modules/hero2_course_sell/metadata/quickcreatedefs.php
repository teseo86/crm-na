<?php
$module_name = 'hero2_course_sell';
$viewdefs [$module_name] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 'assigned_user_name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'description',
            'comment' => 'Full text of the note',
            'label' => 'LBL_DESCRIPTION',
          ),
          1 => 
          array (
            'name' => 'sale_date',
            'label' => 'LBL_SALE_DATE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'hero2_course_sell_aos_products_name',
            'label' => 'LBL_HERO2_COURSE_SELL_AOS_PRODUCTS_FROM_AOS_PRODUCTS_TITLE',
          ),
          1 => 
          array (
            'name' => 'hero2_course_sell_contacts_name',
            'label' => 'LBL_HERO2_COURSE_SELL_CONTACTS_FROM_CONTACTS_TITLE',
          ),
        ),
      ),
    ),
  ),
);
?>
