<?php
$module_name = 'hero2_course_sell';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'SALE_DATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_SALE_DATE',
    'width' => '10%',
    'default' => true,
  ),
  'HERO2_COURSE_SELL_CONTACTS_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_HERO2_COURSE_SELL_CONTACTS_FROM_CONTACTS_TITLE',
    'id' => 'HERO2_COURSE_SELL_CONTACTSCONTACTS_IDA',
    'width' => '10%',
    'default' => true,
  ),
  'HERO2_COURSE_SELL_AOS_PRODUCTS_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_HERO2_COURSE_SELL_AOS_PRODUCTS_FROM_AOS_PRODUCTS_TITLE',
    'id' => 'HERO2_COURSE_SELL_AOS_PRODUCTSAOS_PRODUCTS_IDA',
    'width' => '10%',
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
);
?>
