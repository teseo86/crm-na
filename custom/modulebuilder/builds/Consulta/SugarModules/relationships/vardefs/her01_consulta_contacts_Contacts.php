<?php
// created: 2017-05-31 17:18:07
$dictionary["Contact"]["fields"]["her01_consulta_contacts"] = array (
  'name' => 'her01_consulta_contacts',
  'type' => 'link',
  'relationship' => 'her01_consulta_contacts',
  'source' => 'non-db',
  'module' => 'HER01_Consulta',
  'bean_name' => false,
  'side' => 'right',
  'vname' => 'LBL_HER01_CONSULTA_CONTACTS_FROM_HER01_CONSULTA_TITLE',
);
