<?php
// created: 2017-05-31 17:13:18
$dictionary["HER01_SMS"]["fields"]["her01_sms_leads"] = array (
  'name' => 'her01_sms_leads',
  'type' => 'link',
  'relationship' => 'her01_sms_leads',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_HER01_SMS_LEADS_FROM_LEADS_TITLE',
);
