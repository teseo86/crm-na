<?php
// created: 2017-05-31 17:18:07
$dictionary["Lead"]["fields"]["her01_consulta_leads"] = array (
  'name' => 'her01_consulta_leads',
  'type' => 'link',
  'relationship' => 'her01_consulta_leads',
  'source' => 'non-db',
  'module' => 'HER01_Consulta',
  'bean_name' => false,
  'side' => 'right',
  'vname' => 'LBL_HER01_CONSULTA_LEADS_FROM_HER01_CONSULTA_TITLE',
);
