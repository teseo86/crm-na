<?php
// created: 2017-05-31 17:18:07
$dictionary["HER01_Consulta"]["fields"]["her01_consulta_contacts"] = array (
  'name' => 'her01_consulta_contacts',
  'type' => 'link',
  'relationship' => 'her01_consulta_contacts',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_HER01_CONSULTA_CONTACTS_FROM_CONTACTS_TITLE',
  'id_name' => 'her01_consulta_contactscontacts_ida',
);
$dictionary["HER01_Consulta"]["fields"]["her01_consulta_contacts_name"] = array (
  'name' => 'her01_consulta_contacts_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_HER01_CONSULTA_CONTACTS_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'her01_consulta_contactscontacts_ida',
  'link' => 'her01_consulta_contacts',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["HER01_Consulta"]["fields"]["her01_consulta_contactscontacts_ida"] = array (
  'name' => 'her01_consulta_contactscontacts_ida',
  'type' => 'link',
  'relationship' => 'her01_consulta_contacts',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_HER01_CONSULTA_CONTACTS_FROM_HER01_CONSULTA_TITLE',
);
