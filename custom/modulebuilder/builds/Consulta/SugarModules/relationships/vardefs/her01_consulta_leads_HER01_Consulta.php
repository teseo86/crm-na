<?php
// created: 2017-05-31 17:18:07
$dictionary["HER01_Consulta"]["fields"]["her01_consulta_leads"] = array (
  'name' => 'her01_consulta_leads',
  'type' => 'link',
  'relationship' => 'her01_consulta_leads',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_HER01_CONSULTA_LEADS_FROM_LEADS_TITLE',
  'id_name' => 'her01_consulta_leadsleads_ida',
);
$dictionary["HER01_Consulta"]["fields"]["her01_consulta_leads_name"] = array (
  'name' => 'her01_consulta_leads_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_HER01_CONSULTA_LEADS_FROM_LEADS_TITLE',
  'save' => true,
  'id_name' => 'her01_consulta_leadsleads_ida',
  'link' => 'her01_consulta_leads',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["HER01_Consulta"]["fields"]["her01_consulta_leadsleads_ida"] = array (
  'name' => 'her01_consulta_leadsleads_ida',
  'type' => 'link',
  'relationship' => 'her01_consulta_leads',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_HER01_CONSULTA_LEADS_FROM_HER01_CONSULTA_TITLE',
);
