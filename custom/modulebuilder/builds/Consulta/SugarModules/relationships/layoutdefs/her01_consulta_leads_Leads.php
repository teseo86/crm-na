<?php
 // created: 2017-05-31 17:18:07
$layout_defs["Leads"]["subpanel_setup"]['her01_consulta_leads'] = array (
  'order' => 100,
  'module' => 'HER01_Consulta',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_HER01_CONSULTA_LEADS_FROM_HER01_CONSULTA_TITLE',
  'get_subpanel_data' => 'her01_consulta_leads',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
