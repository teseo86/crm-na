<?php
// created: 2017-05-31 17:18:07
$dictionary["her01_consulta_contacts"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'her01_consulta_contacts' => 
    array (
      'lhs_module' => 'Contacts',
      'lhs_table' => 'contacts',
      'lhs_key' => 'id',
      'rhs_module' => 'HER01_Consulta',
      'rhs_table' => 'her01_consulta',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'her01_consulta_contacts_c',
      'join_key_lhs' => 'her01_consulta_contactscontacts_ida',
      'join_key_rhs' => 'her01_consulta_contactsher01_consulta_idb',
    ),
  ),
  'table' => 'her01_consulta_contacts_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'her01_consulta_contactscontacts_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'her01_consulta_contactsher01_consulta_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'her01_consulta_contactsspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'her01_consulta_contacts_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'her01_consulta_contactscontacts_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'her01_consulta_contacts_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'her01_consulta_contactsher01_consulta_idb',
      ),
    ),
  ),
);