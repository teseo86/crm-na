<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-02-16 20:11:38
$dictionary['Lead']['fields']['e_registry_fields']=
                array(
                    'name' => 'e_registry_fields',
                    'rname' => 'id',
                    'relationship_fields' =>
                        array(
                            'id' => 'registry_channel_id',
                            'registry_channel' => 'registry_channel_name',
                             'how_to_know' => 'how_to_know',
                              'how_to_know_count' => 'how_to_know_count',
                              'id_reservation' => 'id_reservation',
                              'datetime_reservation' => 'datetime_reservation',
                              'description' => 'description',
                            
                        ),
                    'vname' => 'LBL_REGISTRY_CHANNEL',
                    'type' => 'relate',
					'link' => 'fp_events_leads_1',
					'link_type' => 'relationship_info',
					'join_link_name' => 'fp_events_leads_1',
					'source' => 'non-db',
                    'importable' => 'false',
                    'duplicate_merge' => 'disabled',
                    'studio' => false,
                );
       $dictionary['Lead']['fields']['registry_channel_name']=
                array(
                    'massupdate' => false,
                    'name' => 'registry_channel_name',
                    'type' => 'enum',
                    'studio' => 'false',
                    'source' => 'non-db',
                    'vname' => 'LBL_LIST_REGISTRY_CHANNEL',
                    'options' => 'fp_event_registry_channel_dom',
                    'importable' => 'false',
                );
           $dictionary['Lead']['fields']['registry_channel_id']=
                array(
                    'name' => 'registry_channel_id',
                    'type' => 'varchar',
                    'source' => 'non-db',
                    'vname' => 'LBL_LIST_REGISTRY_CHANNEL',
                    'studio' =>
                        array(
                            'listview' => false,
                        ),
                );
                
                 $dictionary['Lead']['fields']['how_to_know']=
                array(
                    'massupdate' => false,
                    'name' => 'how_to_know',
                    'type' => 'varchar',
                    'studio' => 'false',
                    'source' => 'non-db',
                    'vname' => 'LBL_HOW_TO_KNOW',
                   'importable' => 'false',
                );
                $dictionary['Lead']['fields']['how_to_know_count']=
                array(
                    'massupdate' => false,
                    'name' => 'how_to_know_count',
                    'type' => 'int',
                    'studio' => 'false',
                    'source' => 'non-db',
                    'vname' => 'LBL_HOW_TO_KNOW',
                   'importable' => 'false',
                );
                 $dictionary['Lead']['fields']['id_reservation']=
                array(
                    'massupdate' => false,
                    'name' => 'id_reservation',
                    'type' => 'varchar',
                    'studio' => 'false',
                    'source' => 'non-db',
                    'vname' => 'LBL_ID_RESERVATION',
                   'importable' => 'false',
                );
                $dictionary['Lead']['fields']['datetime_reservation']=
                array(
                    'massupdate' => false,
                    'name' => 'datetime_reservation',
                    'type' => 'datetime',
                    'studio' => 'false',
                    'source' => 'non-db',
                    'vname' => 'LBL_DATETIME_RESERVATION',
                   'importable' => 'false',
                );
                $dictionary['Lead']['fields']['description']=
                array(
                    'massupdate' => false,
                    'name' => 'description',
                    'type' => 'text',
                    'studio' => 'false',
                    'source' => 'non-db',
                    'vname' => 'LBL_DESCRIPTION',
                   'importable' => 'false',
                );

 


// created: 2017-07-19 07:10:12
$dictionary["Lead"]["fields"]["opportunities_leads_1"] = array (
  'name' => 'opportunities_leads_1',
  'type' => 'link',
  'relationship' => 'opportunities_leads_1',
  'source' => 'non-db',
  'module' => 'Opportunities',
  'bean_name' => 'Opportunity',
  'vname' => 'LBL_OPPORTUNITIES_LEADS_1_FROM_OPPORTUNITIES_TITLE',
);


// created: 2017-08-24 16:13:23
$dictionary["Lead"]["fields"]["smart_consulta_leads"] = array (
  'name' => 'smart_consulta_leads',
  'type' => 'link',
  'relationship' => 'smart_consulta_leads',
  'source' => 'non-db',
  'module' => 'smart_Consulta',
  'bean_name' => false,
  'side' => 'right',
  'vname' => 'LBL_SMART_CONSULTA_LEADS_FROM_SMART_CONSULTA_TITLE',
);


 // created: 2017-03-08 03:36:46
$dictionary['Lead']['fields']['activities_c']['inline_edit']='1';
$dictionary['Lead']['fields']['activities_c']['labelValue']='Activities';

 

 // created: 2017-03-08 03:37:02
$dictionary['Lead']['fields']['age_c']['inline_edit']='1';
$dictionary['Lead']['fields']['age_c']['labelValue']='Age';

 

 // created: 2017-03-08 03:41:13
$dictionary['Lead']['fields']['age_contacto_na_c']['inline_edit']='1';
$dictionary['Lead']['fields']['age_contacto_na_c']['labelValue']='Years contact in NA';

 

 // created: 2017-03-06 21:18:44
$dictionary['Lead']['fields']['birthdate']['audited']=true;
$dictionary['Lead']['fields']['birthdate']['inline_edit']=true;
$dictionary['Lead']['fields']['birthdate']['comments']='The birthdate of the contact';
$dictionary['Lead']['fields']['birthdate']['merge_filter']='disabled';
$dictionary['Lead']['fields']['birthdate']['enable_range_search']=false;

 

 // created: 2017-02-21 17:40:50
$dictionary['Lead']['fields']['boletin_c']['inline_edit']='1';
$dictionary['Lead']['fields']['boletin_c']['labelValue']='Boletín';

 

 // created: 2017-02-21 17:41:11
$dictionary['Lead']['fields']['boletin_cursos_c']['inline_edit']='1';
$dictionary['Lead']['fields']['boletin_cursos_c']['labelValue']='Boletín Cursos';

 

 // created: 2017-02-21 15:56:01
$dictionary['Lead']['fields']['courses_c']['inline_edit']='1';
$dictionary['Lead']['fields']['courses_c']['labelValue']='Cursos';

 

 // created: 2017-03-06 22:08:13
$dictionary['Lead']['fields']['date_modified']['comments']='Date record last modified';
$dictionary['Lead']['fields']['date_modified']['merge_filter']='disabled';

 

 // created: 2017-02-21 21:03:45
$dictionary['Lead']['fields']['document_number_c']['inline_edit']='1';
$dictionary['Lead']['fields']['document_number_c']['labelValue']='Documento de Identidad';

 

 // created: 2017-03-06 21:19:16
$dictionary['Lead']['fields']['email1']['audited']=true;
$dictionary['Lead']['fields']['email1']['inline_edit']=true;
$dictionary['Lead']['fields']['email1']['merge_filter']='disabled';

 

 // created: 2017-03-08 03:39:23
$dictionary['Lead']['fields']['envio_email_c']['inline_edit']='1';
$dictionary['Lead']['fields']['envio_email_c']['labelValue']='Preference sending mail';

 

 // created: 2017-04-22 17:15:58
$dictionary['Lead']['fields']['facebook_user_c']['inline_edit']=1;
$dictionary['Lead']['fields']['facebook_user_c']['duplicate_merge_dom_value']=0;

 

 // created: 2017-03-06 21:20:12
$dictionary['Lead']['fields']['first_name']['audited']=true;
$dictionary['Lead']['fields']['first_name']['inline_edit']=true;
$dictionary['Lead']['fields']['first_name']['comments']='First name of the contact';
$dictionary['Lead']['fields']['first_name']['merge_filter']='disabled';

 

 // created: 2017-03-08 03:37:23
$dictionary['Lead']['fields']['gender_c']['inline_edit']='1';
$dictionary['Lead']['fields']['gender_c']['labelValue']='Gender';

 

 // created: 2017-03-27 03:25:19
$dictionary['Lead']['fields']['id_persona_bd_c']['inline_edit']='1';
$dictionary['Lead']['fields']['id_persona_bd_c']['labelValue']='Id Persona BD';

 

 // created: 2017-02-13 04:27:52
$dictionary['Lead']['fields']['jjwg_maps_address_c']['inline_edit']=1;

 

 // created: 2017-02-13 04:27:52
$dictionary['Lead']['fields']['jjwg_maps_geocode_status_c']['inline_edit']=1;

 

 // created: 2017-02-13 04:27:52
$dictionary['Lead']['fields']['jjwg_maps_lat_c']['inline_edit']=1;

 

 // created: 2017-02-13 04:27:51
$dictionary['Lead']['fields']['jjwg_maps_lng_c']['inline_edit']=1;

 

 // created: 2017-03-06 21:18:04
$dictionary['Lead']['fields']['last_name']['audited']=true;
$dictionary['Lead']['fields']['last_name']['inline_edit']=true;
$dictionary['Lead']['fields']['last_name']['comments']='Last name of the contact';
$dictionary['Lead']['fields']['last_name']['merge_filter']='disabled';

 

 // created: 2017-09-10 15:43:52
$dictionary['Lead']['fields']['lead_source']['inline_edit']=true;
$dictionary['Lead']['fields']['lead_source']['comments']='Lead source (ex: Web, print)';
$dictionary['Lead']['fields']['lead_source']['merge_filter']='disabled';

 

 // created: 2017-03-06 21:18:59
$dictionary['Lead']['fields']['list_courses_interest_c']['inline_edit']='1';
$dictionary['Lead']['fields']['list_courses_interest_c']['labelValue']='Cursos de Interés';

 

 // created: 2017-02-21 15:54:43
$dictionary['Lead']['fields']['make_call_c']['inline_edit']='1';
$dictionary['Lead']['fields']['make_call_c']['labelValue']='Contactarse por Teléfono';

 

 // created: 2017-03-06 21:17:13
$dictionary['Lead']['fields']['nearest_location_c']['inline_edit']='1';
$dictionary['Lead']['fields']['nearest_location_c']['labelValue']='Sede Cercana';

 

 // created: 2017-03-08 03:40:45
$dictionary['Lead']['fields']['no_email_c']['inline_edit']='1';
$dictionary['Lead']['fields']['no_email_c']['labelValue']='Dont email';

 

 // created: 2017-02-21 15:55:42
$dictionary['Lead']['fields']['philosophy_c']['inline_edit']='1';
$dictionary['Lead']['fields']['philosophy_c']['labelValue']='Filosofía';

 

 // created: 2017-02-21 17:45:52
$dictionary['Lead']['fields']['phone_contact_c']['inline_edit']='1';
$dictionary['Lead']['fields']['phone_contact_c']['labelValue']='Preferencia contacto por teléfono';

 

 // created: 2017-03-06 21:21:43
$dictionary['Lead']['fields']['phone_home']['audited']=true;
$dictionary['Lead']['fields']['phone_home']['inline_edit']=true;
$dictionary['Lead']['fields']['phone_home']['comments']='Home phone number of the contact';
$dictionary['Lead']['fields']['phone_home']['merge_filter']='disabled';

 

 // created: 2017-03-06 21:21:32
$dictionary['Lead']['fields']['phone_mobile']['audited']=true;
$dictionary['Lead']['fields']['phone_mobile']['inline_edit']=true;
$dictionary['Lead']['fields']['phone_mobile']['comments']='Mobile phone number of the contact';
$dictionary['Lead']['fields']['phone_mobile']['merge_filter']='disabled';

 

 // created: 2017-02-21 17:25:26
$dictionary['Lead']['fields']['preferred_calling_time_c']['inline_edit']='1';
$dictionary['Lead']['fields']['preferred_calling_time_c']['labelValue']='Horario preferido Llamada';

 

 // created: 2017-08-31 16:37:48
$dictionary['Lead']['fields']['rol_c']['inline_edit']='1';
$dictionary['Lead']['fields']['rol_c']['labelValue']='Rol';

 

 // created: 2017-09-10 19:32:47
$dictionary['Lead']['fields']['status']['inline_edit']=true;
$dictionary['Lead']['fields']['status']['comments']='Status of the lead';
$dictionary['Lead']['fields']['status']['merge_filter']='disabled';

 

 // created: 2017-03-08 03:41:43
$dictionary['Lead']['fields']['volunteering_c']['inline_edit']='1';
$dictionary['Lead']['fields']['volunteering_c']['labelValue']='Volunteering';

 
?>