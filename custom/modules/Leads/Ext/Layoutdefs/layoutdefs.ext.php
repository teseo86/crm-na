<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-05-02 00:40:46
$layout_defs["Leads"]["subpanel_setup"]['fp_events_leads_1'] = array (
  'order' => 100,
  'module' => 'FP_events',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_FP_EVENTS_LEADS_1_FROM_FP_EVENTS_TITLE',
  'get_subpanel_data' => 'fp_events_leads_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopCreateButton',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopEventSelectButton',
     'mode' => 'MultiSelect',
    
     
    ),
  ),
);


 // created: 2017-07-19 07:10:12
$layout_defs["Leads"]["subpanel_setup"]['opportunities_leads_1'] = array (
  'order' => 100,
  'module' => 'Opportunities',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_OPPORTUNITIES_LEADS_1_FROM_OPPORTUNITIES_TITLE',
  'get_subpanel_data' => 'opportunities_leads_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2017-08-24 16:13:23
$layout_defs["Leads"]["subpanel_setup"]['smart_consulta_leads'] = array (
  'order' => 100,
  'module' => 'smart_Consulta',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SMART_CONSULTA_LEADS_FROM_SMART_CONSULTA_TITLE',
  'get_subpanel_data' => 'smart_consulta_leads',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>