<?php
// created: 2017-03-08 03:41:43
$mod_strings = array (
  'LBL_REGISTRY_CHANNEL' => 'Registry Channel',
  'LBL_DOCUMENT_NUMBER' => 'document number',
  'LBL_HOW_TO_KNOW' => 'How to Know About Event',
  'LBL_HOW_TO_KNOW_COUNT' => 'Count',
  'LBL_ACTIVITIES' => 'Activities',
  'LBL_AGE' => 'Age',
  'LBL_GENDER' => 'Gender',
  'LBL_NO_EMAIL' => 'Dont email',
  'LBL_ENVIO_EMAIL' => 'Preference sending mail',
  'LBL_AGE_CONTACTO_NA' => 'Years contact in NA',
  'LBL_VOLUNTEERING' => 'Volunteering',
   'LBL_ID_RESERVATION'=>'Reservation Id',
    'LBL_DATETIME_RESERVATION'=>'Reservation Date Time',
    'LBL_DESCRIPTION'=>'Description',
);
