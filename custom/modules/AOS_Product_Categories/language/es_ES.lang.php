<?php
// created: 2017-05-09 10:28:32
$mod_strings = array (
  'LBL_AOS_PRODUCT_CATEGORIES_AOS_PRODUCTS_FROM_AOS_PRODUCTS_TITLE' => 'Título del producto',
  'LNK_NEW_RECORD' => 'Crear Categorías de Producto',
  'LNK_LIST' => 'Ver Categorías de Producto',
  'LNK_IMPORT_AOS_PRODUCT_CATEGORIES' => 'Importar Categorías de Producto',
  'LBL_LIST_FORM_TITLE' => 'Lista de Categorías de Producto',
  'LBL_SEARCH_FORM_TITLE' => 'Buscar Categorías de Producto',
  'LBL_PRODUCT_CATEGORYS_NAME' => 'Categoría Padre',
  'LBL_SUB_CATEGORIES' => 'Sub categorías',
  'LBL_PARENT_CATEGORY' => 'Categoría Padre',
  'LBL_HOMEPAGE_TITLE' => 'Mis Categorías de Producto',
);