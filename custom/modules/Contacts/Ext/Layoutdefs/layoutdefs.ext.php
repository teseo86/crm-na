<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-06-27 12:49:26
$layout_defs["Contacts"]["subpanel_setup"]['contacts_aos_products_1'] = array (
  'order' => 100,
  'module' => 'AOS_Products',
  'subpanel_name' => 'ForCustomersPurchasedProducts',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CONTACTS_AOS_PRODUCTS_1_FROM_AOS_PRODUCTS_TITLE',
  'get_subpanel_data' => 'contacts_aos_products_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2017-05-02 00:40:46
$layout_defs["Contacts"]["subpanel_setup"]['fp_events_contacts'] = array (
  'order' => 100,
  'module' => 'FP_events',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_FP_EVENTS_CONTACTS_FROM_FP_EVENTS_TITLE',
  'get_subpanel_data' => 'fp_events_contacts',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopCreateButton',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopEventSelectButton',
     'mode' => 'MultiSelect',
    
     
    ),
  ),
);




 // created: 2017-08-24 16:13:23
$layout_defs["Contacts"]["subpanel_setup"]['smart_consulta_contacts'] = array (
  'order' => 100,
  'module' => 'smart_Consulta',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SMART_CONSULTA_CONTACTS_FROM_SMART_CONSULTA_TITLE',
  'get_subpanel_data' => 'smart_consulta_contacts',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


//auto-generated file DO NOT EDIT
$layout_defs['Contacts']['subpanel_setup']['contacts_aos_products_1']['override_subpanel_name'] = 'Contact_subpanel_contacts_aos_products_1';


//auto-generated file DO NOT EDIT
$layout_defs['Contacts']['subpanel_setup']['hero2_course_sell_contacts']['override_subpanel_name'] = 'Contact_subpanel_hero2_course_sell_contacts';


//auto-generated file DO NOT EDIT
$layout_defs['Contacts']['subpanel_setup']['opportunities']['override_subpanel_name'] = 'Contact_subpanel_Opportunities';

?>