<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2017-06-27 12:49:26
$dictionary["Contact"]["fields"]["contacts_aos_products_1"] = array (
  'name' => 'contacts_aos_products_1',
  'type' => 'link',
  'relationship' => 'contacts_aos_products_1',
  'source' => 'non-db',
  'module' => 'AOS_Products',
  'bean_name' => 'AOS_Products',
  'vname' => 'LBL_CONTACTS_AOS_PRODUCTS_1_FROM_AOS_PRODUCTS_TITLE',
);


 // created: 2017-02-16 20:11:38
$dictionary['Contact']['fields']['e_registry_fields']=
                array(
                    'name' => 'e_registry_fields',
                    'rname' => 'id',
                    'relationship_fields' =>
                        array(
                            'id' => 'registry_channel_id',
                            'registry_channel' => 'registry_channel_name',
                             'how_to_know' => 'how_to_know',
                              'how_to_know_count' => 'how_to_know_count',
                              'id_reservation' => 'id_reservation',
                              'datetime_reservation' => 'datetime_reservation',
                              'description' => 'description',
                        ),
                    'vname' => 'LBL_REGISTRY_CHANNEL',
                    'type' => 'relate',
                    'link' => 'fp_events_contacts',
                    'link_type' => 'relationship_info',
                    'join_link_name' => 'fp_events_contacts',
                    'source' => 'non-db',
                    'importable' => 'false',
                    'duplicate_merge' => 'disabled',
                    'studio' => false,
                );
       $dictionary['Contact']['fields']['registry_channel_name']=
                array(
                    'massupdate' => false,
                    'name' => 'registry_channel_name',
                    'type' => 'enum',
                    'studio' => 'false',
                    'source' => 'non-db',
                    'vname' => 'LBL_LIST_REGISTRY_CHANNEL',
                    'options' => 'fp_event_registry_channel_dom',
                    'importable' => 'false',
                );
           $dictionary['Contact']['fields']['registry_channel_id']=
                array(
                    'name' => 'registry_channel_id',
                    'type' => 'varchar',
                    'source' => 'non-db',
                    'vname' => 'LBL_LIST_REGISTRY_CHANNEL',
                    'studio' =>
                        array(
                            'listview' => false,
                        ),
                );
                 $dictionary['Contact']['fields']['how_to_know']=
                array(
                    'massupdate' => false,
                    'name' => 'how_to_know',
                    'type' => 'varchar',
                    'studio' => 'false',
                    'source' => 'non-db',
                    'vname' => 'LBL_HOW_TO_KNOW',
                   'importable' => 'false',
                );
                $dictionary['Contact']['fields']['how_to_know_count']=
                array(
                    'massupdate' => false,
                    'name' => 'how_to_know_count',
                    'type' => 'int',
                    'studio' => 'false',
                    'source' => 'non-db',
                    'vname' => 'LBL_HOW_TO_KNOW',
                   'importable' => 'false',
                );
                $dictionary['Contact']['fields']['id_reservation']=
                array(
                    'massupdate' => false,
                    'name' => 'id_reservation',
                    'type' => 'varchar',
                    'studio' => 'false',
                    'source' => 'non-db',
                    'vname' => 'LBL_ID_RESERVATION',
                   'importable' => 'false',
                );
                $dictionary['Contact']['fields']['datetime_reservation']=
                array(
                    'massupdate' => false,
                    'name' => 'datetime_reservation',
                    'type' => 'datetime',
                    'studio' => 'false',
                    'source' => 'non-db',
                    'vname' => 'LBL_DATETIME_RESERVATION',
                   'importable' => 'false',
                );
                $dictionary['Contact']['fields']['description']=
                array(
                    'massupdate' => false,
                    'name' => 'description',
                    'type' => 'text',
                    'studio' => 'false',
                    'source' => 'non-db',
                    'vname' => 'LBL_DESCRIPTION',
                   'importable' => 'false',
                );
                


 


// created: 2017-08-24 16:13:23
$dictionary["Contact"]["fields"]["smart_consulta_contacts"] = array (
  'name' => 'smart_consulta_contacts',
  'type' => 'link',
  'relationship' => 'smart_consulta_contacts',
  'source' => 'non-db',
  'module' => 'smart_Consulta',
  'bean_name' => false,
  'side' => 'right',
  'vname' => 'LBL_SMART_CONSULTA_CONTACTS_FROM_SMART_CONSULTA_TITLE',
);


 // created: 2017-02-21 20:31:49
$dictionary['Contact']['fields']['activities_c']['inline_edit']='1';
$dictionary['Contact']['fields']['activities_c']['labelValue']='Actividades';

 

 // created: 2017-02-21 20:44:46
$dictionary['Contact']['fields']['age_c']['inline_edit']='1';
$dictionary['Contact']['fields']['age_c']['labelValue']='Edad';

 

 // created: 2017-03-23 19:51:50
$dictionary['Contact']['fields']['age_contacto_na_c']['inline_edit']='1';
$dictionary['Contact']['fields']['age_contacto_na_c']['labelValue']='Tiempo contacto (años)';

 

 // created: 2017-08-31 16:12:19
$dictionary['Contact']['fields']['alt_address_state']['inline_edit']=true;
$dictionary['Contact']['fields']['alt_address_state']['comments']='Ciudad / Departamento';
$dictionary['Contact']['fields']['alt_address_state']['merge_filter']='disabled';

 

 // created: 2017-03-23 19:47:03
$dictionary['Contact']['fields']['boletin_c']['inline_edit']='1';
$dictionary['Contact']['fields']['boletin_c']['labelValue']='Boletín';

 

 // created: 2017-03-23 19:47:36
$dictionary['Contact']['fields']['boletin_cursos_c']['inline_edit']='1';
$dictionary['Contact']['fields']['boletin_cursos_c']['labelValue']='Boletín Cursos';

 

 // created: 2017-08-28 16:15:05
$dictionary['Contact']['fields']['categoria_cliente_c']['inline_edit']='1';
$dictionary['Contact']['fields']['categoria_cliente_c']['labelValue']='Categoría Cliente';

 

 // created: 2017-08-31 16:07:23
$dictionary['Contact']['fields']['categoria_filosofia_c']['inline_edit']='1';
$dictionary['Contact']['fields']['categoria_filosofia_c']['labelValue']='Categoría Filosofía';

 

 // created: 2017-03-23 20:43:35
$dictionary['Contact']['fields']['courses_c']['inline_edit']='1';
$dictionary['Contact']['fields']['courses_c']['labelValue']='Cursos';

 

 // created: 2017-02-21 21:04:23
$dictionary['Contact']['fields']['document_number_c']['inline_edit']='1';
$dictionary['Contact']['fields']['document_number_c']['labelValue']='Documento Identidad';

 

 // created: 2017-03-23 20:58:53
$dictionary['Contact']['fields']['do_not_call']['inline_edit']=true;
$dictionary['Contact']['fields']['do_not_call']['comments']='An indicator of whether contact can be called';
$dictionary['Contact']['fields']['do_not_call']['merge_filter']='disabled';

 

 // created: 2017-03-23 19:50:00
$dictionary['Contact']['fields']['envio_email_c']['inline_edit']='1';
$dictionary['Contact']['fields']['envio_email_c']['labelValue']='Envío email';

 

 // created: 2017-04-22 17:15:59
$dictionary['Contact']['fields']['facebook_user_c']['inline_edit']=1;
$dictionary['Contact']['fields']['facebook_user_c']['duplicate_merge_dom_value']=0;

 

 // created: 2017-02-22 00:19:16
$dictionary['Contact']['fields']['fp_event_locations_id_c']['inline_edit']=1;

 

 // created: 2017-03-07 04:19:39
$dictionary['Contact']['fields']['gender_c']['inline_edit']='1';
$dictionary['Contact']['fields']['gender_c']['labelValue']='Género';

 

 // created: 2017-03-24 17:21:18
$dictionary['Contact']['fields']['id_persona_bd_c']['inline_edit']='1';
$dictionary['Contact']['fields']['id_persona_bd_c']['labelValue']='Id Persona BD';

 

 // created: 2017-02-13 04:27:51
$dictionary['Contact']['fields']['jjwg_maps_address_c']['inline_edit']=1;

 

 // created: 2017-02-13 04:27:51
$dictionary['Contact']['fields']['jjwg_maps_geocode_status_c']['inline_edit']=1;

 

 // created: 2017-02-13 04:27:51
$dictionary['Contact']['fields']['jjwg_maps_lat_c']['inline_edit']=1;

 

 // created: 2017-02-13 04:27:51
$dictionary['Contact']['fields']['jjwg_maps_lng_c']['inline_edit']=1;

 

 // created: 2017-03-23 19:56:11
$dictionary['Contact']['fields']['list_courses_interest_c']['inline_edit']='1';
$dictionary['Contact']['fields']['list_courses_interest_c']['labelValue']='Cursos de Interés';

 

 // created: 2017-03-23 19:51:15
$dictionary['Contact']['fields']['make_call_c']['inline_edit']='1';
$dictionary['Contact']['fields']['make_call_c']['labelValue']='Contactarse por teléfono';

 

 // created: 2017-08-28 16:42:26
$dictionary['Contact']['fields']['monto_acumulado_c']['inline_edit']='1';
$dictionary['Contact']['fields']['monto_acumulado_c']['labelValue']='Monto acumulado';

 

 // created: 2017-03-23 19:48:31
$dictionary['Contact']['fields']['no_email_c']['inline_edit']='1';
$dictionary['Contact']['fields']['no_email_c']['labelValue']='No envío correos';

 

 // created: 2017-08-25 10:30:01
$dictionary['Contact']['fields']['no_filosofia_c']['inline_edit']='1';
$dictionary['Contact']['fields']['no_filosofia_c']['labelValue']='No Filosofía';

 

 // created: 2017-08-28 16:42:05
$dictionary['Contact']['fields']['num_cursos_c']['inline_edit']='1';
$dictionary['Contact']['fields']['num_cursos_c']['labelValue']='# Cursos matriculados';

 

 // created: 2017-02-21 20:30:59
$dictionary['Contact']['fields']['philosophy_c']['inline_edit']='1';
$dictionary['Contact']['fields']['philosophy_c']['labelValue']='Filosofía';

 

 // created: 2017-03-23 19:55:15
$dictionary['Contact']['fields']['phone_contact_c']['inline_edit']='1';
$dictionary['Contact']['fields']['phone_contact_c']['labelValue']='Preferencia contacto por teléfono';

 

 // created: 2017-03-23 19:53:55
$dictionary['Contact']['fields']['phone_work']['inline_edit']=true;
$dictionary['Contact']['fields']['phone_work']['comments']='Work phone number of the contact';
$dictionary['Contact']['fields']['phone_work']['merge_filter']='disabled';

 

 // created: 2017-03-27 03:26:33
$dictionary['Contact']['fields']['photo']['inline_edit']=true;
$dictionary['Contact']['fields']['photo']['importable']='true';
$dictionary['Contact']['fields']['photo']['merge_filter']='disabled';
$dictionary['Contact']['fields']['photo']['border']='';

 

 // created: 2017-03-23 19:57:32
$dictionary['Contact']['fields']['preferred_calling_time_c']['inline_edit']='1';
$dictionary['Contact']['fields']['preferred_calling_time_c']['labelValue']='Horario preferido Llamada';

 

 // created: 2017-08-31 16:12:25
$dictionary['Contact']['fields']['primary_address_city']['inline_edit']=true;
$dictionary['Contact']['fields']['primary_address_city']['comments']='City for primary address';
$dictionary['Contact']['fields']['primary_address_city']['merge_filter']='disabled';

 

 // created: 2017-08-31 15:56:31
$dictionary['Contact']['fields']['puntos_filosofia_c']['inline_edit']='1';
$dictionary['Contact']['fields']['puntos_filosofia_c']['labelValue']='Puntos Filosofía';

 

 // created: 2017-02-22 00:19:16
$dictionary['Contact']['fields']['sede_c']['inline_edit']='1';
$dictionary['Contact']['fields']['sede_c']['labelValue']='Sede';

 

 // created: 2017-08-28 16:30:58
$dictionary['Contact']['fields']['tipo_contacto_c']['inline_edit']='1';
$dictionary['Contact']['fields']['tipo_contacto_c']['labelValue']='Tipo Contacto';

 

 // created: 2017-02-21 20:31:26
$dictionary['Contact']['fields']['volunteering_c']['inline_edit']='1';
$dictionary['Contact']['fields']['volunteering_c']['labelValue']='Voluntariado';

 
?>