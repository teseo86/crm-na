<?php
// created: 2017-05-08 17:21:14
$mod_strings = array (
  'LBL_REGISTRY_CHANNEL' => 'Registry Channel',
  'LBL_HOW_TO_KNOW' => 'How to Know About Event',
  'LBL_HOW_TO_KNOW_COUNT' => 'Count',
  'LBL_ID_RESERVATION' => 'Reservation Id',
  'LBL_DATETIME_RESERVATION' => 'Reservation Date Time',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_HERO2_COURSE_SELL_CONTACTS_FROM_HERO2_COURSE_SELL_TITLE' => 'Course Sell',
);