<?php
$popupMeta = array (
    'moduleMain' => 'AOS_Products',
    'varName' => 'AOS_Products',
    'orderBy' => 'aos_products.name',
    'whereClauses' => array (
  'name' => 'aos_products.name',
  'price' => 'aos_products.price',
  'created_by' => 'aos_products.created_by',
  'aos_product_category_name' => 'aos_products.aos_product_category_name',
  'status_c' => 'aos_products_cstm.status_c',
  'fp_event_locations_aos_products_1_name' => 'aos_products.fp_event_locations_aos_products_1_name',
),
    'searchInputs' => array (
  1 => 'name',
  6 => 'price',
  7 => 'created_by',
  9 => 'aos_product_category_name',
  10 => 'status_c',
  11 => 'fp_event_locations_aos_products_1_name',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'aos_product_category_name' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_AOS_PRODUCT_CATEGORYS_NAME',
    'id' => 'AOS_PRODUCT_CATEGORY_ID',
    'link' => true,
    'width' => '10%',
    'name' => 'aos_product_category_name',
  ),
  'fp_event_locations_aos_products_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_FP_EVENT_LOCATIONS_AOS_PRODUCTS_1_FROM_FP_EVENT_LOCATIONS_TITLE',
    'id' => 'FP_EVENT_LOCATIONS_AOS_PRODUCTS_1FP_EVENT_LOCATIONS_IDA',
    'width' => '10%',
    'name' => 'fp_event_locations_aos_products_1_name',
  ),
  'status_c' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_STATUS',
    'width' => '10%',
    'name' => 'status_c',
  ),
  'price' => 
  array (
    'name' => 'price',
    'width' => '10%',
  ),
  'created_by' => 
  array (
    'name' => 'created_by',
    'label' => 'LBL_CREATED',
    'type' => 'enum',
    'function' => 
    array (
      'name' => 'get_user_array',
      'params' => 
      array (
        0 => false,
      ),
    ),
    'width' => '10%',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => '25%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'FP_EVENT_LOCATIONS_AOS_PRODUCTS_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_FP_EVENT_LOCATIONS_AOS_PRODUCTS_1_FROM_FP_EVENT_LOCATIONS_TITLE',
    'id' => 'FP_EVENT_LOCATIONS_AOS_PRODUCTS_1FP_EVENT_LOCATIONS_IDA',
    'width' => '10%',
    'default' => true,
    'name' => 'fp_event_locations_aos_products_1_name',
  ),
  'AOS_PRODUCT_CATEGORY_NAME' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_AOS_PRODUCT_CATEGORYS_NAME',
    'id' => 'AOS_PRODUCT_CATEGORY_ID',
    'link' => true,
    'width' => '10%',
    'default' => true,
    'name' => 'aos_product_category_name',
  ),
  'PRICE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_PRICE',
    'default' => true,
    'name' => 'price',
  ),
  'STATUS_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_STATUS',
    'width' => '10%',
    'name' => 'status_c',
  ),
),
);
