<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-02-20 21:26:39
$layout_defs["AOS_Products"]["subpanel_setup"]['aos_products_campaigns_1'] = array (
  'order' => 100,
  'module' => 'Campaigns',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AOS_PRODUCTS_CAMPAIGNS_1_FROM_CAMPAIGNS_TITLE',
  'get_subpanel_data' => 'aos_products_campaigns_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2017-02-21 17:40:05
$layout_defs["AOS_Products"]["subpanel_setup"]['aos_products_opportunities_1'] = array (
  'order' => 100,
  'module' => 'Opportunities',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AOS_PRODUCTS_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE',
  'get_subpanel_data' => 'aos_products_opportunities_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2017-06-27 12:49:26
$layout_defs["AOS_Products"]["subpanel_setup"]['contacts_aos_products_1'] = array (
  'order' => 100,
  'module' => 'Contacts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CONTACTS_AOS_PRODUCTS_1_FROM_CONTACTS_TITLE',
  'get_subpanel_data' => 'contacts_aos_products_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


//auto-generated file DO NOT EDIT
$layout_defs['AOS_Products']['subpanel_setup']['aos_products_purchases']['override_subpanel_name'] = 'AOS_Products_subpanel_aos_products_purchases';


//auto-generated file DO NOT EDIT
$layout_defs['AOS_Products']['subpanel_setup']['contacts_aos_products_1']['override_subpanel_name'] = 'AOS_Products_subpanel_contacts_aos_products_1';


//auto-generated file DO NOT EDIT
$layout_defs['AOS_Products']['subpanel_setup']['hero2_course_sell_aos_products']['override_subpanel_name'] = 'AOS_Products_subpanel_hero2_course_sell_aos_products';

?>