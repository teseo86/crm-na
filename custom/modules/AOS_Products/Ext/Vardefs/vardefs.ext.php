<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2017-02-20 21:26:39
$dictionary["AOS_Products"]["fields"]["aos_products_campaigns_1"] = array (
  'name' => 'aos_products_campaigns_1',
  'type' => 'link',
  'relationship' => 'aos_products_campaigns_1',
  'source' => 'non-db',
  'module' => 'Campaigns',
  'bean_name' => 'Campaign',
  'side' => 'right',
  'vname' => 'LBL_AOS_PRODUCTS_CAMPAIGNS_1_FROM_CAMPAIGNS_TITLE',
);


// created: 2017-02-21 17:40:05
$dictionary["AOS_Products"]["fields"]["aos_products_opportunities_1"] = array (
  'name' => 'aos_products_opportunities_1',
  'type' => 'link',
  'relationship' => 'aos_products_opportunities_1',
  'source' => 'non-db',
  'module' => 'Opportunities',
  'bean_name' => 'Opportunity',
  'side' => 'right',
  'vname' => 'LBL_AOS_PRODUCTS_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE',
);


// created: 2017-02-20 21:57:03
$dictionary["AOS_Products"]["fields"]["campaigns_aos_products_1"] = array (
  'name' => 'campaigns_aos_products_1',
  'type' => 'link',
  'relationship' => 'campaigns_aos_products_1',
  'source' => 'non-db',
  'module' => 'Campaigns',
  'bean_name' => 'Campaign',
  'vname' => 'LBL_CAMPAIGNS_AOS_PRODUCTS_1_FROM_CAMPAIGNS_TITLE',
  'id_name' => 'campaigns_aos_products_1campaigns_ida',
);
$dictionary["AOS_Products"]["fields"]["campaigns_aos_products_1_name"] = array (
  'name' => 'campaigns_aos_products_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CAMPAIGNS_AOS_PRODUCTS_1_FROM_CAMPAIGNS_TITLE',
  'save' => true,
  'id_name' => 'campaigns_aos_products_1campaigns_ida',
  'link' => 'campaigns_aos_products_1',
  'table' => 'campaigns',
  'module' => 'Campaigns',
  'rname' => 'name',
);
$dictionary["AOS_Products"]["fields"]["campaigns_aos_products_1campaigns_ida"] = array (
  'name' => 'campaigns_aos_products_1campaigns_ida',
  'type' => 'link',
  'relationship' => 'campaigns_aos_products_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CAMPAIGNS_AOS_PRODUCTS_1_FROM_AOS_PRODUCTS_TITLE',
);


// created: 2017-06-27 12:49:26
$dictionary["AOS_Products"]["fields"]["contacts_aos_products_1"] = array (
  'name' => 'contacts_aos_products_1',
  'type' => 'link',
  'relationship' => 'contacts_aos_products_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_CONTACTS_AOS_PRODUCTS_1_FROM_CONTACTS_TITLE',
);




$dictionary['AOS_Products']['fields']['contacts_aos_products_1_fields'] = array (
			'name' => 'contacts_aos_products_1_fields',
			'rname' => 'id',
			'relationship_fields'=>array(
                            'id' => 'contacts_aos_products_1_id', 
                            'precio_curso' => 'contacts_aos_products_1_precio_curso', 
                            'vendedor' => 'contacts_aos_products_1_vendedor',
                            'id_inscripcion' => 'contacts_aos_products_1_id_inscripcion',
                            'como_se_entero' => 'contacts_aos_products_1_como_se_entero',
                            'rol' => 'contacts_aos_products_1_rol',
                            'fecha_inicio_clases' => 'contacts_aos_products_1_fecha_inicio_clases',
                            'fecha_fin_clases' => 'contacts_aos_products_1_fecha_fin_clases',
                        ),
			'vname' => 'LBL_CONTACTS_AOS_PRODUCTS_1_FIELDS',
			'type' => 'relate',
			'link' => 'contacts_aos_products_1',
			'link_type' => 'relationship_info',
			'join_link_name' => 'contacts_aos_products_1_c',
                        'join_name' => 'contacts_aos_products_1_c',
			'source' => 'non-db',
			'importable' => false,
                        'duplicate_merge'=> 'disabled',
			'studio' => array('listview' => false),
			'join_primary' => false, //this is key!!! See SugarBean.php and search for join_primary for more info

    
);

$dictionary['AOS_Products']['fields']['contacts_aos_products_1_id'] = array (
			'name' => 'contacts_aos_products_1_id',
			'type' => 'varchar',
			'source' => 'non-db',
			'vname' => 'LBL_CONTACT_AOS_PRODUCTS_1_ID',
			'studio' => array('listview' => false),
);

$dictionary['AOS_Products']['fields']['contacts_aos_products_1_vendedor'] = array (
			'name' => 'contacts_aos_products_1_vendedor',
			'type' => 'varchar',
                        'source' => 'non-db',
			'vname' => 'LBL_VENDEDOR',
                        'reportable' => true,
);

$dictionary['AOS_Products']['fields']['contacts_aos_products_1_precio_curso'] = array (
			'name' => 'contacts_aos_products_1_precio_curso',
			'type' => 'varchar',
                        'source' => 'non-db',
			'vname' => 'LBL_PRECIO_CURSO',
                        'reportable' => true,
);


$dictionary['AOS_Products']['fields']['contacts_aos_products_1_id_inscripcion'] = array (
			'name' => 'contacts_aos_products_1_id_inscripcion',
			'type' => 'varchar',
                        'source' => 'non-db',
			'vname' => 'LBL_ID_INSCRIPCION',
                        'reportable' => true,
);

$dictionary['AOS_Products']['fields']['contacts_aos_products_1_rol'] = array (
			'name' => 'contacts_aos_products_1_rol',
			'type' => 'varchar',
                        'source' => 'non-db',
			'vname' => 'LBL_ROL_CONTACTO',
                        'reportable' => true,
);

$dictionary['AOS_Products']['fields']['contacts_aos_products_1_como_se_entero'] = array (
			'name' => 'contacts_aos_products_1_como_se_entero',
			'type' => 'varchar',
                        'source' => 'non-db',
			'vname' => 'LBL_COMO_SE_ENTERO_CURSO',
                        'reportable' => true,
);

$dictionary['AOS_Products']['fields']['contacts_aos_products_1_fecha_inicio_clases'] = array (
			'name' => 'contacts_aos_products_1_fecha_inicio_clases',
			'type' => 'date',
                        'source' => 'non-db',
			'vname' => 'LBL_FECHA_INICIO_CURSO',
                        'reportable' => true,
);

$dictionary['AOS_Products']['fields']['contacts_aos_products_1_fecha_fin_clases'] = array (
			'name' => 'contacts_aos_products_1_fecha_fin_clases',
			'type' => 'date',
                        'source' => 'non-db',
			'vname' => 'LBL_FECHA_FIN_CURSO',
                        'reportable' => true,
);



// created: 2017-08-31 18:30:20
$dictionary["AOS_Products"]["fields"]["fp_event_locations_aos_products_1"] = array (
  'name' => 'fp_event_locations_aos_products_1',
  'type' => 'link',
  'relationship' => 'fp_event_locations_aos_products_1',
  'source' => 'non-db',
  'module' => 'FP_Event_Locations',
  'bean_name' => 'FP_Event_Locations',
  'vname' => 'LBL_FP_EVENT_LOCATIONS_AOS_PRODUCTS_1_FROM_FP_EVENT_LOCATIONS_TITLE',
  'id_name' => 'fp_event_locations_aos_products_1fp_event_locations_ida',
);
$dictionary["AOS_Products"]["fields"]["fp_event_locations_aos_products_1_name"] = array (
  'name' => 'fp_event_locations_aos_products_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_FP_EVENT_LOCATIONS_AOS_PRODUCTS_1_FROM_FP_EVENT_LOCATIONS_TITLE',
  'save' => true,
  'id_name' => 'fp_event_locations_aos_products_1fp_event_locations_ida',
  'link' => 'fp_event_locations_aos_products_1',
  'table' => 'fp_event_locations',
  'module' => 'FP_Event_Locations',
  'rname' => 'name',
);
$dictionary["AOS_Products"]["fields"]["fp_event_locations_aos_products_1fp_event_locations_ida"] = array (
  'name' => 'fp_event_locations_aos_products_1fp_event_locations_ida',
  'type' => 'link',
  'relationship' => 'fp_event_locations_aos_products_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_FP_EVENT_LOCATIONS_AOS_PRODUCTS_1_FROM_AOS_PRODUCTS_TITLE',
);


 // created: 2017-05-09 15:18:49
$dictionary['AOS_Products']['fields']['aos_product_category_name']['inline_edit']=true;
$dictionary['AOS_Products']['fields']['aos_product_category_name']['merge_filter']='disabled';

 

 // created: 2017-08-24 21:45:41
$dictionary['AOS_Products']['fields']['course_category_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['course_category_c']['labelValue']='Categoría';

 

 // created: 2017-08-24 21:47:08
$dictionary['AOS_Products']['fields']['course_subcategory_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['course_subcategory_c']['labelValue']='Subcategoría';

 

 // created: 2017-08-24 21:50:45
$dictionary['AOS_Products']['fields']['course_type_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['course_type_c']['labelValue']='Tipo Curso';

 

 // created: 2017-08-24 21:52:11
$dictionary['AOS_Products']['fields']['duracion_meses_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['duracion_meses_c']['labelValue']='Duración (meses)';

 

 // created: 2017-02-20 16:52:49
$dictionary['AOS_Products']['fields']['enrollment_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['enrollment_c']['labelValue']='Matrícula';

 

 // created: 2017-08-24 21:54:20
$dictionary['AOS_Products']['fields']['fecha_fin_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['fecha_fin_c']['labelValue']='Fecha Fin';

 

 // created: 2017-08-24 21:55:22
$dictionary['AOS_Products']['fields']['id_curso_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['id_curso_c']['labelValue']='Id Curso BD';

 

 // created: 2017-02-20 23:03:29
$dictionary['AOS_Products']['fields']['material_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['material_c']['labelValue']='Material';

 

 // created: 2017-02-20 23:05:53
$dictionary['AOS_Products']['fields']['num_classes_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['num_classes_c']['labelValue']='# Clases';

 

 // created: 2017-08-24 21:56:51
$dictionary['AOS_Products']['fields']['num_inscritos_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['num_inscritos_c']['labelValue']='# Inscritos';

 

 // created: 2017-08-24 22:03:02
$dictionary['AOS_Products']['fields']['num_posibles_inscritos_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['num_posibles_inscritos_c']['labelValue']='# Num Posibles Inscritos';

 

 // created: 2017-08-24 22:00:19
$dictionary['AOS_Products']['fields']['periodicidad_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['periodicidad_c']['labelValue']='Periodicidad';

 

 // created: 2017-02-20 23:02:46
$dictionary['AOS_Products']['fields']['schedule_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['schedule_c']['labelValue']='Horario';

 

 // created: 2017-02-20 23:02:11
$dictionary['AOS_Products']['fields']['start_date_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['start_date_c']['labelValue']='Fecha Inicio';

 

 // created: 2017-03-09 16:57:05
$dictionary['AOS_Products']['fields']['status_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['status_c']['labelValue']='Status';

 

 // created: 2017-02-20 16:29:25
$dictionary['AOS_Products']['fields']['student_price_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['student_price_c']['labelValue']='Precio Estudiante';

 

 // created: 2017-09-01 13:14:55
$dictionary['AOS_Products']['fields']['turno_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['turno_c']['labelValue']='Turno';

 
?>