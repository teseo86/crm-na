<?php
// created: 2017-02-20 21:58:42
$mod_strings = array (
  'LBL_FP_EVENT_LOCATIONS_FP_EVENTS_1_FROM_FP_EVENTS_TITLE' => 'Actividades',
  'LBL_TELEFONO' => 'Teléfono',
  'LBL_IDSEDERESERVA' => 'Id Sede Reservaciones',
  'LBL_ADDRESS_POSTALCODE' => 'Código Postal',
  'LBL_ADDRESS_STATE' => 'Estado/Provincia',
  'LBL_CORREO' => 'Correo',
  'LBL_IDSEDEBD' => 'Id Sede BD',
  'LBL_DEPARTAMENTO' => 'Departamento',
  'LBL_EDITVIEW_PANEL2' => 'Otra información',
  'LNK_NEW_RECORD' => 'Crear Sedes',
  'LNK_LIST' => 'Ver Sedes',
  'LNK_IMPORT_FP_EVENT_LOCATIONS' => 'Importar Sedes',
  'LBL_LIST_FORM_TITLE' => 'Lista de Sedes',
  'LBL_SEARCH_FORM_TITLE' => 'Buscar Sedes',
  'LBL_HOMEPAGE_TITLE' => 'Mis Sedes',
);