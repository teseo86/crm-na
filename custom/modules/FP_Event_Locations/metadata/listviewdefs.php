<?php
$module_name = 'FP_Event_Locations';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'CORREO_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_CORREO',
    'width' => '10%',
  ),
  'TELEFONO_C' => 
  array (
    'type' => 'phone',
    'default' => true,
    'label' => 'LBL_TELEFONO',
    'width' => '10%',
  ),
  'CAPACITY' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_CAPACITY',
    'width' => '10%',
  ),
);
?>
