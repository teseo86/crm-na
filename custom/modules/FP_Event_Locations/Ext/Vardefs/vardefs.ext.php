<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2017-08-31 18:30:20
$dictionary["FP_Event_Locations"]["fields"]["fp_event_locations_aos_products_1"] = array (
  'name' => 'fp_event_locations_aos_products_1',
  'type' => 'link',
  'relationship' => 'fp_event_locations_aos_products_1',
  'source' => 'non-db',
  'module' => 'AOS_Products',
  'bean_name' => 'AOS_Products',
  'side' => 'right',
  'vname' => 'LBL_FP_EVENT_LOCATIONS_AOS_PRODUCTS_1_FROM_AOS_PRODUCTS_TITLE',
);


 // created: 2017-02-14 20:16:15
$dictionary['FP_Event_Locations']['fields']['address_postalcode']['required']=false;
$dictionary['FP_Event_Locations']['fields']['address_postalcode']['inline_edit']=true;

 

 // created: 2017-02-14 20:16:42
$dictionary['FP_Event_Locations']['fields']['address_state']['inline_edit']=true;

 

 // created: 2017-02-14 20:17:31
$dictionary['FP_Event_Locations']['fields']['correo_c']['inline_edit']='1';
$dictionary['FP_Event_Locations']['fields']['correo_c']['labelValue']='Correo';

 

 // created: 2017-02-14 20:20:24
$dictionary['FP_Event_Locations']['fields']['departamento_c']['inline_edit']='1';
$dictionary['FP_Event_Locations']['fields']['departamento_c']['labelValue']='Departamento';

 

 // created: 2017-02-14 20:18:11
$dictionary['FP_Event_Locations']['fields']['idsedebd_c']['inline_edit']='1';
$dictionary['FP_Event_Locations']['fields']['idsedebd_c']['labelValue']='Id Sede BD';

 

 // created: 2017-02-14 20:15:57
$dictionary['FP_Event_Locations']['fields']['idsedereserva_c']['inline_edit']='1';
$dictionary['FP_Event_Locations']['fields']['idsedereserva_c']['labelValue']='Id Sede Reservaciones';

 

 // created: 2017-02-14 20:14:52
$dictionary['FP_Event_Locations']['fields']['telefono_c']['inline_edit']='1';
$dictionary['FP_Event_Locations']['fields']['telefono_c']['labelValue']='Teléfono';

 
?>