<?php

function sendSugarPHPMail($tos, $subject, $body,$ccs="",$bcc="", $email_flag)
{
	require_once('include/SugarPHPMailer.php');
	require_once('modules/Administration/Administration.php');

	$mail  = new SugarPHPMailer();
	$admin = new Administration();
	$admin->retrieveSettings();

	if ($admin->settings['mail_sendtype'] == "SMTP") 
	{
		$mail->Host = $admin->settings['mail_smtpserver'];
		$mail->Port = $admin->settings['mail_smtpport'];

		if ($admin->settings['mail_smtpauth_req']) 
		{
			$mail->SMTPAuth = TRUE;
			$mail->Username = $admin->settings['mail_smtpuser'];
			$mail->Password = $admin->settings['mail_smtppass'];
		}
		if ($admin->settings['mail_smtpssl'] == 1) 
		{
			$mail->SMTPSecure = 'ssl';
		}
		else if ($admin->settings['mail_smtpssl'] == 2) 
		{
			$mail->SMTPSecure = 'tls';
		}

		$mail->Mailer   = "smtp";
		$mail->SMTPKeepAlive = true;

	}
	else
	{
		$mail->mailer = 'sendmail';
	}
	
	if($email_flag==1)
	{
		$mail->From     = $admin->settings['notify_fromaddress'];
		$mail->FromName = $admin->settings['notify_fromname'];
	}
	
	$mail->ContentType = "text/html"; //"text/plain"

	$mail->Subject = $subject;
	$mail->Body = $body;

	if(is_array($tos))
	{
		foreach ($tos as $id => $email)
		{
		  $mail->AddAddress("{$email['email']}", "{$email['name']}");
		}
	}
	
	if(is_array($ccs))
	{
		foreach ($ccs as $id => $email)
		{
		$mail->AddCC("{$email['email']}", "{$email['name']}");
		}
	}
	
	if(is_array($bcc))
	{
		foreach ($bcc as $id => $email)
		{
			$mail->AddBCC("{$email['email']}", "{$email['name']}");
		}
	}
	
	if (!$mail->send()) 
	{
		$GLOBALS['log']->info("sendSugarPHPMail - Mailer error: " . $mail->ErrorInfo);
		return false;
	}
	else
	{
		 return true;
	}
}
?>
