<?php
//URL http://localhost/suite781/index.php?entryPoint=sendEmailToRegisteredUser&module=FP_events
    require_once('custom/modules/FP_events/customFunction.php');
    require_once('modules/Emails/Email.php');
    $admin1 = new Administration();
	$admin1->retrieveSettings();
    global $db,$mod_strings;
	$query = "SELECT id,register_user_email from fp_events where check_to_send_email = 1 and deleted = 0";
	
	$result = $GLOBALS['db']->query($query);
	while($row = $GLOBALS['db']->fetchByAssoc($result)) {
		
		$tos        = array();
		$subject    = $mod_strings['LBL_REGISTERED_EMAIL_SUBJECT'];
		$body       = $row['register_user_email'];
		$ccs        = '';
		$bcc        = '';
		$email_flag = 1;
		 $event_id = $row['id'];
		 $query_contact = 'select fp_events_contactscontacts_idb from  fp_events_contacts_c where invite_status = "Register" and  fp_events_contactsfp_events_ida="'.$row['id'].'" AND deleted=0';
		 $res_contact = $db->query($query_contact);
		 while($row_contact = $GLOBALS['db']->fetchByAssoc($res_contact)) {
			 
			 $module1 = 'Contacts';
		     $ContactDetail = BeanFactory::getBean($module1, $row_contact['fp_events_contactscontacts_idb']);
		     $tos[] = 
				array(
					'email'=>$ContactDetail->email1,
					'name'=>$ContactDetail->name
				);
				$emailObjContact = new Email();
				$defaults1 = $emailObjContact->getSystemDefaultEmail();
				$emailObjContact->to_addrs= $ContactDetail->email1;
				$emailObjContact->type= 'archived';
				$emailObjContact->deleted = '0';
				$emailObjContact->name = $subject ;
				$emailObjContact->description = $body;
				$emailObjContact->description_html = null;
				$emailObjContact->from_addr = $admin1->settings['notify_fromaddress'];
				if ( $ContactDetail instanceOf SugarBean && !empty($ContactDetail->id) ) {
					$emailObjContact->parent_type = $ContactDetail->module_dir;
					$emailObjContact->parent_id = $ContactDetail->id;
				}
				$emailObjContact->date_sent = TimeDate::getInstance()->nowDb();
				$emailObjContact->modified_user_id = '1';
				$emailObjContact->created_by = '1';
				$emailObjContact->status = 'sent';
				$emailObjContact->save();
			
			 
		 }
		 
		//update Leads query
		$query_lead = 'select fp_events_leads_1leads_idb from fp_events_leads_1_c  WHERE fp_events_leads_1fp_events_ida="'.$event_id.'"  AND invite_status = "Register" and deleted = 0';
		$res_lead = $db->query($query_lead);
		 while($row_lead = $GLOBALS['db']->fetchByAssoc($res_lead)) {
			 
			 $module2 = 'Leads';
		     $LeadDetail = BeanFactory::getBean($module2, $row_lead['fp_events_leads_1leads_idb']);
		     if ($LeadDetail->no_email_c != '1' && $LeadDetail->envio_email_c)
		     {
				 $tos[] = 
					array(
						'email'=>$LeadDetail->email1,
						'name'=>$LeadDetail->name
					);
					
					$emailObjLead = new Email();
					$defaults2 = $emailObjLead->getSystemDefaultEmail();
					$emailObjLead->to_addrs= $emailObjLead->email1;
					$emailObjLead->type= 'archived';
					$emailObjLead->deleted = '0';
					$emailObjLead->name = $subject ;
					$emailObjLead->description = $body;
					$emailObjLead->description_html = null;
					$emailObjLead->from_addr = $admin1->settings['notify_fromaddress'];
					if ( $emailObjLead instanceOf SugarBean && !empty($emailObjLead->id) ) {
						$emailObjLead->parent_type = $emailObjLead->module_dir;
						$emailObjLead->parent_id = $emailObjLead->id;
					}
					$emailObjLead->date_sent = TimeDate::getInstance()->nowDb();
					$emailObjLead->modified_user_id = '1';
					$emailObjLead->created_by = '1';
					$emailObjLead->status = 'sent';
					$emailObjLead->save();
			}
			 
		 }
		//update targets query
		$query_prospect= 'select fp_events_prospects_1prospects_idb from  fp_events_prospects_1_c  WHERE fp_events_prospects_1fp_events_ida="'.$event_id.'" AND invite_status = "Register" and deleted = 0';
		$res_prospect = $db->query($query_prospect);
		while($row_prospect = $GLOBALS['db']->fetchByAssoc($res_prospect)) {
			 
			 $module3 = 'Prospects';
		     $ProsDetail = BeanFactory::getBean($module3, $row_prospect['fp_events_prospects_1prospects_idb']);
		     $tos[] = 
				array(
					'email'=>$ProsDetail->email1,
					'name'=>$ProsDetail->name
				);
				$emailObjPro = new Email();
				$defaults3 = $emailObjPro->getSystemDefaultEmail();
				$emailObjPro->to_addrs= $emailObjPro->email1;
				$emailObjPro->type= 'archived';
				$emailObjPro->deleted = '0';
				$emailObjPro->name = $subject ;
				$emailObjPro->description = $body;
				$emailObjPro->description_html = null;
				$emailObjPro->from_addr = $admin1->settings['notify_fromaddress'];
				if ( $emailObjPro instanceOf SugarBean && !empty($emailObjPro->id) ) {
					$emailObjPro->parent_type = $emailObjPro->module_dir;
					$emailObjPro->parent_id = $emailObjPro->id;
				}
				$emailObjPro->date_sent = TimeDate::getInstance()->nowDb();
				$emailObjPro->modified_user_id = '1';
				$emailObjPro->created_by = '1';
				$emailObjPro->status = 'sent';
				$emailObjPro->save();
			 
			 
		 }
		 
		 //echo "<pre>";
		 //print_r($tos);
		if (sendSugarPHPMail($tos, $subject, $body, $ccs, $bcc, $email_flag))
		{
			
			
		}
		unset($tos);
	}
	
?>
