<?php
$module_name = 'FP_events';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
        'hidden' => 
        array (
          0 => '<input id="custom_hidden_1" type="hidden" name="custom_hidden_1" value=""/>',
          1 => '<input id="custom_hidden_2" type="hidden" name="custom_hidden_2" value=""/>',
        ),
      ),
      'maxColumns' => '2',
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'custom/include/javascript/checkbox.js',
        ),
        1 => 
        array (
          'file' => 'cache/include/javascript/sugar_grp_yui_widgets.js',
        ),
      ),
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_PANEL_OVERVIEW' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL3' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EMAIL_INVITE' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'LBL_PANEL_OVERVIEW' => 
      array (
        0 => 
        array (
          0 => 'name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'tipo_actividad_c',
            'studio' => 'visible',
            'label' => 'LBL_TIPO_ACTIVIDAD',
          ),
          1 => 
          array (
            'name' => 'fp_event_locations_fp_events_1_name',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'date_start',
            'comment' => 'Date of start of meeting',
            'label' => 'LBL_DATE',
          ),
          1 => 
          array (
            'name' => 'date_end',
            'comment' => 'Date meeting ends',
            'label' => 'LBL_DATE_END',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'fecha_actividad_c',
            'label' => 'LBL_FECHA_ACTIVIDAD',
          ),
          1 => 
          array (
            'name' => 'expositor_c',
            'label' => 'LBL_EXPOSITOR',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'duration',
            'customCode' => '{$fields.duration_hours.value}{$MOD.LBL_HOURS_ABBREV} {$fields.duration_minutes.value}{$MOD.LBL_MINSS_ABBREV} ',
            'label' => 'LBL_DURATION',
          ),
          1 => 
          array (
            'name' => 'budget',
            'label' => 'LBL_BUDGET',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'activity_status_type',
            'label' => 'LBL_ACTIVITY_STATUS',
          ),
        ),
        6 => 
        array (
          0 => 'description',
        ),
        7 => 
        array (
          0 => 'assigned_user_name',
          1 => 
          array (
            'name' => 'campaigns_fp_events_1_name',
            'label' => 'LBL_CAMPAIGNS_FP_EVENTS_1_FROM_CAMPAIGNS_TITLE',
          ),
        ),
      ),
      'lbl_editview_panel3' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'num_reservations_c',
            'label' => 'LBL_NUM_RESERVATIONS',
          ),
          1 => 
          array (
            'name' => 'num_assistance_c',
            'label' => 'LBL_NUM_ASSISTANCE',
            'customCode' => '{$total_assist}',
          ),
        ),
      ),
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'id_activity_bd_reservation_c',
            'label' => 'LBL_ID_ACTIVITY_BD_RESERVATION',
          ),
        ),
      ),
      'LBL_EMAIL_INVITE' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'invite_templates',
            'studio' => 'visible',
            'label' => 'LBL_INVITE_TEMPLATES',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'imagen_email_c',
            'studio' => 'visible',
            'label' => 'LBL_IMAGEN_EMAIL',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'accept_redirect',
            'label' => 'LBL_ACCEPT_REDIRECT',
          ),
          1 => 
          array (
            'name' => 'decline_redirect',
            'label' => 'LBL_DECLINE_REDIRECT',
          ),
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'check_to_send_email',
            'label' => 'LBL_CHECK_TO_SEND_EMAIL',
          ),
          1 => 
          array (
            'name' => 'register_user_email_message',
            'studio' => 'visible',
            'label' => 'LBL_REGISTER_USER_EMAIL_MESSAGE',
          ),
        ),
      ),
    ),
  ),
);
?>
