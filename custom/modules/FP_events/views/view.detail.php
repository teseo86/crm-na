<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/MVC/View/views/view.detail.php');

class FP_eventsViewDetail extends ViewDetail {
	var $currSymbol;
	function __construct(){
 		parent::__construct();
 	}

    /**
     * @deprecated deprecated since version 7.6, PHP4 Style Constructors are deprecated and will be remove in 7.8, please update your code, use __construct instead
     */
    function FP_eventsViewDetail(){
        $deprecatedMessage = 'PHP4 Style Constructors are deprecated and will be remove in 7.8, please update your code';
        if(isset($GLOBALS['log'])) {
            $GLOBALS['log']->deprecated($deprecatedMessage);
        }
        else {
            trigger_error($deprecatedMessage, E_USER_DEPRECATED);
        }
        self::__construct();
    }


	function display(){
		$this->bean->email_templates();
		
		 global $db;
		
		 
		  //Count the number of delegates linked to the event that have not yet been invited
        $queryp = "SELECT * FROM fp_events_contacts_c WHERE fp_events_contactsfp_events_ida='".$this->bean->id."' AND (invite_status='Assist' OR invite_status='Assist WithoutReservation' ) AND deleted='0'";
        $resultp = $db->query($queryp);
        $contactp_count = $db->getRowCount($resultp);//count contacts

        $querya = "SELECT * FROM fp_events_prospects_1_c WHERE fp_events_prospects_1fp_events_ida='".$this->bean->id."' AND (invite_status='Assist' OR invite_status='Assist WithoutReservation' ) AND deleted='0'";
        $resulta = $db->query($querya);
        $prospectp_count = $db->getRowCount($resulta);//count targets

        $queryd = "SELECT * FROM fp_events_leads_1_c WHERE fp_events_leads_1fp_events_ida='".$this->bean->id."' AND (invite_status='Assist' OR invite_status='Assist WithoutReservation' ) AND deleted='0'";
        $resultd = $db->query($queryd);
        $leadp_count = $db->getRowCount($resultd);//count leads

        $total_assist = $contactp_count + $prospectp_count + $leadp_count;
		$this->ss->assign("total_assist", $total_assist);
		parent::display();
	}
}
?>
