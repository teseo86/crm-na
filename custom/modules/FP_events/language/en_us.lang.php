<?php
// created: 2017-02-19 12:21:29
$mod_strings = array (
  'LBL_MANAGE_DELEGATES_CONFIRMED' => 'Confirm',
  'LBL_MANAGE_DELEGATES_ASSIST' => 'Assist',
  'LBL_MANAGE_DELEGATES_ASSIST_WR' => 'Assist WithoutReservation',
  'LBL_MANAGE_DELEGATES_ASSIST_REGISTER' => 'Register',
  'LBL_MANAGE_REGISTRY_TITLE' => 'Registry Channel',
  'LBL_MANAGE_REGISTRY_CALL' => 'Call',
  'LBL_MANAGE_REGISTRY_EMAIL' => 'Email',
  'LBL_SELECT_REGISTRY_CHANNEL' => 'Registry Channel',
  'LBL_MANAGE_REGISTRY_APP' => 'App',
  'LBL_MANAGE_REGISTRY_WEB' => 'Web',
  'LBL_BUDGET' => 'Budget',
  'LBL_REGISTER_USER_EMAIL_MESSAGE' => 'Email message for registered user',
  'LBL_CHECK_TO_SEND_EMAIL' => 'Check to send email to registered user',
  'LBL_EDITVIEW_PANEL1' => 'Registered User Email Settings',
  'LBL_REGISTERED_EMAIL_SUBJECT'=>'Invitation mail to Registered User',
  'LBL_HOW_TO_KNOW_ABOUT_EVENT'=>'How to Know About Event',
  'LBL_MANAGE_SUBMIT'=>'Submit',
  'LBL_PERSONAL_CONTACT_EXTERN'=>'Personal Contact Extern',
  'LBL_PERSONAL_CONTACT_INTERN'=>'Personal Contact Intern',
  'LBL_HOW_TO_KNOW'=>'How to Know About Event',
   'LBL_HOW_TO_KNOW_COUNT'=>'Count',
    
 'LBL_SOCIAL_NETWORK'=>'Internet Social Network',
 'LBL_GOOGLE'=>'Internet Google',
 'LBL_WEBPAGE'=>'Internet Webpage',
 'LBL_MAILING'=>'Mailing',
 'LBL_COURSES'=>'Courses',
 'LBL_SOCIAL'=>'Social Volunteering',
 'LBL_PRINT'=>'Printer paper',
    

);
