<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2017-02-20 21:57:03
$dictionary["FP_events"]["fields"]["campaigns_fp_events_1"] = array (
  'name' => 'campaigns_fp_events_1',
  'type' => 'link',
  'relationship' => 'campaigns_fp_events_1',
  'source' => 'non-db',
  'module' => 'Campaigns',
  'bean_name' => 'Campaign',
  'vname' => 'LBL_CAMPAIGNS_FP_EVENTS_1_FROM_CAMPAIGNS_TITLE',
  'id_name' => 'campaigns_fp_events_1campaigns_ida',
);
$dictionary["FP_events"]["fields"]["campaigns_fp_events_1_name"] = array (
  'name' => 'campaigns_fp_events_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CAMPAIGNS_FP_EVENTS_1_FROM_CAMPAIGNS_TITLE',
  'save' => true,
  'id_name' => 'campaigns_fp_events_1campaigns_ida',
  'link' => 'campaigns_fp_events_1',
  'table' => 'campaigns',
  'module' => 'Campaigns',
  'rname' => 'name',
);
$dictionary["FP_events"]["fields"]["campaigns_fp_events_1campaigns_ida"] = array (
  'name' => 'campaigns_fp_events_1campaigns_ida',
  'type' => 'link',
  'relationship' => 'campaigns_fp_events_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CAMPAIGNS_FP_EVENTS_1_FROM_FP_EVENTS_TITLE',
);



$dictionary["FP_events"]["fields"]["register_user_email"] = array (
	'required' => false,
	'name' => 'register_user_email',
	'vname' => 'LBL_REGISTER_USER_EMAIL_MESSAGE',
	'type' => 'text',
	'massupdate' => 0,
	'default' => '',
	'comments' => '',
	'help' => '',
	'audited' => 1,
	'reportable' => 0,
	'studio' => 'visible',
	'dependency' => false,
);  

$dictionary["FP_events"]["fields"]["check_to_send_email"] = array (
    'required' => false,
    'name' => 'check_to_send_email',
    'vname' => 'LBL_CHECK_TO_SEND_EMAIL',
    'type' => 'bool',
	'massupdate' => 0,
	'comments' => '',
	'help' => '',
	'importable' => 'true',
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => '0',
	'audited' => false,
	'reportable' => true,
	'calculated' => false,
	'len' => '255',
	'size' => '20',
);
 

 


// created: 2017-02-20 21:27:33
$dictionary["FP_events"]["fields"]["fp_events_campaigns_1"] = array (
  'name' => 'fp_events_campaigns_1',
  'type' => 'link',
  'relationship' => 'fp_events_campaigns_1',
  'source' => 'non-db',
  'module' => 'Campaigns',
  'bean_name' => 'Campaign',
  'side' => 'right',
  'vname' => 'LBL_FP_EVENTS_CAMPAIGNS_1_FROM_CAMPAIGNS_TITLE',
);


 // created: 2017-03-08 03:09:07
$dictionary['FP_events']['fields']['check_to_send_email']['default']='0';
$dictionary['FP_events']['fields']['check_to_send_email']['inline_edit']=true;
$dictionary['FP_events']['fields']['check_to_send_email']['merge_filter']='disabled';

 

 // created: 2017-02-22 17:59:59
$dictionary['FP_events']['fields']['duration_hours']['required']=false;
$dictionary['FP_events']['fields']['duration_hours']['inline_edit']=true;
$dictionary['FP_events']['fields']['duration_hours']['comments']='Duration (hours)';
$dictionary['FP_events']['fields']['duration_hours']['merge_filter']='disabled';
$dictionary['FP_events']['fields']['duration_hours']['enable_range_search']=false;
$dictionary['FP_events']['fields']['duration_hours']['min']=false;
$dictionary['FP_events']['fields']['duration_hours']['max']=false;
$dictionary['FP_events']['fields']['duration_hours']['disable_num_format']='';

 

 // created: 2017-05-31 17:41:22
$dictionary['FP_events']['fields']['expositor_c']['inline_edit']='1';
$dictionary['FP_events']['fields']['expositor_c']['labelValue']='Expositor';

 

 // created: 2017-06-02 00:28:16
$dictionary['FP_events']['fields']['fecha_actividad_c']['inline_edit']='1';
$dictionary['FP_events']['fields']['fecha_actividad_c']['labelValue']='Fecha Actividad';

 

 // created: 2017-02-22 17:35:52
$dictionary['FP_events']['fields']['hour_activity_c']['inline_edit']='1';
$dictionary['FP_events']['fields']['hour_activity_c']['labelValue']='Hora Inicio';

 

 // created: 2017-02-22 17:32:04
$dictionary['FP_events']['fields']['id_activity_bd_reservation_c']['inline_edit']='1';
$dictionary['FP_events']['fields']['id_activity_bd_reservation_c']['labelValue']='Id BD Reservaciones';

 

 // created: 2017-05-22 19:45:07
$dictionary['FP_events']['fields']['imagen_email_c']['inline_edit']='1';
$dictionary['FP_events']['fields']['imagen_email_c']['labelValue']='Imagen';

 

 // created: 2017-03-14 22:17:54
$dictionary['FP_events']['fields']['image_email_c']['inline_edit']='1';
$dictionary['FP_events']['fields']['image_email_c']['labelValue']='Imagen';

 

 // created: 2017-02-18 03:34:26
$dictionary['FP_events']['fields']['num_assistance_c']['inline_edit']='1';
$dictionary['FP_events']['fields']['num_assistance_c']['labelValue']='# Asistentes';

 

 // created: 2017-02-18 03:35:29
$dictionary['FP_events']['fields']['num_declined_c']['inline_edit']='1';
$dictionary['FP_events']['fields']['num_declined_c']['labelValue']='# Rechazos';

 

 // created: 2017-02-18 03:34:56
$dictionary['FP_events']['fields']['num_reservations_c']['inline_edit']='1';
$dictionary['FP_events']['fields']['num_reservations_c']['labelValue']='# Reservaciones';

 

 // created: 2017-03-08 03:09:33
$dictionary['FP_events']['fields']['register_user_email']['inline_edit']=true;
$dictionary['FP_events']['fields']['register_user_email']['merge_filter']='disabled';
$dictionary['FP_events']['fields']['register_user_email']['reportable']=true;
$dictionary['FP_events']['fields']['register_user_email']['rows']='4';
$dictionary['FP_events']['fields']['register_user_email']['cols']='20';

 

 // created: 2017-08-24 18:45:59
$dictionary['FP_events']['fields']['tipo_actividad_c']['inline_edit']='1';
$dictionary['FP_events']['fields']['tipo_actividad_c']['labelValue']='Tipo Actividad';

 
?>