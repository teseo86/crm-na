<?php
// created: 2017-03-26 23:50:18
$mod_strings = array (
  'LNK_NEW_RECORD' => 'Crear Reporte',
  'LNK_LIST' => 'Ver Reportes',
  'LNK_IMPORT_AOR_REPORTS' => 'Importar Reportes',
  'LBL_LIST_FORM_TITLE' => 'Lista de Reportes',
  'LBL_SEARCH_FORM_TITLE' => 'Buscar Reportes',
  'LBL_MODULE_NAME' => 'Reportes',
);