<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-02-20 21:57:03
$layout_defs["Campaigns"]["subpanel_setup"]['campaigns_aos_products_1'] = array (
  'order' => 100,
  'module' => 'AOS_Products',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CAMPAIGNS_AOS_PRODUCTS_1_FROM_AOS_PRODUCTS_TITLE',
  'get_subpanel_data' => 'campaigns_aos_products_1',
  'top_buttons' => 
  array (
    0 => 
    array (
     // 'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      //'widget_class' => 'SubPanelTopSelectButton',
      //'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2017-02-20 21:57:03
$layout_defs["Campaigns"]["subpanel_setup"]['campaigns_fp_events_1'] = array (
  'order' => 100,
  'module' => 'FP_events',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CAMPAIGNS_FP_EVENTS_1_FROM_FP_EVENTS_TITLE',
  'get_subpanel_data' => 'campaigns_fp_events_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      //'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
     // 'widget_class' => 'SubPanelTopSelectButton',
     // 'mode' => 'MultiSelect',
    ),
  ),
);


//auto-generated file DO NOT EDIT
$layout_defs['Campaigns']['subpanel_setup']['campaigns_aos_products_1']['override_subpanel_name'] = 'Campaign_subpanel_campaigns_aos_products_1';


//auto-generated file DO NOT EDIT
$layout_defs['Campaigns']['subpanel_setup']['campaigns_fp_events_1']['override_subpanel_name'] = 'Campaign_subpanel_campaigns_fp_events_1';

?>