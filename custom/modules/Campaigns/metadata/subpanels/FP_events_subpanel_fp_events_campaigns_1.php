<?php
// created: 2017-02-21 00:01:20
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'name' => 'name',
    'vname' => 'LBL_LIST_CAMPAIGN_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '85%',
    'default' => true,
  ),
  'status' => 
  array (
    'name' => 'status',
    'vname' => 'LBL_LIST_STATUS',
    'width' => '15%',
    'default' => true,
  ),
  'edit_button' => 
  array (
    'vname' => 'LBL_EDIT_BUTTON',
    'widget_class' => 'SubPanelEditButton',
    'module' => 'Campaigns',
    'width' => '5%',
    'default' => true,
  ),
  'remove_button' => 
  array (
    'vname' => 'LBL_REMOVE',
    'widget_class' => 'SubPanelRemoveButton',
    'module' => 'Campgains',
    'width' => '5%',
    'default' => true,
  ),
);