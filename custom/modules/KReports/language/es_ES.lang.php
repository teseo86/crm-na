<?php
// created: 2017-08-17 01:09:54
$mod_strings = array (
    'LBL_LIST_FORM_TITLE' => 'Lista de Reportes',
    'LBL_SEARCH_FORM_TITLE' => 'Buscar Reporte',
    'LBL_SAVE_BUTTON_LABEL' => 'Guardar',
    'LBL_CANCEL_BUTTON_LABEL' => 'Cancelar',
    'LBL_REMOVE_BUTTON_LABEL' => 'Eliminar',
    'LBL_REPORT_NAME_LABEL' => 'Nombre',
    'LBL_SAVE_LAYOUT_BUTTON_LABEL' => 'Save Layout',
    'LBL_LOADMASK' => '... cargando data ...',
    'LBL_SAVEMASK' => '... guardando ...',
    'LBL_ERROR' => 'Error',
    'LBL_ERROR_NAME' => 'Por favor, ingrese el nombre del reporte!',
    'LBL_ASSIGNED_USER_LABEL' => 'Usuario',
    'LBL_ASSIGNED_TEAM_LABEL' => 'Equipo',
    'LBL_KORGOBJECTS_LABEL' => 'Territory', /* revisar */
    'LBL_REPORT_OPTIONS' => 'Opciones',
    'LBL_DEFAULT_NAME' => 'Nuevo Reporte',
    'LBL_SEARCHING' => 'buscando ...',
    'LBL_LONGTEXT_LABEL' => 'Descripción',
    'LBL_CHART_NODATA' => 'no hay data del Reporte Especializado disponible para mostrar',
    'LBL_REPORT_RELOAD' => 'Aplicar filtros',
    'LBL_LIST_LISTTYPE' => 'List Type', /* revisar */
    'LBL_LIST_CHART_LAYOUT' => 'Diseño gráfico',
    'LBL_LIST_DATEENTERED' => 'Fecha creación',
    'LBL_LIST_DATEMODIFIED' => 'Fecha modificación',
    'LBL_SEARCHING' => 'Buscando...',
    'LBL_SELECT' => 'Seleccionar',
    'LBL_MANIPULATE' => 'Mostrar',
    'LBL_AUTH_CHECK' => 'Authorization Check',
    'LBL_AUTH_FULL' => 'On all Nodes',
    'LBL_AUTH_TOP' => 'Top Node only',
    'LBL_AUTH_NONE' => 'Deshabilitado',
    'LBL_SHOW_DELETED' => 'Mostrar eliminados',
    'LBL_FOLDED_PANELS' => 'Collapsible Panels',
    'LBL_DYNOPTIONS' => 'Dynamic Options',
    'LBL_RESULTS' => 'Results collapsed',
    'LBL_PANEL_OPEN' => 'abrir',
    'LBL_PANEL_COLLAPSED' => 'collapsed',
    'LBL_OPTIONS_MENUITEMS' => 'Toolbar Items',
    'LBL_ADVANCEDOPTIONS_MENUITEMS' => 'Advanced Options',
    'LBL_AOP_EXPORTTOPLANNING' => 'Export to Planning Nodes',
    'LBL_TOOLBARITEMS_FS' => 'Toolbar Items',
    'LBL_TOOLBARITEMS_SHOW' => 'Mostrar',
    'LBL_SHOW_EXPORT' => 'Exportar',
    'LBL_SHOW_SNAPSHOTS' => 'Snapshots',
    'LBL_SHOW_TOOLS' => 'Herramientas',
    'LBL_DATA_UPDATE' => 'Data update',
    'LBL_UPDATE_ON_REQUEST' => 'on User Request',
    'LBL_MODULE_NAME' => 'Reporte especializado',
    'LBL_REPORT_STATUS' => 'Estado reporte',
    'LBL_MODULE_TITLE' => 'Reporte especializado',
    'LBL_SEARCH_FORM_TITLE' => 'Buscar reporte',
    'LBL_LIST_FORM_TITLE' => 'Lista de reportes',
    'LBL_NEW_FORM_TITLE' => 'Crear reporte especializado',
    'LBL_LIST_CLOSE' => 'Cerrar',
    'LBL_LIST_SUBJECT' => 'Título',
    'LBL_DESCRIPTION' => 'Descripción:',
    'LNK_NEW_REPORT' => 'Crear nuevo reporte',
    'LNK_REPORT_LIST' => 'Lista de reportes',
    'LBL_UNIONTREE' => 'unir módulos',
    'LBL_UNIONLISTFIELDS' => 'Union List Fields',
    'LBL_UNIONFIELDDISPLAYPATH' => 'Union Path',
    'LBL_UNIONFIELDNAME' => 'Union Field name',
    'LBL_SELECT_MODULE' => 'Seleccionar un modulo',
    'LBL_SELECT_TAB' => 'Seleccione una pestaña',
    'LBL_ENTER_SEARCH_TERM' => 'Ingrese texto búsqueda',
    'LBL_LIST_MODULE' => 'Módulo',
    'LBL_LIST_ASSIGNED_USER_NAME' => 'Usuario asignado',
    'LBL_DEFINITIONS' => 'Definición de reporte',
    'LBL_MODULES' => 'Módulos',
    'LBL_LISTFIELDS' => 'Manipular',
    'LBL_PRESENTATION' => 'Presentación',
    'LBL_CHARTDEFINITION' => 'Detalles gráfico',
    'LBL_TARGETLIST_NAME' => 'Nombre de lista de público objetivo',
    'LBL_TARGETLIST_PROMPT' => 'Name of the new Targetlist',
    'LBL_DUPLICATE_NAME' => 'Nombre de reporte',
    'LBL_DUPLICATE_PROMPT' => 'Ingresar el nombre de nuevo reporte',
    'LBL_DYNAMIC_OPTIONS' => 'Criterio de búsqueda/filtros',
    
    // various 
    'LBL_PIVOTVIEW' => 'Pivot',
    'LBL_STANDARDWSUMMARY' => 'Estándar con resumen',
    'LBL_STANDARDWPREVIEW' => 'Estándar con vista previa',
    'LBL_TREEVIEW' => 'Vista de árbol',
    'LBL_TREEVIEWPROPERTIES_GRPUNTIL' => 'group until',

    'LBL_GROUPEDVIEW' => 'Grouped View',
    'LBL_STANDARDVIEW' => 'Standard View',
    'LBL_EDITPLUGIN' => 'Edit View (beta)',
    
    'LBL_GOOGLECHARTS' => 'Google Charts',
    'LBL_FUSIONCHARTS' => 'Fusion Charts',
    'LBL_HIGHCHARTS' => 'High Charts',
    'LBL_GOOGLEMAPS' => 'Google Maps',
    'LBL_GOOGLEGEO' => 'Google Geo',
    'LBL_SUGARCHARTS' => 'Sugar Charts',
    'LBL_AMCHARTS' => 'Am Charts',
    'LBL_AMMAP' => 'Am Maps',
    'LBL_AMMAP_TYPE_AREA' => 'World Map color countries',
    'LBL_AMMAP_TYPE_BUBBLES' => 'World Map with Bubbles',

    'LBL_GEOOPTIONS_TITLE' => 'Title',
    'LBL_GEOOPTIONS_REGION' => 'Display Region',
    'LBL_GEOOPTIONS_RESOLUTION' => 'Resolution',
    'LBL_GEOOPTIONS_COUNTRIES' => 'Countries',
    'LBL_GEOOPTIONS_PROVINCES' => 'Provinces',
    'LBL_GEOOPTIONS_METROS' => 'Metros',

    'LBL_ANMAPOPTIONS_COLORSTEPS' => 'Number of color steps',
    
    'LBL_PUBLISH_CNTENTRIES' => 'number of entries displayed',

    'LBL_PDF_CHARTSONSEPARATEPAGE' => 'Charts on separate Page',

    'LBL_KPDRILLDOWN' => 'Presentation Drilldown',
    'LBL_LINK_LINKTYPE' => 'Link',
    'LBL_POPUP_LINKTYPE' => 'Popup',
    'LBL_CHART_LINKTYPE' => 'Chart',
    'LBL_DRILLDOWNMENUTITLE' => 'Drill Down',
    'LBL_ADDLINKEDREPORT_TITLE' => 'Link Report',
    'LBL_ADDLINK_ADD' => 'Agregar',
    'LBL_MAPPING_TITLE' => 'InputMapping',

    'LBL_EDITABLE' => 'editable',

    'LBL_PUBLISH_ASDASHLET' => 'publish as Dashlet',
    'LBL_PUBLISH_ASSUBPANEL' => 'publicar como Subpanel',
    'LBL_PUBLISH_MOBILE' => 'publish Mobile',

    'LBL_CHARTOPTIONS_ROTATELABELS' => 'Rotate Labels',
    'LBL_STACK_NONE' => 'not stacked',
    'LBL_STACK_NORMAL' => 'stracked',
    'LBL_STACK_PERCENT' => 'porcentaje',
    'LBL_CHARTRENDER_SPLINE' => 'Spline',
    'LBL_CHARTTYPE_SPLINE' => 'Spline',
    'LBL_CHARTTYPE_AREASPLINE' => 'Area Spline',

    'LBL_CHARTTYPE_ARTRD' => 'Area w. Trendline',
    'LBL_CHARTTYPE_ARSPLINETRD' => 'Area Spline w. Trendline',

    'LBL_CHARTTYPE_ARSPLINEPOLR' => 'Area Spline Polar',
    'LBL_CHARTTYPE_ARSPLINESTCKPOL' => 'Area Spline stacked Polar',

    'LBL_CHARTTYPE_COLTRD' => 'Column w. Trendline',
    'LBL_CHARTTYPE_COLPLR' => 'Column Polar',
    'LBL_CHARTTYPE_LPOLR' => 'Line polar',
    'LBL_CHARTTYPE_DONUT' => 'Donut',
    'LBL_CHARTTYPE_PIE180' => 'Pie 180°',
    'LBL_CHARTTYPE_DONUT180' => 'Donut 180°',

    'LBL_CHARTTYPE_COLSTACKED' => 'Columns stacked',
    'LBL_CHARTTYPE_COLSTKPOLR' => 'Columns stacked polar',
    'LBL_CHARTTYPE_COLSTCKPER' => 'Columns stacked 100%',
    'LBL_CHARTTYPE_BRSTACKED' => 'Bars stacked',
    'LBL_CHARTTYPE_BRSTCKPER' => 'Bars stacked 100%',
    'LBL_CHARTTYPE_ARSTACKED' => 'Area stacked',
    'LBL_CHARTTYPE_ARSTCKPER' => 'Area stacked 100%',
    'LBL_CHARTTYPE_ARSPLINESTACKED' => 'Area Spline stacked',
    'LBL_CHARTTYPE_ARSPLINESTCKPER' => 'Area Spline stacked 100%',
    'LBL_CHARTTYPE_LPLR' => 'Line Polar',
    'LBL_CHARTTYPE_COLSTKPPL' => 'Columns stacked 100% polar',
    'LBL_CHARTTYPE_SPLPLR' => 'Spline polar',
    'LBL_CHARTTYPE_ARSTCKPOL' => 'Area stacked polar',
    'LBL_CHARTTYPE_ARSTCKPPL' => 'Area stacked 100% polar',
    'LBL_CHARTTYPE_ARSPLINESTCKPPL' => 'Area Spline stacked polar',
    'LBL_CHARTTYPE_ARSPLINESTCKPPL' => 'Area Spline stacked 100% polar',
    'LBL_CHARTTYPE_ARPOLR' => 'Area polar',
    'LBL_CHARTTYPE_FUNNEL' => 'Funnel',
    'LBL_CHARTTYPE_PYRAMID' => 'Pyramid',
    'LBL_TRENDLINENAME' => 'Trend',
    'LBL_KSNAPSHOTANALYZER' => 'Snapshot Analyzer',
    'LBL_SNAPSHOTANALYZER_TITLE' => 'ad hoc Snapshot Analyzer',
    'LBL_KPLANNER_EXPORT' => 'KPlanner Export',

    'LBL_GOOGLEMAPS_LABEL' => 'Google Maps',
    'LBL_GOOGLEMAPS_COLORSOPTIONS' => 'Colores',
    'LBL_GOOGLEMAPS_COLORS' => 'Colores',
    'LBL_GOOGLEMAPS_COLORCRITERIA' => 'Color by',
    'LBL_GOOGLEMAPS_INFO' => 'Popupinfo',
    'LBL_GOOGLEMAPS_LEGEND' => 'Mostrar leyenda',
    'LBL_GOOGLEMAPS_COLORSOPTIONSRESET' => 'reset',
    'LBL_GOOGLEMAPS_SPIDERFY' => 'Spiderfy pins',
    'LBL_GOOGLEMAPS_ROUTEPLANNER_OPTIONS' => 'Route planner options',
    'LBL_GOOGLEMAPS_ROUTEPLANNER_RESET' => 'reset',
    'LBL_GOOGLEMAPS_ROUTEPLANNER_DISPLAY' => 'Display route planner',
    'LBL_GOOGLEMAPS_ROUTEPLANNER_WAYPOINTLABEL' => 'Waypoint name',
    'LBL_GOOGLEMAPS_ROUTEPLANNER_WAYPOINTADDRESS' => 'Waypoint address',
    'LBL_GOOGLEMAPS_ROUTEPLANNER_TITLE' => 'Route planner',
    'LBL_GOOGLEMAPS_ROUTEPLANNER_SUMMARY' => 'Route details',
    'LBL_GOOGLEMAPS_ROUTEPLANNER_START' => 'Inicio',
    'LBL_GOOGLEMAPS_ROUTEPLANNER_END' => 'Fin',
    'LBL_GOOGLEMAPS_ROUTEPLANNER_WAYPOINTS' => 'Waypoints',
    'LBL_GOOGLEMAPS_ROUTEPLANNER_BTN' => 'Plan route',
    'LBL_GOOGLEMAPS_ROUTEPLANNER_RESETBTN' => 'Delete route',
    'LBL_GOOGLEMAPS_ROUTEPLANNER_WAYPTGCBY' => 'Waypoint geocode by',
    'LBL_GOOGLEMAPS_ROUTEPLANNER_SEG' => 'Route segment',
    'LBL_GOOGLEMAPS_ROUTEPLANNER_SEGFROM' => 'desde',
    'LBL_GOOGLEMAPS_ROUTEPLANNER_SEGTO' => 'hasta',
    'LBL_GOOGLEMAPS_RESIZEMAP_BTN' => 'Resize map',
    'LBL_GOOGLEMAPS_CIRCLEDESIGNER_OPTIONS' => 'Periphery search options',
    'LBL_GOOGLEMAPS_CIRCLEDESIGNER_RESET' => 'reset',
    'LBL_GOOGLEMAPS_CIRCLEDESIGNER_DISPLAY' => 'Display periphery search',
    'LBL_GOOGLEMAPS_CIRCLEDESIGNER_LABEL' => 'Periphery start point name',
    'LBL_GOOGLEMAPS_CIRCLEDESIGNER_TITLE' => 'Periphery search',
    'LBL_GOOGLEMAPS_CIRCLEDESIGNER_SUMMARY' => 'Periphery details',
    'LBL_GOOGLEMAPS_CIRCLEDESIGNER_DISTANCE' => 'Distance in Km',
    'LBL_GOOGLEMAPS_CIRCLEDESIGNER_WAYPOINTS' => 'Search from',
    'LBL_GOOGLEMAPS_CIRCLEDESIGNER_BTN' => 'Periphery search',
    'LBL_GOOGLEMAPS_CIRCLEDESIGNER_RESETBTN' => 'Delete circle(s)',
    'LBL_GOOGLEMAPS_CIRCLEDESIGNER_MODULE' => 'Módulo',
    'LBL_GOOGLEMAPS_CIRCLEDESIGNER_DISPLAYFIELDS' => 'Query config',

    'LBL_PREVIEW' => 'Preview for',

    'LBL_PDF_WHERE' => 'Print Selection criteria',
    
    // Grid headers
    'LBL_FIELDNAME' => 'Campo',
    'LBL_NAME' => 'Nombre',
    'LBL_OPERATOR' => 'Operador',
    'LBL_VALUE_FROM' => 'Igual/Desde',
    'LBL_VALUE_TO' => 'a',
    'LBL_JOIN_TYPE' => 'Requerido',
    'LBL_TYPE' => 'Tipo',
    'LBL_WIDTH' => 'Ancho',
    'LBL_SORTPRIORITY' => 'Sortseq.',
    'LBL_SORTSEQUENCE' => 'Sort',
    'LBL_EXPORTPDF' => 'mostrar en PDF',
    'LBL_DISPLAY' => 'Mostrar',
    'LBL_OVERRIDETYPE' => 'sobreescribir tipo',
    'LBL_LINK' => 'Link',
    'LBL_WIDGET' => 'Widget',
    'LBL_FIXEDVALUE' => 'Valor fijo',
    'LBL_ASSIGNTOVALUE' => 'Store',
    'LBL_FORMULAVALUE' => 'Fórmula',
    'LBL_FORMULASEQUENCE' => 'Seq.',
    'LBL_PATH' => 'Path',
    'LBL_FULLPATH' => 'technical Path',
    'LBL_SEQUENCE' => 'Seq.',
    'LBL_GROUPBY' => 'Agrupar por',
    'LBL_SQLFUNCTION' => 'Función',
    'LBL_CUSTOMSQLFUNCTION' => 'CustomFunction',
    'LBL_VALUETYPE' => 'Value Type',
    'LBL_DISPLAYFUNCTION' => 'Disp. Funct.',
    'LBL_USEREDITABLE' => 'Allow Edit',
    'LBL_DASHLETEDITABLE' => 'Dashlet Option',
    'LBL_QUERYCONTEXT' => 'Contexto',
    'LBL_QUERYREFERENCE' => 'Referencia',
    'LBL_UEOPTION_YES' => 'si',
    'LBL_UEOPTION_NO' => 'no',
    'LBL_UEOPTION_YFO' => 'value only',
    'LBL_UEOPTION_YO1' => 'on/(off)',
    'LBL_UEOPTION_YO2' => '(on)/off',
    'LBL_DEOPTION_YES' => 'si',
    'LBL_DEOPTION_NO' => 'no',
    'LBL_ONOFF_YO1' => 'si',
    'LBL_ONOFF_YO2' => 'no',
    'LBL_ONOFF_COLUMN' => 's/n',
    // Title and Headers for Multiselect Popup
    'LBL_MUTLISELECT_POPUP_TITLE' => 'Seleccionar valores',
    'LBL_MULTISELECT_VALUE_HEADER' => 'ID',
    'LBL_MULTISELECT_TEXT_HEADER' => 'Valor',
    'LBL_MUTLISELECT_CLOSE_BUTTON' => 'Actualizar',
    'LBL_MUTLISELECT_CANCEL_BUTTON' => 'Cancelar',
    // Title and Headers for Datetimepicker Popup
    'LBL_DATETIMEPICKER_POPUP_TITLE' => 'Seleccionar Fecha/Hora',
    'LBL_DATETIMEPICKER_CLOSE_BUTTON' => 'Actualizar',
    'LBL_DATETIMEPICKER_CANCEL_BUTTON' => 'Cancelar',
    'LBL_DATETIMEPICKER_DATE' => 'Date',
    // for the Snapshot Comaprison
    'LBL_SNAPSHOTCOMPARISON_POPUP_TITLE' => 'Chart by Chart',
    'LBL_SNAPSHOTTRENDANALYSIS_POPUP_TITLE' => 'Trend Analysis',
    'LBL_SNAPSHOTCOMPARISON_SNAPHOT_HEADER' => 'Snapshot',
    'LBL_SNAPSHOTCOMPARISON_DESCRIPTION_HEADER' => 'Description',
    'LBL_SNAPSHOTCOMPARISON_SELECT_CHART' => 'Select Chart:',
    'LBL_SNAPSHOTCOMPARISON_SELECT_LEFT' => 'Select left source:',
    'LBL_SNAPSHOTCOMPARISON_SELECT_RIGHT' => 'Select right source:',
    'LBL_SNAPSHOTCOMPARISON_DATASERIES' => 'Data',
    'LBL_SNAPSHOTCOMPARISON_DATADIMENSION' => 'Dimension',
    'LBL_SNAPSHOTCOMPARISON_CHARTTYPE' => 'Charttype',
    'LBL_BASIC_TRENDLINE_BUTTON_LABEL' => 'Trend Analysis',
    'LBL_SNAPSHOTCOMPARISON_CHARTTYPE_MSLINE' => 'Linea',
    'LBL_SNAPSHOTCOMPARISON_CHARTTYPE_STACKEDAREA2D' => 'Area',
    'LBL_SNAPSHOTCOMPARISON_CHARTTYPE_MSBAR2D' => 'Barra 2D',
    'LBL_SNAPSHOTCOMPARISON_CHARTTYPE_MSBAR3D' => 'Barra 3D',
    'LBL_SNAPSHOTCOMPARISON_CHARTTYPE_MSCOLUMN2D' => 'Column 2D',
    'LBL_SNAPSHOTCOMPARISON_CHARTTYPE_MSCOLUMN3D' => 'Column 3D',
    'LBL_SNAPSHOTCOMPARISON_LOADINGCHARTMSG' => 'cargando Gráfico',
    // Operator Names
    'LBL_OP_IGNORE' => 'ignorar',
    'LBL_OP_EQUALS' => '=',
    'LBL_OP_EQGROUPED' => 'is (bucket)',
    'LBL_OP_AUTOCOMPLETE' => 'autocompletar nombre',
    'LBL_OP_SOUNDSLIKE' => 'es como',
    'LBL_OP_NOTEQUAL' => '≠',
    'LBL_OP_STARTS' => 'empieza con',
    'LBL_OP_CONTAINS' => 'contiene',
    'LBL_OP_NOTSTARTS' => 'no empieza con',
    'LBL_OP_NOTCONTAINS' => 'no contiene',
    'LBL_OP_BETWEEN' => 'está entre',
    'LBL_OP_ISEMPTY' => 'está vacío',
    'LBL_OP_ISEMPTYORNULL' => 'está vacío o nulo',
    'LBL_OP_ISNULL' => 'es nulo',
    'LBL_OP_ISNOTEMPTY' => 'no es vacío',
    'LBL_OP_FIRSTDAYOFMONTH' => 'first Day of current Month',
    'LBL_OP_FIRSTDAYNEXTMONTH' => 'first Day of next Month',
    'LBL_OP_NTHDAYOFMONTH' => 'nth day of current month',
    'LBL_OP_THISMONTH' => 'mes actual',
    'LBL_OP_NOTTHISMONTH' => 'not current month',
    'LBL_OP_THISWEEK' => 'semana actual',
    'LBL_OP_NEXTNMONTH' => 'within the next n full month',    
    'LBL_OP_NEXTNMONTHDAILY' => 'within the next n month Daily',    
    'LBL_OP_NEXT3MONTH' => 'within the next 3 month',
    'LBL_OP_NEXT3MONTHDAILY' => 'within the next 3 month Daily',
    'LBL_OP_NEXT6MONTH' => 'within the next 6 month',
    'LBL_OP_NEXT6MONTHDAILY' => 'within the next 6 month Daily',
    'LBL_OP_LAST3MONTHDAILY' => 'within the last 3 month Daily',
    'LBL_OP_LAST6MONTH' => 'within the last 6 month',
    'LBL_OP_LAST6MONTHDAILY' => 'within the last 6 month daily',
    'LBL_OP_LASTNFMONTH' => 'within the last n full month',
    'LBL_OP_LASTNMONTHDAILY' => 'within the last n month daily',
    'LBL_OP_LASTNYEAR' => 'within the last n full year',
    'LBL_OP_LASTNYEARDAILY' => 'within the last n year daily',
    'LBL_OP_NEXTNYEAR' => 'within the next n full year',
    'LBL_OP_NEXTNYEARDAILY' => 'within the next n year daily',
    'LBL_OP_TODAY' => 'hoy',
    'LBL_OP_PAST' => 'in the past',
    'LBL_OP_FUTURE' => 'in the future',
    'LBL_OP_LASTNDAYS' => 'in the last n days (count)',
    'LBL_OP_LASTNFDAYS' => 'in the last full n days (count)',
    'LBL_OP_LASTNDDAYS' => 'in the last n days (Date)',
    'LBL_OP_LASTNWEEKS' => 'in the last n weeks',
    'LBL_OP_LASTNFQUARTER' => 'in the last n full quarters',
    'LBL_OP_NOTLASTNWEEKS' => 'not in the last n weeks',
    'LBL_OP_LASTNFWEEKS' => 'in the last full n weeks',
    'LBL_OP_NEXTNDAYS' => 'in the next n days (count)',
    'LBL_OP_NEXTNDDAYS' => 'in the next n days (Date)',
    'LBL_OP_NEXTNWEEKS' => 'in the next n weeks',
    'LBL_OP_NEXTNFQUARTER' => 'in the next n full quarters',
    'LBL_OP_NOTNEXTNWEEKS' => 'not in the next n weeks',
    'LBL_OP_BETWNDAYS' => 'between n days (count)',
    'LBL_OP_BETWNDDAYS' => 'between n days (Date)',
    'LBL_OP_BEFORE' => 'antes',
    'LBL_OP_AFTER' => 'después',
    'LBL_OP_LASTMONTH' => 'last month',
    'LBL_OP_LAST3MONTH' => 'within the last 3 month',
    'LBL_OP_THISYEAR' => 'this year',
    'LBL_OP_LASTYEAR' => 'last year',
    'LBL_OP_TYYTD' => 'YTD',
    'LBL_OP_LYYTD' => 'last Year YTD',
    'LBL_OP_GREATER' => '>',
    'LBL_OP_LESS' => '<',
    'LBL_OP_GREATEREQUAL' => '>=',
    'LBL_OP_LESSEQUAL' => '<=',
    'LBL_OP_ONEOF' => 'es uno de estos valores',
    'LBL_OP_ONEOFNOT' => 'no es uno de estos valores',
    'LBL_OP_ONEOFNOTORNULL' => 'is not one of or NULL',
    'LBL_OP_PARENT_ASSIGN' => 'assign from Parent',
    'LBL_OP_FUNCTION' => 'función',
    'LBL_OP_REFERENCE' => 'reference',
    'LBL_BOOL_0' => 'falso',
    'LBL_BOOL_1' => 'verdadero',
    // for the List view Menu
    'LBL_LISTVIEW_OPTIONS' => 'List Options',
    // List Limits
    'LBL_LI_TOP10' => 'top 10',
    'LBL_LI_TOP20' => 'top 20',
    'LBL_LI_TOP50' => 'top 50',
    'LBL_LI_TOP250' => 'top 250',
    'LBL_LI_BOTTOM50' => 'bottom 50',
    'LBL_LI_BOTTOM10' => 'bottom 10',
    'LBL_LI_NOLIMIT' => 'no hay límite',

    // buttons
    'LBL_CHANGE_GROUP_NAME' => 'Cambiar nombre de grupo',
    'LBL_CHANGE_GROUP_NAME_PROMPT' => 'Nombre :',
    'LBL_ADD_GROUP_NAME' => 'Crear nuevo grupo',

    'LBL_SELECTION_CLAUSE' => 'Select Clause: ',
    'LBL_SELECTION_LIMIT' => 'Limitar lista a :',
    'LBL_RECORDS' => 'Registros',
    'LBL_PERCENTAGE' => '%',
    'LBL_EDIT_BUTTON_LABEL' => 'Editar',
    'LBL_DELETE_BUTTON_LABEL' => 'Eliminar campo',
    'LBL_ADD_BUTTON_LABEL' => 'Agregar',
    'LBL_ADDEMTPY_BUTTON_LABEL' => 'Agregar campo fijo',
    'LBL_DOWN_BUTTON_LABEL' => '',
    'LBL_UP_BUTTON_LABEL' => '',
    'LBL_SNAPSHOT_BUTTON_LABEL' => 'Take Snapshot',
    'LBL_CURRENT_SNAPSHOT' => 'actual',
    'LBL_SNAPSHOTMENU_BUTTON_LABEL' => 'Snapshots',
    'LBL_TOOLSMENU_BUTTON_LABEL' => 'Herramientas',
    'LBL_EXPORTMENU_BUTTON_LABEL' => 'Exportar',
    'LBL_COMPARE_SNAPSHOTS_BUTTON_LABEL' => 'Chart by Chart Comparison',
    'LBL_EXPORT_TO_EXCEL_BUTTON_LABEL' => 'Excel',
    'LBL_EXPORT_TO_KLM_BUTTON_LABEL' => 'Google Earth KML',
    'LBL_EXPORT_TO_PDF_BUTTON_LABEL' => 'PDF',
    'LBL_EXPORT_TO_PDFWCHART_BUTTON_LABEL' => 'PDF w. Chart',
    'LBL_EXPORT_TO_TARGETLIST_BUTTON_LABEL' => 'Público objetivo',
    'LBL_SQL_BUTTON_LABEL' => 'SQL',
    'LBL_DUPLICATE_REPORT_BUTTON_LABEL' => 'Duplicar',
    'LBL_LISTTYPE' => 'List Type',
    'LBL_CHART_LAYOUTS' => 'Diseño',
    'LBL_CHART_TYPE' => 'Tipo',
    'LBL_CHART_DIMENSION' => 'Dimension',
    'LBL_CHART_INDEX_LABEL' => 'Chart Index',
    'LBL_CHART_INDEX_EMPTY_TEXT' => 'Select a Chart ID',
    'LBL_CHART_LABEL' => 'Chart',
    'LBL_CHART_HEIGHT_LABEL' => 'Chart Height',

    // Dropdown Values
    'LBL_DD_1' => 'si',
    'LBL_DD_0' => 'no',

    // DropDownValues
    'LBL_DD_SEQ_YES' => 'Si',
    'LBL_DD_SEQ_NO' => 'No',
    'LBL_DD_SEQ_PRIMARY' => '1',
    'LBL_DD_SEQ_2' => '2',
    'LBL_DD_SEQ_3' => '3',
    'LBL_DD_SEQ_4' => '4',
    'LBL_DD_SEQ_5' => '5',
    // Panel Titles
    'LBL_WHERRE_CLAUSE_TITLE' => 'select',
    //Confirm Dialog
    'LBL_DIALOG_CONFIRM' => 'Confirm',
    'LBL_DIALOG_DELETE_YN' => 'are you sure you want to delete this Report?',

    // for the views options
    'LBL_RESET_BUTTON' => 'Resetear',
    'LBL_TREESTRCUTUREGRID_TITLE' => 'Jerarquía de árbol',
    'LBL_REPOSITORYGRID_TITLE' => 'campos disponibles',
    'LBL_CANCEL_BUTTON' => 'Cancelar',
    'LBL_CLOSE_BUTTON' => 'Cerrar',
    'LBL_LISTTYPEPROPERTIES' => 'Propiedades',
    'LBL_XAXIS_TITLE' => 'X-Axis Fields',
    'LBL_YAXIS_TITLE' => 'Y-Axis Fields',
    'LBL_VALUES_TITLE' => 'Valor de campo',
    'LBL_SUMMARIZATION_TITLE' => 'Sumamrization Fields',
    'LBL_FUNCTION' => 'Función',
    'LBL_FUNCTION_SUM' => 'Sum',
    'LBL_FUNCTION_CUMSUM' => 'Sum cumulated',
    'LBL_FUNCTION_COUNT' => 'Count',
    'LBL_FUNCTION_COUNT_DISTINCT' => 'Count Distinct',
    'LBL_FUNCTION_AVG' => 'Promedio',
    'LBL_FUNCTION_MIN' => 'Mínimo',
    'LBL_FUNCTION_MAX' => 'Máximo',
    'LBL_FUNCTION_GROUP_CONCAT' => 'Group Concat',
    //2013-03-01 Sort function for Group Concat
    'LBL_FUNCTION_GROUP_CONASC' => 'Group Concat (asc)',
    'LBL_FUNCTION_GROUP_CONDSC' => 'Group Concat (desc)',
    // Value Types
    'LBL_VALUETYPE_TOFSUM' => 'display Sum',
    'LBL_VALUETYPE_POFSUM' => '% de suma',
    'LBL_VALUETYPE_POFCOUNT' => '% of Count',
    'LBL_VALUETYPE_POFAVG' => '% de promedio',
    'LBL_VALUETYPE_DOFSUM' => 'Δ to Sum',
    'LBL_VALUETYPE_DOFCOUNT' => 'Δ to Count',
    'LBL_VALUETYPE_DOFAVG' => 'Δ to Average',
    'LBL_VALUETYPE_C' => 'Cumulated',
    // panel title
    'LBL_STANDARDGRIDPANELTITLE' => 'Report Result',
    'LBL_STANDRDGRIDPANEL_FOOTERWCOUNT' => 'Displaying Records {0} - {1} of {2}',
    'LBL_STANDRDGRIDPANEL_FOOTERWOCOUNT' => 'Displaying Records {0} - {1}',
    'LBL_STANDARDGRIDPROPERTIES_COUNT' => 'process Count',
    'LBL_STANDARDGRIDPROPERTIES_SYNCHRONOUSCOUNT' => 'syncronous',
    'LBL_STANDARDGRIDPROPERTIES_ASYNCHRONOUSCOUNT' => 'asyncronous',
    'LBL_STANDARDGRIDPROPERTIES_NOCOUNT' => 'no count',
    'LBL_STANDARDGRIDENTRIES_COUNT' => 'records per page',
    // General Labels
    'LBL_YES' => 'si',
    'LBL_NO' => 'no',
    'LBL_HID' => 'oculto',
    'LBL_SORT_ASC' => 'asc.',
    'LBL_SORT_DESC' => 'desc.',
    'LBL_SORT_SORTABLE' => 'sortable',
    'LBL_AND' => 'Y',
    'LBL_OR' => 'O',
    'LBL_JT_OPTIONAL' => 'opcional',
    'LBL_JT_REQUIRED' => 'requerido',
    //Trendlines
    'LBL_TRENDLINE_STARTVALUE' => 'StartValue',
    'LBL_TRENDLINE_ENDVALUE' => 'EndValue',
    'LBL_ADD_TRENDLINE' => 'add Trendline',
    'LBL_DELETE_TRENDLINE' => 'delete Trendline',
    'LBL_TRENDLINE_MIN' => 'Minimum',
    'LBL_TRENDLINE_MAX' => 'Maximum',
    'LBL_TRENDLINE_AVG' => 'Average',
    'LBL_TRENDLINE_AMM' => 'Area Min/Max',
    'LBL_TRENDLINE_LRG' => 'linear Regression',
    'LBL_TRENDLINE_CST' => 'Custom',
    'LBL_STANDARDTYPE' => 'Tipo',
    'LBL_TRENDLINE_STYLE' => 'Estilo',
    'LBL_TRENDLINE_VAL' => 'Valor',
    'LBL_TRENDLINE_TXT' => 'Nombre',
    'LBL_TRENDLINE_NOT' => '-',
    'LBL_TRENDLINE_DISPLAY' => 'Info',
    // for report publishing
    'LBL_PUBLISH_OPTION' => 'publish Report',
    'LBL_PUBLISHPOPUP_TITLE' => 'Publish Report Options',
    'LBL_PUBLISHPOPUP_SUBPANEL' => 'Subpanel',
    'LBL_PUBLISHPOPUP_DASHLET' => 'Dashlet',
    'LBL_PUBLISHPOPUP_GRID' => 'publish Grid',
    'LBL_PUBLISHPOPUP_CHART' => 'publish Chart',
    'LBL_PUBLISHPOPUP_SUBPANELORDER' => 'Subpanel Order',
    'LBL_PUBLISHPOPUP_CLOSE' => 'Cerrar',
    'LBL_PUBLISHPOPUP_MENU' => 'publish as Menu item',
    'LBL_PUBLISH_ASDASHLET' => 'Publish as Dashlet',
    'LBL_PUBLISH_MOBILE' => 'Publish to SpiceCRM Mobile',
    'LBL_PUBLISH_ASSUBPANEL' => 'Publish as Subpanel',
    'LBL_PUBLISH_DASHLET_NAME' => 'Dshlet Title',
    'LBL_PUBLISH_DASHLETREPORT' => 'Dshlet Report',

    // for Export to Planning
    'LBL_EXPORTTOPLANINGPOPUP_TITLE' => 'Export to Planning Nodes Settings',
    // for the pdf
    'LBL_PDF_DATE_LEADIN' => ' creado el ',
    'LBL_PDF_DATE_LEADOUT' => '',
    'LBL_PDF_PAGE_LEADIN' => 'Página ',
    'LBL_PDF_PAGE_SEPARATOR' => ' de ',
    // for the targetlist Export
    'LBL_TARGETLISTEXPORTPOPUP_TITLE' => 'Export to Targetlist',
    'LBL_TARGETLISTPOUPFIELDSET_LABEL' => 'Export Options',
    'LBL_TGLLISTPOPUP_CLOSE' => 'Cerrar',
    'LBL_TGLLISTPOPUP_EXEC' => 'Ejecutar',
    'LBL_TARGETLISTPOUP_OPTIONS' => 'Action',
    'LBL_TGLEXP_NEW' => 'crear nuevo',
    'LBL_TGLEXP_UPD' => 'update existing',
    'LBL_TARGETLISTPOUPNEWFIELDSET_LABEL' => 'New Targetlist',
    'LBL_TARGETLISTPERFSETTINGS_LABEL' => 'Performance Settings',
    'LBL_TARGETLISTPERFCHECKBOX_LABEL' => 'create direct',
    'LBL_TARGETLISTPOUP_NEWNAME' => 'Targetlist Name',
    'LBL_TARGETLISTPOUPCHANGEFIELDSET_LABEL' => 'Update Targetlist',
    'LBL_TARGETLISTPOUP_LISTS' => 'Target Lists',
    'LBL_TARGETLISTPOUP_ACTIONS' => 'Action',
    'LBL_TGLACT_REP' => 'actualizar',
    'LBL_TGLACT_ADD' => 'agregar',
    'LBL_TGLACT_SUB' => 'subtract',
    'LBL_TARGETLISTPOUP_CAMPAIGNS' => 'add to campaign',
    'LBL_LAST_DAY_OF_MONTH' => 'last day of month',
    'LBL_EXPORT_TO_PLANNER_BUTTON_LABEL' => 'Export to KPlanner',
    'LBL_PLANNEREXPORTPOPUP_TITLE' => 'Export to KPlanner',
    'LBL_EXPORTPOPUP_CLOSE' => 'Cancelar',
    'LBL_EXPORTPOPUP_EXEC' => 'Export to KPlanner',
    'LBL_PLANNEREXPORTPOPUP_SCOPESETS' => 'Scope Set',
    'LBL_PLANNINCHARACTERISTICSGRID_TITLE' => 'Planning Characteristics',
    'LBL_CHARFIELDVALUE' => 'Characteristic Value',
    'LBL_CHARFIELDNAME' => 'Characteristic Name',
    'LBL_CHARFIXEDVALUE' => 'Valor Fijo',
    'LBL_PLANNEREXPORTPOPUP_NODENAME' => 'Nodename',
    // for the Drilldown
    'LBL_KPDRILLDOWN' => 'Drilldown',
    // for the Viualization
    'LBL_VISUALIZATION' => 'Visualizar',
    'LBL_VISUALIZATIONPLUGIN' => 'tipo',
    'LBL_VISUALIZATIONTOOLBAR_LAYOUT' => 'Diseño',
    'LBL_VISUALIZATION_HEIGHT' => 'alto (px)',
    'LBL_GOOGLECHARTS' => 'Google Charts',
    'LBL_CHARTFS_TYPE' => 'Chart Type',
    'LBL_CHARTFS_DATA' => 'Chart Data',
    'LBL_CHARTFS_SERIES' => 'Dataseries',
    'LBL_CHARTFS_VALUES' => 'Valores',
    'LBL_DIMENSIONS' => 'Dimensiones',
    'LBL_DIMENSION_111' => 'one dimensional (series)',
    'LBL_DIMENSION_10N' => 'one dimensional (values)',
    'LBL_DIMENSION_220' => 'two dimensional (no values)',
    'LBL_DIMENSION_221' => 'two dimensional (series)',
    'LBL_DIMENSION_21N' => 'two dimensional (values)',
    'LBL_DIMENSION_331' => 'three dimensional (series)',
    'LBL_DIMENSION_32N' => 'three dimensional (values)',
    'LBL_CHARTTYPE_DIMENSION1' => 'Dimension 1',
    'LBL_CHARTTYPE_DIMENSION2' => 'Dimension 2',
    'LBL_CHARTTYPE_DIMENSION3' => 'Dimension 3',
    'LBL_CHARTTYPE_MULTIPLIER' => 'Multiplier',
    'LBL_CHARTTYPE_COLORS' => 'Colores',
    'LBL_CHARTTYPE_COLORPREVIEW' => 'Preview',
    'LBL_CHARTTYPE_DATASERIES' => 'Dataseries',
    'LBL_CHARTTYPES' => 'Type',
    'LBL_CHARTTYPE_AREA' => 'Area Chart',
    'LBL_CHARTTYPE_STEPPEDAREA' => 'Stepped Area Chart',
    'LBL_CHARTTYPE_BAR' => 'Bar Chart',
    'LBL_CHARTTYPE_BUBBLE' => 'Bubble Chart',
    'LBL_CHARTTYPE_SANKEY' => 'Sankey Chart',
    'LBL_CHARTTYPE_COLUMN' => 'Column Chart',
    'LBL_CHARTTYPE_GAUGE' => 'Gauges',
    'LBL_CHARTTYPE_PIE' => 'Pie Chart',
    'LBL_CHARTTYPE_LINE' => 'Line Chart',
    'LBL_CHARTTYPE_SCATTER' => 'Scatter Chart',
    'LBL_CHARTTYPE_COMBO' => 'Combo Chart',
    'LBL_CHARTTYPE_CANDLESTICK' => 'Candlestick',
    'LBL_CHARTFUNCTION' => 'Function',
    'LBL_MEANING' => 'Meaning',
    'LBL_COLOR' => 'Color',
    'LBL_AXIS' => 'Axis',
    'LBL_CHARTAXIS_P' => 'Primary',
    'LBL_CHARTAXIS_S' => 'Secondary',
    'LBL_RENDERER' => 'render as',
    'LBL_CHARTRENDER_DEFAULT' => 'default',
    'LBL_CHARTRENDER_BARS' => 'Bars',
    'LBL_CHARTRENDER_COLUMN' => 'Column',
    'LBL_CHARTRENDER_LINE' => 'Line',
    'LBL_CHARTRENDER_AREA' => 'Area',
    'LBL_CHARTRENDER_STEPPEDAREA' => 'Stepped Area',
    'LBL_CHARTOPTIONS_FS' => 'Chartoptions',
    'LBL_CHARTOPTIONS_TITLE' => 'Title',
    'LBL_CHARTOPTIONS_CONTEXT' => 'Context',
    'LBL_CHARTOPTIONS_VMINMAX' => 'V Axis Min/Max',
    'LBL_CHARTOPTIONS_HMINMAX' => 'H Axis Min/Max',
    'LBL_CHARTOPTIONS_GREEN' => 'Green from/to',
    'LBL_CHARTOPTIONS_YELLOW' => 'Yellow from/to',
    'LBL_CHARTOPTIONS_RED' => 'Red from/to',
    'LBL_CHARTOPTIONS_LEGEND' => 'display Legend',
    'LBL_CHARTOPTIONS_EMTPY' => 'show empty Values',
    'LBL_CHARTOPTIONS_NOVLABLES' => 'hide V-Axis Labels',
    'LBL_CHARTOPTIONS_NOHLABLES' => 'hide H-Axis Labels',
    'LBL_CHARTOPTIONS_LOGV' => 'logarithmic V Scale',
    'LBL_CHARTOPTIONS_LOGH' => 'logarithmic H Scale',
    'LBL_CHARTOPTIONS_3D' => '3 dimensional',
    'LBL_CHARTOPTIONS_STACKED' => 'stacked Series',
    'LBL_CHARTOPTIONS_REVERSED' => 'reverse Series',
    'LBL_CHARTOPTIONS_CTFUNCTION' => 'smoothed Line',
    'LBL_CHARTOPTIONS_POINTS' => 'show Points',
    'LBL_CHARTOPTIONS_MATERIAL' => 'material Design',
    'LBL_CHARTOPTIONS_ALLOWOVERLAP' => 'allow data labels to overlap',

    // for Fusion Charts ... needs to be moved
    'LBL_CHARTTYPE_COLUMN2D' => 'Column 2D',
    'LBL_CHARTTYPE_COLUMN3D' => 'Column 3D',
    'LBL_CHARTTYPE_PIE2D' => 'Pie 2D',
    'LBL_CHARTTYPE_PIE3D' => 'Pie 3D',
    'LBL_CHARTTYPE_DOUGNUT2D' => 'Dougnut 2D',
    'LBL_CHARTTYPE_DOUGNUT3D' => 'Dougnut 3D',
    'LBL_CHARTTYPE_BAR2D' => 'Bar 2D',
    'LBL_CHARTTYPE_AREA2D' => 'Area 2D',
    'LBL_CHARTTYPE_STACKEDAREA2D' => 'stacked area 2D',
    'LBL_CHARTTYPE_PARETO2D' => 'Pareto 2D',
    'LBL_CHARTTYPE_PARETO3D' => 'Pareto 3D',
    'LBL_CHARTTYPE_STACKEDCOLUMN2D' => 'stacked Column 2D',
    'LBL_CHARTTYPE_STACKEDCOLUMN3D' => 'stacked Column 3D',
    'LBL_CHARTTYPE_MSCOLUMN2D' => 'multiseries Column 2D',
    'LBL_CHARTTYPE_MSCOLUMN3D' => 'multiseries Column 3D',
    'LBL_CHARTTYPE_MSBAR2D' => 'multiseries Bar 2D',
    'LBL_CHARTTYPE_MSBAR3D' => 'multiseries Bar 3D',
    'LBL_CHARTTYPE_STACKEDBAR2D' => 'stacked Bar 2D',
    'LBL_CHARTTYPE_STACKEDBAR3D' => 'stacked Bar 3D',
    'LBL_CHARTTYPE_MARIMEKKO' => 'Marimekko Chart',
    'LBL_CHARTTYPE_MSLINE' => 'multiseries Line',
    'LBL_CHARTTYPE_MSAREA' => 'multiseries Area',
    'LBL_CHARTTYPE_MSCOMBIDY2D' => 'multiseries Combination dual',
    'LBL_CHARTOPTIONS_ROUNDEDGES' => 'round Edges',
    'LBL_CHARTOPTIONS_HIDELABELS' => 'hide Labels',
    'LBL_CHARTOPTIONS_HIDEVALUES' => 'hide Values',
    'LBL_CHARTOPTIONS_FORMATNUMBERSCALE' => 'scale Numbers',
    'LBL_CHARTOPTIONS_ROTATEVALUES' => 'rotate Value',
    'LBL_CHARTOPTIONS_PLACEVALUESINSIDE' => 'place Values inside',
    'LBL_CHARTOPTIONS_SHOWSHADOE' => 'show Shadow',
    'LBL_CHARTOPTIONS_LPOS' => 'Legend',
    'LBL_LPOS_NONE' => 'none',
    'LBL_LPOS_RIGHT' => 'right',
    'LBL_LPOS_LEFT' => 'left',
    'LBL_LPOS_BOTTOM' => 'bottom',
    'LBL_LPOS_TOP' => 'top',


    'LBL_STANDARDPLUGIN' => 'Standard View',


    // for the Google Maps
    'LBL_GOOGLEMAPSFS_GEOCODEBY' => 'Geo by',
    'LBL_GOOGLEMAPSFS_GEOCODELATLONG' => 'Lat/Long',
    'LBL_GOOGLEMAPSFS_GEOCODEADDRESS' => 'Address',
    'LBL_GOOGLEMAPS_LONGITUDE' => 'Longitude',
    'LBL_GOOGLEMAPS_LATITUDE' => 'Latitude',
    'LBL_GOOGLEMAPSFS_LATLONG' => 'Geocoordinates',
    'LBL_GOOGLEMAPS_STREET' => 'Street',
    'LBL_GOOGLEMAPS_CITY' => 'City',
    'LBL_GOOGLEMAPS_PC' => 'Postalcode',
    'LBL_GOOGLEMAPS_COUNTRY' => 'Country',
    'LBL_GOOGLEMAPS_ADDRESS' => 'Address',
    'LBL_GOOGLEMAPSFS_TITLE' => 'Pin Info',
    'LBL_GOOGLEMAPS_TITLE' => 'Title',
    'LBL_GOOGLEMAPS_CLUSTER' => 'Cluster Pins',
    'LBL_GOOGLEMAPS_LABEL' => 'Google Maps',
    'LBL_GOOGLEMAPS_COLORSOPTIONS' => 'Colors',
    'LBL_GOOGLEMAPS_COLORS' => 'Colors',
    'LBL_GOOGLEMAPS_COLORCRITERIA' => 'Color by',
    'LBL_GOOGLEMAPS_INFO' => 'Popupinfo',
    'LBL_GOOGLEMAPS_LEGEND' => 'Display legend',
    'LBL_GOOGLEMAPS_COLORSOPTIONSRESET' => 'reset',
    'LBL_GOOGLEMAPS_SPIDERFY' => 'Spiderfy pins',
    'LBL_GOOGLEMAPS_ROUTEPLANNER_OPTIONS' => 'Route planner options',
    'LBL_GOOGLEMAPS_ROUTEPLANNER_RESET' => 'reset',
    'LBL_GOOGLEMAPS_ROUTEPLANNER_DISPLAY' => 'Display route planner',
    'LBL_GOOGLEMAPS_ROUTEPLANNER_WAYPOINTLABEL' => 'Waypoint name',
    'LBL_GOOGLEMAPS_ROUTEPLANNER_WAYPOINTADDRESS' => 'Waypoint address',
    'LBL_GOOGLEMAPS_ROUTEPLANNER_TITLE' => 'Route planner',
    'LBL_GOOGLEMAPS_ROUTEPLANNER_SUMMARY' => 'Route details',
    'LBL_GOOGLEMAPS_ROUTEPLANNER_START' => 'Start',
    'LBL_GOOGLEMAPS_ROUTEPLANNER_END' => 'End',
    'LBL_GOOGLEMAPS_ROUTEPLANNER_WAYPOINTS' => 'Waypoints',
    'LBL_GOOGLEMAPS_ROUTEPLANNER_BTN' => 'Plan route',
    'LBL_GOOGLEMAPS_ROUTEPLANNER_RESETBTN' => 'Delete route',
    'LBL_GOOGLEMAPS_ROUTEPLANNER_WAYPTGCBY' => 'Waypoint geocode by',
    'LBL_GOOGLEMAPS_ROUTEPLANNER_SEG' => 'Route segment',
    'LBL_GOOGLEMAPS_ROUTEPLANNER_SEGFROM' => 'from',
    'LBL_GOOGLEMAPS_ROUTEPLANNER_SEGTO' => 'to',
    'LBL_GOOGLEMAPS_RESIZEMAP_BTN' => 'Resize map',
    'LBL_GOOGLEMAPS_CIRCLEDESIGNER_OPTIONS' => 'Periphery search options',
    'LBL_GOOGLEMAPS_CIRCLEDESIGNER_RESET' => 'reset',
    'LBL_GOOGLEMAPS_CIRCLEDESIGNER_DISPLAY' => 'Display periphery search',
    'LBL_GOOGLEMAPS_CIRCLEDESIGNER_LABEL' => 'Periphery start point name',
    'LBL_GOOGLEMAPS_CIRCLEDESIGNER_TITLE' => 'Periphery search',
    'LBL_GOOGLEMAPS_CIRCLEDESIGNER_SUMMARY' => 'Periphery details',
    'LBL_GOOGLEMAPS_CIRCLEDESIGNER_DISTANCE' => 'Distance in Km',
    'LBL_GOOGLEMAPS_CIRCLEDESIGNER_WAYPOINTS' => 'Search from',
    'LBL_GOOGLEMAPS_CIRCLEDESIGNER_BTN' => 'Periphery search',
    'LBL_GOOGLEMAPS_CIRCLEDESIGNER_RESETBTN' => 'Delete circle(s)',
    'LBL_GOOGLEMAPS_CIRCLEDESIGNER_MODULE' => 'Módulo',
    'LBL_GOOGLEMAPS_CIRCLEDESIGNER_DISPLAYFIELDS' => 'Query config',

    // for the Plugins
    'LBL_PRESENTATION_PLUGIN' => 'Plugin',
    'LBL_PRESENTATION_PARAMS' => 'Presentation Parameters',
    'LBL_DEFAULT_GROUPBY' => 'Default Group By',
    'LBL_INTEGRATION' => 'Integrar',
    'LBL_INTEGRATION_PLUGINNAME' => 'Plugin',
    'LBL_CSV_EXPORT' => 'Exportar a CSV',
    'LBL_EXCEL_EXPORT' => 'Exportar a Excel',
    'LBL_TARGETLIST_EXPORT' => 'Exportar a Público objetivo',
    'LBL_SNAPSHOT_EXPORT' => 'take Snapshot',
    'LBL_QUERY_ANALIZER' => 'Query Analyzer',
    'LBL_SCHEDULE_REPORT' => 'schedule Reporte',
    'LBL_PUBLISH_REPORT' => 'publicar Reporte',
    'LBL_PUBLISH_DASHLET' => 'Publicar como Dashlet',
    'LBL_PUBLISH_DASHLETREPORT' => 'Select Report',
    'LBL_PUBLISH_DASHLETTITLE' => 'Dashlet Title',
    'LBL_PUBLISH_DASHLET_PRESENTATION' => 'Presentation',
    'LBL_PUBLISH_DASHLET_PRESENTATION_VISUALIZATION' => 'Visualization',
    'LBL_PUBLISH_DASHLET_NAME' => 'Nombre',
    'LBL_PUBLISH_SUBPANEL_SEQUENCE' => 'Secuencia',
    'LBL_PUBLISH_SUBPANEL_MODULE' => 'Módulo',
    'LBL_PUBLISH_SUBPANEL_TAB' => 'Pestaña',


    // Scheduler Options
    'LBL_KSCHEDULING_SAVETOACTION_DONOTHING' => '&nbsp;',
    'LBL_KSCHEDULING_SAVETOACTION_ADD' => 'agregar',
    'LBL_KSCHEDULING_SAVETOACTION_REPLACE' => 'reemplazar',

    // PDF Export Option
    'LBL_PDF_EXPORT' => 'PDF Export',
    'LBL_PDF_EXPORTOPTIONS_GENERAL' => 'General',
    'LBL_PDF_LAYOUT' => 'PDF Layout',
    'LBL_PDF_FORMAT' => 'Format',
    'LBL_PDFFORMAT_LTR' => 'Letter',
    'LBL_PDFFORMAT_LGL' => 'Legal',
    'LBL_PDFFORMAT_A4' => 'A4',
    'LBL_PDFFORMAT_A5' => 'A5',
    'LBL_PDF_ORIENTATION' => 'Orientation',
    'LBL_PDF_MULTILINE' => 'multiline',
    'LBL_PDFORIENT_P' => 'Portrait',
    'LBL_PDFORIENT_L' => 'Landscape',
    'LBL_PDF_PALIGNMENT' => 'Data Alignment',
    'LBL_PDFPALIGNMENT_L' => 'Izquierda',
    'LBL_PDFPALIGNMENT_R' => 'Derecha',
    'LBL_PDFPALIGNMENT_C' => 'Center',
    'LBL_PDFPALIGNMENT_S' => 'Stretch',
    'LBL_PDF_NEWPAGEPERGROUP' => 'new Page per Group',
    'LBL_PDF_HEADERPERPAGE' => 'header on each page',

    // Pivot Plugin ... to be moved later
    'LBL_PIVOT_SETTINGS' => 'Pivot table settings',
    'LBL_PIVOT_ADVANCED' => 'Advanced Settings',
    'LBL_PIVOT_REPOSITORY' => 'campos disponibles',
    'LBL_PIVOT_COLUMNS' => 'Columns',
    'LBL_PIVOT_ROWS' => 'Rows',
    'LBL_PIVOT_ADDROWINFO' => 'additonal Row Info',
    'LBL_PIVOT_VALUES' => 'Values',
    'LBL_PIVOT_FUNCTiON' => 'Function',
    'LBL_PIVOT_TOTALS' => 'show totals',
    'LBL_PIVOT_SUMS' => 'show sum',
    'LBL_PIVOT_ROTATEHEADERS' => 'rotate Headers',
    'LBL_PIVOT_EMPTYCOLUMNS' => 'show empty Columnns',
    'LBL_PIVOT_ADJUSTCOLUMNS' => 'adjust column width',
    'LBL_PIVOT_SORTCOLUMNS' => 'sort Columns',
    'LBL_PIVOT_LBLPIVOTDATA' => 'Pivot Data',
    'LBL_PIVOT_NAMECOLUMNWIDTH' => 'Item Column Width',
    'LBL_PIVOT_MINCOLUMNWIDTH' => 'min Column Width',

    // the field renderer
    'LBL_RENDERER_-' => '-',
    'LBL_RENDERER_CURRENCY' => 'Currency',
    'LBL_RENDERER_SCURRENCY' => 'System Currency',
    'LBL_RENDERER_UCURRENCY' => 'User Currency',
    'LBL_RENDERER_PERCENTAGE' => 'Percentage',
    'LBL_RENDERER_NUMBER' => 'Number',
    'LBL_RENDERER_INT' => 'Integer',
    'LBL_RENDERER_DATE' => 'Date',
    'LBL_RENDERER_DATETIME' => 'Datetime',
    'LBL_RENDERER_DATETUTC' => 'Datetime (UTC)',
    'LBL_RENDERER_FLOAT' => 'Float',
    'LBL_RENDERER_BOOL' => 'Boolean',
    'LBL_RENDERER_TEXT' => 'Text',
    'LBL_RENDERER_NONE' => 'do not Format',

    // override Alignment
    'LBL_OVERRIDEALIGNMENT' => 'override Alignment',
    'LBL_ALIGNMENT_-' => '-',
    'LBL_ALIGNMENT_LEFT' => 'left',
    'LBL_ALIGNMENT_RIGHT' => 'right',
    'LBL_ALIGNMENT_CENTER' => 'center',

    'LBL_REPORTTIMEOUT' => 'Timeout',
    'LBL_RT30' => '30 seconds',
    'LBL_RT60' => '1 minute',
    'LBL_RT120' => '2 minutes',
    'LBL_RT240' => '3 minutes',
    'LBL_RT300' => '4 minutes',

    'LBL_KSNAPSHOTS' => 'Snapshots',
    'LBL_KSNAPSHOT' => 'Snapshot',
    'LBL_TAKING_SNAPSHOT' => 'taking snapshot ... ',
    'LBL_GROUPING' => 'Grouping',
    'LBL_PICK_DATETIME' => 'choose Date/Time'
);


$mod_strings['LBL_PIVOTVIEW'] = 'Pivot';
$mod_strings['LBL_STANDARDWSUMMARY'] = 'Standard with Summary';
$mod_strings['LBL_STANDARDWPREVIEW'] = 'Standard with Preview';
$mod_strings['LBL_TREEVIEW'] = 'Treeview';
$mod_strings['LBL_TREEVIEWPROPERTIES_GRPUNTIL'] = 'group until';

$mod_strings['LBL_GROUPEDVIEW'] = 'Grouped View';
$mod_strings['LBL_STANDARDVIEW'] = 'Standard View';
$mod_strings['LBL_EDITPLUGIN'] = 'Edit View (beta)';

$mod_strings['LBL_GOOGLECHARTS'] = 'Google Charts';
$mod_strings['LBL_FUSIONCHARTS'] = 'Fusion Charts';
$mod_strings['LBL_HIGHCHARTS'] = 'High Charts';
$mod_strings['LBL_GOOGLEMAPS'] = 'Google Maps';
$mod_strings['LBL_GOOGLEGEO'] = 'Google Geo';
$mod_strings['LBL_SUGARCHARTS'] = 'Sugar Charts';

$mod_strings['LBL_GEOOPTIONS_TITLE'] = 'Title';
$mod_strings['LBL_GEOOPTIONS_REGION'] = 'Display Region';
$mod_strings['LBL_GEOOPTIONS_RESOLUTION'] = 'Resolution';
$mod_strings['LBL_GEOOPTIONS_COUNTRIES'] = 'Countries';
$mod_strings['LBL_GEOOPTIONS_PROVINCES'] = 'Provinces';
$mod_strings['LBL_GEOOPTIONS_METROS'] = 'Metros';

$mod_strings['LBL_PUBLISH_CNTENTRIES'] = 'number of entries displayed';

$mod_strings['LBL_PDF_CHARTSONSEPARATEPAGE'] = 'Charts on separate Page';

$mod_strings['LBL_KPDRILLDOWN'] = 'Presentation Drilldown';
$mod_strings['LBL_LINK_LINKTYPE'] = 'Link';
$mod_strings['LBL_POPUP_LINKTYPE'] = 'Popup';
$mod_strings['LBL_CHART_LINKTYPE'] = 'Chart';
$mod_strings['LBL_DRILLDOWNMENUTITLE'] = 'Drill Down';
$mod_strings['LBL_ADDLINKEDREPORT_TITLE'] = 'Link Report';
$mod_strings['LBL_ADDLINK_ADD'] = 'Add';
$mod_strings['LBL_MAPPING_TITLE'] = 'InputMapping';

$mod_strings['LBL_EDITABLE'] = 'editable';

$mod_strings['LBL_PUBLISH_ASDASHLET'] = 'publish as Dashlet';
$mod_strings['LBL_PUBLISH_ASSUBPANEL'] = 'publish as Subpanel';
$mod_strings['LBL_PUBLISH_MOBILE'] = 'publish Mobile';

$mod_strings['LBL_CHARTOPTIONS_ROTATELABELS'] = 'Rotate Labels';
$mod_strings['LBL_STACK_NONE'] = 'not stacked';
$mod_strings['LBL_STACK_NORMAL'] = 'stracked';
$mod_strings['LBL_STACK_PERCENT'] = 'percentage';
$mod_strings['LBL_CHARTRENDER_SPLINE'] = 'Spline';
$mod_strings['LBL_CHARTTYPE_SPLINE'] = 'Spline';
$mod_strings['LBL_CHARTTYPE_AREASPLINE'] = 'Area Spline';

$mod_strings['LBL_CHARTTYPE_ARTRD'] = 'Area w. Trendline';
$mod_strings['LBL_CHARTTYPE_ARSPLINETRD'] = 'Area Spline w. Trendline';

$mod_strings['LBL_CHARTTYPE_ARSPLINEPOLR'] = 'Area Spline Polar';
$mod_strings['LBL_CHARTTYPE_ARSPLINESTCKPOL'] = 'Area Spline stacked Polar';

$mod_strings['LBL_CHARTTYPE_COLTRD'] = 'Column w. Trendline';
$mod_strings['LBL_CHARTTYPE_COLPLR'] = 'Column Polar';
$mod_strings['LBL_CHARTTYPE_LPOLR'] = 'Line polar';
$mod_strings['LBL_CHARTTYPE_DONUT'] = 'Donut';
$mod_strings['LBL_CHARTTYPE_PIE180'] = 'Pie 180°';
$mod_strings['LBL_CHARTTYPE_DONUT180'] = 'Donut 180°';

$mod_strings['LBL_CHARTTYPE_COLSTACKED'] = 'Columns stacked';
$mod_strings['LBL_CHARTTYPE_COLSTKPOLR'] = 'Columns stacked polar';
$mod_strings['LBL_CHARTTYPE_COLSTCKPER'] = 'Columns stacked 100%';
$mod_strings['LBL_CHARTTYPE_BRSTACKED'] = 'Bars stacked';
$mod_strings['LBL_CHARTTYPE_BRSTCKPER'] = 'Bars stacked 100%';
$mod_strings['LBL_CHARTTYPE_ARSTACKED'] = 'Area stacked';
$mod_strings['LBL_CHARTTYPE_ARSTCKPER'] = 'Area stacked 100%';
$mod_strings['LBL_CHARTTYPE_ARSPLINESTACKED'] = 'Area Spline stacked';
$mod_strings['LBL_CHARTTYPE_ARSPLINESTCKPER'] = 'Area Spline stacked 100%';
$mod_strings['LBL_CHARTTYPE_LPLR'] = 'Line Polar';
$mod_strings['LBL_CHARTTYPE_COLSTKPPL'] = 'Columns stacked 100% polar';
$mod_strings['LBL_CHARTTYPE_SPLPLR'] = 'Spline polar';
$mod_strings['LBL_CHARTTYPE_ARSTCKPOL'] = 'Area stacked polar';
$mod_strings['LBL_CHARTTYPE_ARSTCKPPL'] = 'Area stacked 100% polar';
$mod_strings['LBL_CHARTTYPE_ARSPLINESTCKPPL'] = 'Area Spline stacked polar';
$mod_strings['LBL_CHARTTYPE_ARSPLINESTCKPPL'] = 'Area Spline stacked 100% polar';
$mod_strings['LBL_CHARTTYPE_ARPOLR'] = 'Area polar';
$mod_strings['LBL_CHARTTYPE_FUNNEL'] = 'Funnel';
$mod_strings['LBL_CHARTTYPE_PYRAMID'] = 'Pyramid';
$mod_strings['LBL_TRENDLINENAME'] = 'Trend';
$mod_strings['LBL_KSNAPSHOTANALYZER'] = 'Snapshot Analyzer';
$mod_strings['LBL_SNAPSHOTANALYZER_TITLE'] = 'ad hoc Snapshot Analyzer';
$mod_strings['LBL_KPLANNER_EXPORT'] = 'KPlanner Export';

$mod_strings['LBL_GOOGLEMAPS_LABEL'] = 'Google Maps';
$mod_strings['LBL_GOOGLEMAPS_COLORSOPTIONS'] = 'Colors';
$mod_strings['LBL_GOOGLEMAPS_COLORS'] = 'Colors';
$mod_strings['LBL_GOOGLEMAPS_COLORCRITERIA'] = 'Color by';
$mod_strings['LBL_GOOGLEMAPS_INFO'] = 'Popupinfo';
$mod_strings['LBL_GOOGLEMAPS_LEGEND'] = 'Display legend';
$mod_strings['LBL_GOOGLEMAPS_COLORSOPTIONSRESET'] = 'reset';
$mod_strings['LBL_GOOGLEMAPS_SPIDERFY'] = 'Spiderfy pins';
$mod_strings['LBL_GOOGLEMAPS_ROUTEPLANNER_OPTIONS'] = 'Route planner options';
$mod_strings['LBL_GOOGLEMAPS_ROUTEPLANNER_RESET'] = 'reset';
$mod_strings['LBL_GOOGLEMAPS_ROUTEPLANNER_DISPLAY'] = 'Display route planner';
$mod_strings['LBL_GOOGLEMAPS_ROUTEPLANNER_WAYPOINTLABEL'] = 'Waypoint name';
$mod_strings['LBL_GOOGLEMAPS_ROUTEPLANNER_WAYPOINTADDRESS'] = 'Waypoint address';
$mod_strings['LBL_GOOGLEMAPS_ROUTEPLANNER_TITLE'] = 'Route planner';
$mod_strings['LBL_GOOGLEMAPS_ROUTEPLANNER_SUMMARY'] = 'Route details';
$mod_strings['LBL_GOOGLEMAPS_ROUTEPLANNER_START'] = 'Start';
$mod_strings['LBL_GOOGLEMAPS_ROUTEPLANNER_END'] = 'End';
$mod_strings['LBL_GOOGLEMAPS_ROUTEPLANNER_WAYPOINTS'] = 'Waypoints';
$mod_strings['LBL_GOOGLEMAPS_ROUTEPLANNER_BTN'] = 'Plan route';
$mod_strings['LBL_GOOGLEMAPS_ROUTEPLANNER_RESETBTN'] = 'Delete route';
$mod_strings['LBL_GOOGLEMAPS_ROUTEPLANNER_WAYPTGCBY'] = 'Waypoint geocode by';
$mod_strings['LBL_GOOGLEMAPS_ROUTEPLANNER_SEG'] = 'Route segment';
$mod_strings['LBL_GOOGLEMAPS_ROUTEPLANNER_SEGFROM'] = 'from';
$mod_strings['LBL_GOOGLEMAPS_ROUTEPLANNER_SEGTO'] = 'to';
$mod_strings['LBL_GOOGLEMAPS_RESIZEMAP_BTN'] = 'Resize map';
$mod_strings['LBL_GOOGLEMAPS_CIRCLEDESIGNER_OPTIONS'] = 'Periphery search options';
$mod_strings['LBL_GOOGLEMAPS_CIRCLEDESIGNER_RESET'] = 'reset';
$mod_strings['LBL_GOOGLEMAPS_CIRCLEDESIGNER_DISPLAY'] = 'Display periphery search';
$mod_strings['LBL_GOOGLEMAPS_CIRCLEDESIGNER_LABEL'] = 'Periphery start point name';
$mod_strings['LBL_GOOGLEMAPS_CIRCLEDESIGNER_TITLE'] = 'Periphery search';
$mod_strings['LBL_GOOGLEMAPS_CIRCLEDESIGNER_SUMMARY'] = 'Periphery details';
$mod_strings['LBL_GOOGLEMAPS_CIRCLEDESIGNER_DISTANCE'] = 'Distance in Km';
$mod_strings['LBL_GOOGLEMAPS_CIRCLEDESIGNER_WAYPOINTS'] = 'Search from';
$mod_strings['LBL_GOOGLEMAPS_CIRCLEDESIGNER_BTN'] = 'Periphery search';
$mod_strings['LBL_GOOGLEMAPS_CIRCLEDESIGNER_RESETBTN'] = 'Delete circle(s)';
$mod_strings['LBL_GOOGLEMAPS_CIRCLEDESIGNER_MODULE'] = 'Módulo';
$mod_strings['LBL_GOOGLEMAPS_CIRCLEDESIGNER_DISPLAYFIELDS'] = 'Query config';

$mod_strings['LBL_PREVIEW'] = 'Preview for';

$mod_strings['LBL_PDF_WHERE'] = 'Print Selection criteria';



    //Bucket Manager
$mod_strings['LNK_MANAGE_BUCKETS'] = 'Bucket Manager';
$mod_strings['LBL_GROUPING_ID'] = 'Grouping ID';
$mod_strings['LBL_GROUPING_NAME'] = 'Grouping Name';
$mod_strings['LBL_GROUPING_DESCRIPTION'] = 'Description';
$mod_strings['LBL_GROUPING_MODULENAME'] = 'Nombre Módulo';
$mod_strings['LBL_GROUPING_FIELDNAME'] = 'Nombre campo';
$mod_strings['LBL_GROUPING_MAPPING'] = 'Mapeo';
$mod_strings['LBL_GROUPING_CREATE'] = 'Create New Grouping';
$mod_strings['LBL_GROUPING_ENUMFIELDS'] = 'Campos disponibles';
$mod_strings['LBL_MAPPING_VALUE'] = 'Mapping Name';
$mod_strings['LBL_MAPPING_NEWVALUE'] = 'New Mapping';
$mod_strings['LBL_MAPPING_PROMPT_NAME'] = 'Please, enter a name';
$mod_strings['LBL_ENUMVALUE_ID'] = 'ID';
$mod_strings['LBL_ENUMVALUE_VALUE'] = 'Value';
$mod_strings['LBL_ENUMVALUE_LABEL'] = 'Etiqueta';
$mod_strings['LBL_ENUMVALUE_HANDLER'] = 'Group remaining values into \'others\'';
$mod_strings['LBL_GRID_GROUPINGS'] = 'Groupings';
$mod_strings['LBL_GRID_MODULEFIELD'] = 'Fuente';
$mod_strings['LBL_GRID_MAPPINGS'] = 'Mappings';
$mod_strings['LBL_GRID_ENUMVALUES'] = '<< Drag Values';
$mod_strings['LBL_ERROR'] = 'Error:';
$mod_strings['LBL_ERROR_REQUIREDALL'] = 'Todos los campos son obligatorios!';
$mod_strings['LBL_ERROR_DELETEGROUPING'] = 'Grouping could not be deleted. Please, check Sugar log.';
$mod_strings['LBL_ERROR_UPDATEGROUPING'] = 'Grouping could not be saved. Please, check Sugar log.';
$mod_strings['LBL_ERROR_NOGROUPINGSELECTED'] = 'Please, select a grouping!';

    //DList Manager   
$mod_strings['LNK_MANAGE_DLISTS'] = 'DList Manager';
$mod_strings['LBL_GRID_DLISTS'] = 'Distributions lists';
$mod_strings['LBL_DLIST_FILTER'] = 'Filtrar';
$mod_strings['LBL_DLIST_SEARCH'] = 'Buscar';
$mod_strings['LBL_DLIST_ID'] = 'ID';
$mod_strings['LBL_DLIST_NAME'] = 'Nombre';
$mod_strings['LBL_DLIST_USER_ID'] = 'User ID';
$mod_strings['LBL_DLIST_USER_USERNAME'] = 'User Name';
$mod_strings['LBL_DLIST_FIRSTNAME'] = 'Nombres';
$mod_strings['LBL_DLIST_LASTNAME'] = 'Apellidos';
$mod_strings['LBL_DLIST_EMAIL1'] = 'e-mail';
$mod_strings['LBL_DLIST_CONTACT_ID'] = 'Contact ID';
$mod_strings['LBL_DLIST_CONTACT_ACCOUNTNAME'] = 'Account Name';
$mod_strings['LBL_DLIST_CONTACT_ACCOUNTID'] = 'Account ID';
$mod_strings['LBL_DLIST_KREPORT_MODULENAME'] = 'Módulo';
$mod_strings['LBL_DLIST_WINDOW_ADDKREPORTS_TITLE'] = 'Add KReports';
$mod_strings['LBL_DLIST_WINDOW_ADDUSERS_TITLE'] = 'Add Users';
$mod_strings['LBL_DLIST_WINDOW_ADDCONTACTS_TITLE'] = 'Add Contacts';

    //ksavedfilters
$mod_strings['LBL_KSAVEDFILTERS'] = 'Saved Filters';
$mod_strings['LBL_KSAVEDFILTERS_ID'] = 'Saved Filter ID';
$mod_strings['LBL_KSAVEDFILTERS_NAME'] = 'Nombre';
$mod_strings['LBL_KSAVEDFILTERS_ASSIGNED_USER_ID'] = 'Assigned User ID';
$mod_strings['LBL_KSAVEDFILTERS_ASSIGNED_USER_NAME'] = 'Assigned user';
$mod_strings['LBL_KSAVEDFILTERS_IS_GLOBAL'] = 'set for all users';
$mod_strings['LBL_KSAVEDFILTERS_IS_GLOBAL_MARK'] = '(G)';
$mod_strings['LBL_KSAVEDFILTERS_SELECTEDFILTERS'] = 'Filtros';
$mod_strings['LBL_KSAVEDFILTERS_SAVE_BTN'] = 'Save Filters';
$mod_strings['LBL_KSAVEDFILTERS_DELETE_BTN'] = 'Eliminar';
$mod_strings['LBL_KSAVESFILTERS_EMPTYTEXT'] = '-- select a filter --';
$mod_strings['LBL_KSAVEDFILTERS_WINDOW_TITLE'] = 'Save Filter';
$mod_strings['LBL_KSAVEDFILTERS_STATUS'] = 'Estado';
$mod_strings['LBL_KSAVEDFILTERS_CONTENT'] = 'Filter details';