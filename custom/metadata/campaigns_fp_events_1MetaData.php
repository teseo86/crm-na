<?php
// created: 2017-02-20 21:57:03
$dictionary["campaigns_fp_events_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => false,
  'relationships' => 
  array (
    'campaigns_fp_events_1' => 
    array (
      'lhs_module' => 'Campaigns',
      'lhs_table' => 'campaigns',
      'lhs_key' => 'id',
      'rhs_module' => 'FP_events',
      'rhs_table' => 'fp_events',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'campaigns_fp_events_1_c',
      'join_key_lhs' => 'campaigns_fp_events_1campaigns_ida',
      'join_key_rhs' => 'campaigns_fp_events_1fp_events_idb',
    ),
  ),
  'table' => 'campaigns_fp_events_1_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'campaigns_fp_events_1campaigns_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'campaigns_fp_events_1fp_events_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'campaigns_fp_events_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'campaigns_fp_events_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'campaigns_fp_events_1campaigns_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'campaigns_fp_events_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'campaigns_fp_events_1fp_events_idb',
      ),
    ),
  ),
);
