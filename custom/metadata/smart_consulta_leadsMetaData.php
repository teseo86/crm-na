<?php
// created: 2017-08-24 16:13:23
$dictionary["smart_consulta_leads"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'smart_consulta_leads' => 
    array (
      'lhs_module' => 'Leads',
      'lhs_table' => 'leads',
      'lhs_key' => 'id',
      'rhs_module' => 'smart_Consulta',
      'rhs_table' => 'smart_consulta',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'smart_consulta_leads_c',
      'join_key_lhs' => 'smart_consulta_leadsleads_ida',
      'join_key_rhs' => 'smart_consulta_leadssmart_consulta_idb',
    ),
  ),
  'table' => 'smart_consulta_leads_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'smart_consulta_leadsleads_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'smart_consulta_leadssmart_consulta_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'smart_consulta_leadsspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'smart_consulta_leads_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'smart_consulta_leadsleads_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'smart_consulta_leads_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'smart_consulta_leadssmart_consulta_idb',
      ),
    ),
  ),
);