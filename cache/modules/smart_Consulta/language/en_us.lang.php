<?php
// created: 2017-09-13 03:34:05
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Assigned User Id',
  'LBL_ASSIGNED_TO_NAME' => 'Assigned to',
  'LBL_SECURITYGROUPS' => 'Security Groups',
  'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => 'Security Groups',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Date Created',
  'LBL_DATE_MODIFIED' => 'Date Modified',
  'LBL_MODIFIED' => 'Modified By',
  'LBL_MODIFIED_ID' => 'Modified By Id',
  'LBL_MODIFIED_NAME' => 'Modified By Name',
  'LBL_CREATED' => 'Created By',
  'LBL_CREATED_ID' => 'Created By Id',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DELETED' => 'Deleted',
  'LBL_NAME' => 'Name',
  'LBL_CREATED_USER' => 'Created by User',
  'LBL_MODIFIED_USER' => 'Modified by User',
  'LBL_LIST_NAME' => 'Name',
  'LBL_EDIT_BUTTON' => 'Edit',
  'LBL_REMOVE' => 'Remove',
  'LBL_LIST_FORM_TITLE' => 'Consulta List',
  'LBL_MODULE_NAME' => 'Consulta',
  'LBL_MODULE_TITLE' => 'Consulta',
  'LBL_HOMEPAGE_TITLE' => 'My Consulta',
  'LNK_NEW_RECORD' => 'Create Consulta',
  'LNK_LIST' => 'View Consulta',
  'LNK_IMPORT_SMART_CONSULTA' => 'Importar Consulta',
  'LBL_SEARCH_FORM_TITLE' => ' Consulta',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_SMART_CONSULTA_SUBPANEL_TITLE' => 'Consulta',
  'LBL_NEW_FORM_TITLE' => 'New Consulta',
  'LBL_FUENTE' => 'Fuente',
  'LBL_ESTADO' => 'Estado',
  'solicitante' => 'Solicitante',
  'LBL_SEDE_FP_EVENT_LOCATIONS_ID' => 'Sede (relacionado Sedes ID)',
  'LBL_SEDE' => 'Sede',
  'LBL_CURSO_AOS_PRODUCT_CATEGORIES_ID' => 'Curso solicitante (relacionado Curso Base ID)',
  'LBL_CURSO' => 'Curso solicitante',
  'LBL_ID_BD_CONSULTA' => 'ID Bd Consulta',
  'LBL_SMART_CONSULTA_LEADS_FROM_LEADS_TITLE' => 'Interesados',
  'LBL_SMART_CONSULTA_CONTACTS_FROM_CONTACTS_TITLE' => 'Contactos',
);